installations
-------------

to install postgres and dependencies in ubuntu follow below steps:

    sudo apt-get update
    sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib

    sudo su - postgres
    psql

    create database findroom;
    create USER root WITH PASSWORD 'root';
    sudo pip install django psycopg2