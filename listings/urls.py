from django.conf.urls import include, url
from django.contrib import admin

from rest_framework.routers import DefaultRouter,SimpleRouter

from .views import *

router = SimpleRouter()

router.register(r'countries',country_viewset , 'countries')
router.register(r'cities',city_viewset, 'cities')
router.register(r'states',state_viewset, 'states')
router.register(r'location',location_viewset, 'location')
router.register(r'listings',listings_viewset , 'listings')
router.register(r'intrested_listings',intrested_listings_viewset, 'intrested_listings')
router.register(r'my_listings',my_listings, 'my_listings')
#router.register(r'messages',messages_viewset, 'messages')
router.register(r'listing_photos',listing_photos_viewset, 'listing_photos')
router.register(r'purchase_type',purchaseType_viewset, 'purchase_type')
router.register(r'area',area_viewset, 'area')
router.register(r'tenant_ages',tenant_ages_viewset, 'ages')
router.register(r'delete_intrest', delete_intrest, 'delete_intrest')
router.register(r'property_type',Property_Type_viewset,'property_type')
router.register(r'room_type',Room_Type_viewset,'room_type')
router.register(r'requests',Request_viewset,'requests')
router.register(r'all_requests',all_requests,'all_requests')
router.register(r'paid_listings',paid_listings,'paid_listings')
router.register(r'all_furnitures',all_furnitures,'all_furnitures')
router.register(r'get_all_cleaning_basis',get_all_cleaning_basis,'get_all_cleaning_basis')
router.register(r'get_bill_details',get_bill_details,'get_bill_details')
router.register(r'get_photo_schemes',get_photo_schemes,'get_photo_schemes')
router.register(r'get_scan360_schemes',get_scan360_schemes,'get_scan360_schemes')

router.register(r'listing-status-change',Listing_Status_Change_viewset,'Listing_Status_Change_viewset')


urlpatterns = router.urls
