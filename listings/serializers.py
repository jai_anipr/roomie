from .models import *
from rest_framework.serializers import ModelSerializer

class listings_serializer(ModelSerializer):


    class Meta:
        model = listings
        fields = '__all__'
        depth = 3


class intrested_listings_serializer(ModelSerializer):
    class Meta:
        model = intrested_listings
        fields = '__all__'
        depth = 3

class listing_photos_serializer(ModelSerializer):
    class Meta:
        model = listing_photos
        fields = '__all__'



class country_serializer(ModelSerializer):
    class Meta:
        model = country
        fields = '__all__'


class state_serializer(ModelSerializer):
    class Meta:
        model = state
        fields = '__all__'
        depth = 2

class city_serializer(ModelSerializer):
    class Meta:
        model = city
        fields = '__all__'
        depth = 2


class location_serializer(ModelSerializer):
    class Meta:
        model = location
        fields = '__all__'
        depth = 2

class purchase_type_serializer(ModelSerializer):
    class Meta:
        model = purchase_type
        fields = '__all__'
        depth = 2

class area_serializer(ModelSerializer):
    class Meta:
        model = area
        fields = '__all__'
        depth = 2

class tenant_age_serializer(ModelSerializer):
    class Meta:
        model = ages_tenant
        fields = '__all__'
        depth = 2

class property_type_serializer(ModelSerializer):
    class Meta:
        model = property_type
        fields = '__all__'
        depth = 2

class room_type_serializer(ModelSerializer):
    class Meta:
        model = room_type
        fields = '__all__'
        depth = 2

class request_serializer(ModelSerializer):
    class Meta:
        model = requests
        fields = '__all__'
        depth = 2


class furniture_serializer(ModelSerializer):
    class Meta:
        model = furniture_type
        fields = '__all__'

class cleaning_serializer(ModelSerializer):
    class Meta:
        model = cleaning
        fields = '__all__'


class billing_serializer(ModelSerializer):
    class Meta:
        model = bills_details
        fields = '__all__'

class scan_schemes_serializer(ModelSerializer):
    class Meta:
        model = scan_schemes
        fields = '__all__'

class photo_scheme_serializer(ModelSerializer):
    class Meta:
        model = photography_schemes
        fields = '__all__'
