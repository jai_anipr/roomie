# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from config.models import badges,rules,amenity
from users.models import findroomUser
from django.contrib.auth.models import User
import django_filters
from  rest_framework import filters

# Create your models here.

class country(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.name)


class state(models.Model):
    state_name = models.CharField(max_length=100)
    country_id = models.ForeignKey(country)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.state_name)


class city(models.Model):
    city_name = models.CharField(max_length=100)
    state_id = models.ForeignKey(state,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.city_name)

class area(models.Model):
    name=models.CharField(max_length=100,null=True,blank=True)
    city_id=models.ForeignKey(city,null=True,blank=True)
    state_id=models.ForeignKey(state,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return '%s' % (self.name)

class location(models.Model):
    street_name = models.CharField(max_length=500)
    #latitude = models.FloatField()
    #longitude = models.FloatField()
    apartment = models.CharField(max_length=100,blank=True,null=True)
    # city = models.ForeignKey(city)
    # state = models.ForeignKey(state)
    building_name = models.CharField(max_length=100,null=True,blank=True)
    zip_code = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        return '%s' % (self.street_name)


class listing_photos(models.Model):
    photo = models.CharField(max_length=250,null=True,blank=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        
        return '%s' % (self.photo)

class ages_tenant(models.Model):
    name=models.CharField(max_length=100)
    def __str__(self):
        return '%s' % (self.name)

# class nationali(models.Model):
#     name=models.CharField(max_length=100)
#     def __str__(self):
#         return '%s' % (self.name)


class purchase_type(models.Model):
    purchase_name = models.CharField(max_length=250,null=True,blank=True)
    price = models.BigIntegerField(null=True,blank=True)
    days = models.BigIntegerField(null=True,blank=True)
    jobs = models.BigIntegerField(null=True,blank=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        
        return '%s' % (self.purchase_name)

class property_type(models.Model):
    property_name = models.CharField(max_length=250,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        
        return '%s' % (self.property_name)

class listing_status(models.Model):
    name = models.CharField(max_length=250,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        
        return '%s' % (self.name)

class room_type(models.Model):
    room_type_name = models.CharField(max_length=250,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        
        return '%s' % (self.room_type_name)

class furniture_type(models.Model):
    furniture_name = models.CharField(max_length=250,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        
        return '%s' % (self.furniture_name)

class cleaning(models.Model):
    cleaning_basis = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.cleaning_basis)

class bills_details(models.Model):
    bill_details = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.bill_details)

class photography_schemes(models.Model):
    scheme_name = models.CharField(max_length=250, null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.scheme_name)

class scan_schemes(models.Model):
    scheme_name = models.CharField(max_length=250, null=True, blank=True)
    price = models.IntegerField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.scheme_name)

# class living_rooms(models.Model):
#     living_room_no = models.CharField(max_length=250,null=True,blank=True)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     def __str__(self):
#
#         return '%s' % (self.living_room_no)
#
# class kitchen_rooms(models.Model):
#     kitchen_room_no = models.CharField(max_length=250,null=True,blank=True)
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#     def __str__(self):
#
#         return '%s' % (self.kitchen_room_no)

class listings(models.Model):
    user_id = models.ForeignKey(findroomUser,null=True,blank=True)

    GENDER_CHOICES = (
        ('male', 'male'),
        ('female', 'female'),
        ('either', 'either'),
    )
    
    gender_choice = models.CharField(max_length=6, choices=GENDER_CHOICES,blank=True,null=True)
    age_choices=models.ManyToManyField(ages_tenant,blank=True)

    is_active = models.NullBooleanField(default=True)
    country_id = models.ForeignKey(country,null=True,blank=True)
    state_id = models.ForeignKey(state,null=True,blank=True)
    city_id = models.ForeignKey(city,null=True,blank=True)
    location_id = models.ForeignKey(location,null=True,blank=True)
    latitude = models.FloatField(null=True,blank=True)
    longitude = models.FloatField(null=True,blank=True)
    monthly_rent = models.BigIntegerField()
    deposit = models.BigIntegerField(null=True,blank=True)
    commision = models.BigIntegerField(null=True,blank=True)
    yearly_rent = models.BigIntegerField(null=True,blank=True)
    nationalities = models.ManyToManyField(country,blank=True,related_name="nationalities")
    move_in_date = models.DateField(null=True,blank=True)
    purchase_type = models.ForeignKey(purchase_type,blank=True,null=True)
    room_type = models.ForeignKey(room_type,blank=True,null=True)
    listing_status = models.ForeignKey(listing_status,blank=True,null=True)

    STAY_CHOICES = (
        ('small', 'small'),
        ('medium', 'medium'),
        ('long', 'long'),
    )
    validity_for_stay = models.CharField(max_length=6, choices=STAY_CHOICES)

 #   validity_for_stay = models.CharField(max_length=100,null=True,blank=True)
    amenity_ids = models.ManyToManyField(amenity,blank=True)
    rules_ids = models.ManyToManyField(rules,blank=True)
    description = models.TextField(null=True,blank=True)
    badge_id = models.ManyToManyField(badges,blank=True)
    photo_ids = models.ManyToManyField(listing_photos,blank=True)
    security = models. NullBooleanField(default=False)
    area_id = models.ForeignKey(area,null=True,blank=True)
    distance_from_metro = models.CharField(max_length=120,null=True,blank=True)
    distance_from_busstop = models.CharField(max_length=120,null=True,blank=True)
    bedrooms = models.CharField(max_length=120,null=True,blank=True)
    bathrooms = models.CharField(max_length=120,null=True,blank=True)
    distance_from_tram = models.CharField(max_length=120,null=True,blank=True)
    distance_from_airport = models.CharField(max_length=120,null=True,blank=True)
    landmark = models.CharField(max_length=150,null=True,blank=True)
    tax_area = models.CharField(max_length=150,null=True,blank=True)
    property_type_id = models.ForeignKey(property_type,null=True,blank=True)
    is_listable = models.NullBooleanField(default=False)
    is_featured = models.NullBooleanField(default=False)
    furniture = models.ForeignKey(furniture_type,null=True,blank=True)
    living_room = models.CharField(max_length=100,null=True,blank=True)
    kitchen_room = models.CharField(max_length=100,null=True,blank=True)
    #changed from is_secure
    cleaning_included = models.ForeignKey(cleaning,null=True,blank=True)
    bills_included = models.ForeignKey(bills_details,null=True,blank=True)
    listing_desc_title = models.TextField(null=True,blank=True)
    professional_photography = models.ForeignKey(photography_schemes,null=True,blank=True)
    scanning_360 = models.ForeignKey(scan_schemes,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        
        return '%s' % (self.user_id)


class intrested_listings(models.Model):
    user_id = models.ForeignKey(findroomUser)
    listing_id = models.ForeignKey(listings)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        
        return '%s' % (self.user_id)

class requests(models.Model):
    user_id = models.ForeignKey(findroomUser,null=True,blank=True)
    name = models.CharField(max_length=120,null=True,blank=True)
    mobile = models.CharField(max_length=120,null=True,blank=True)
    email = models.CharField(max_length=120,null=True,blank=True)
    how_many_properties = models.CharField(max_length=120,null=True,blank=True)
    url = models.CharField(max_length=500,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        
        return '%s' % (self.user_id)

