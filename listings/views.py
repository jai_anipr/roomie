from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from rest_framework.viewsets import ModelViewSet
from .models import *
from rest_framework import request, response, status
from .serializers import *
import django_filters
from  rest_framework import filters
from config.models import notification


class ProductFilter(filters.FilterSet):
    min_price = django_filters.NumberFilter(name="monthly_rent", lookup_expr='gte')
    max_price = django_filters.NumberFilter(name="monthly_rent", lookup_expr='lte')
    move_date_gte = django_filters.DateFilter(name="move_in_date", lookup_expr='gte')

    class Meta:
        model = listings
        fields = ['min_price', 'validity_for_stay', 'move_date_gte', 'max_price', 'deposit','amenity_ids',
              'badge_id', 'is_active', 'city_id', 'country_id', 'state_id',
              'gender_choice','area_id','rules_ids','is_listable','is_featured','room_type','property_type_id','purchase_type__purchase_name']

from itertools import chain
import ast
import itertools


from django.db.models import Count
class listings_viewset(ModelViewSet):
    queryset = listings.objects.all()
    serializer_class = listings_serializer
    filter_class = ProductFilter
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    search_fields = ('location_id__street_name', 'city_id__city_name', 'state_id__state_name','area_id__name',)
    # filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    # filter_fields = ('location_id','deposit','move_in_date','amenity_ids','badge_id','is_active','monthly_rent','city_id','country_id','state_id','city_id',)

    ordering_fields = ('created_at', 'updated_at', 'start_time', 'id','-is_featured',)

    def list(self, request, *args, **kwargs):
        findroomuserid = request.GET.get('userid')
        ame_ids = request.GET.get('ame_ids')
        age_ids = request.GET.get('age_ids')

        rul_ids = request.GET.get('rule_ids')
        try:
            queryset = listings.objects.all().order_by('-is_featured','-updated_at')
            serializer = listings_serializer(instance=queryset, many=True)
            if age_ids:
                print 'a'
                age_list = ast.literal_eval(age_ids)
                print age_list
                age_queryset = listings.objects.filter(age_choices__id__in=age_list)
                print age_queryset
                qs = self.filter_queryset(age_queryset)
            else:
                qs = self.filter_queryset(queryset)

            if ame_ids:
                ame_new_list = ast.literal_eval(ame_ids)
                for i in ame_new_list:
                    print i
                print len(ame_new_list)
                print ame_new_list
                print type(ame_new_list)

                ame_queryset = listings.objects.filter(amenity_ids__id__in=ame_new_list).annotate(num_tags=Count('amenity_ids__id')).filter(num_tags=len(ame_new_list))
                print ame_queryset
                qs = self.filter_queryset(ame_queryset)
            else:
                qs = self.filter_queryset(qs)
            if rul_ids:

                k = [int(s) for s in rul_ids.split(',')]
                me_rule_list = ast.literal_eval(rul_ids)
                print me_rule_list
                rul_queryset = listings.objects.filter(rules_ids__id__in=k).annotate(
                    num_tags=Count('rules_ids__id')).filter(num_tags=len(k))

                query1 = self.filter_queryset(rul_queryset)
                if ame_ids:
                    one =[]
                    for i in ame_queryset:
                        one.append(i.id)
                    for i in rul_queryset:
                        one.append(i.id)
                    final_list1 = list(set(one))
                    print final_list1, 'final'
                    new_queryset=listings.objects.filter(id__in=final_list1)
                    query1 = self.filter_queryset(new_queryset)



            else:
                query1 = self.filter_queryset(qs)
                print query1, 'aksbdisabduisab'

            page = self.paginate_queryset(query1)
            if page is not None:
                serializer = listings_serializer(page, many=True)

            if (findroomuserid):


                for temp in serializer.data:

                    my_list_object = listings.objects.filter(id=temp['id'], user_id=int(findroomuserid))

                    if my_list_object:
                        temp['I_created_this'] = True
                    else:
                        temp['I_created_this'] = False

                    my_fav_object = intrested_listings.objects.filter(listing_id=temp['id'], user_id=int(findroomuserid))

                    if my_fav_object:
                        temp['I_favourited_this'] = True
                    else:
                        temp['I_favourited_this'] = False

            return (self.get_paginated_response(serializer.data))
        except Exception as e:
            return response.Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):

        try:

            # ---- parameter verifications ----
            allowed_fields = ['is_active','city','area','zip_code' ,'location_id', 'city_id', 'state_id', 'monthly_rent', 'user_id', 'deposit',
                              'move_in_date','yearly_rent','commision', 'validity_for_stay','latitude','longitude','area_id','roomtype_id','propertytype_id','is_flexible',
                              'amenity_ids', 'gender_choice', 'age_choices', 'rules_ids', 'description', 'badge_id','bathrooms','bedrooms','kitchen_rooms','living_rooms',
                              'photo_ids','professional_photograph','scan360','listing_desc_title','tax_area','bills_included','cleaning_details','nationalities','cleaning_details','bills_included','furniture','landmark', 'building_name','is_secure','security','plan','distance_from_airport','distance_from_tram','distance_from_busstop','distance_from_metro']

            mandatory_fields = ['monthly_rent']
            input_keys = request.data.keys()

            temp = set(input_keys) - set(allowed_fields)
            if len(temp):
                temp_string = ','.join(str(x) for x in temp)
                return response.Response({'error': 'invalid fields: ' + temp_string},
                                         status=status.HTTP_400_BAD_REQUEST)

            # implementing mandatory fields
            mandatory_temp = set(mandatory_fields) - set(input_keys)
            if len(mandatory_temp):
                temp_string = ','.join(str(x) for x in mandatory_temp)
                return response.Response({'error': 'mandatory fields: ' + temp_string},
                                         status=status.HTTP_400_BAD_REQUEST)

            listing_obj = listings()

            if 'location_id' in request.data:
                loc_data = request.data['location_id']

                location_obj = location()
                if 'zip_code' in loc_data:
                    location_obj.zip_code = loc_data['zip_code']
                if "street_name" in loc_data:
                    location_obj.street_name = loc_data['street_name']
                if "apartment" in loc_data:
                    location_obj.apartment = loc_data['apartment']
                if "building_name" in loc_data:
                    location_obj.building_name = loc_data['building_name']

                location_obj.save()

                listing_obj.location_id = location_obj

            if 'country_id' in request.data:
                country_obj = country.objects.get(id=int(request.data['country_id']))
                listing_obj.country_id = country_obj

            if 'user_id' in request.data:
                user_obj = findroomUser.objects.get(id=int(request.data['user_id']))
                listing_obj.user_id = user_obj

            if 'state_id' in request.data:
                state_obj = state.objects.get_or_create(id=int(request.data['state_id']))
                listing_obj.state_id = state_obj

            if 'security' in request.data:
                listing_obj.security = request.data['security']



            if 'propertytype_id' in request.data:
                property_obj = property_type.objects.get(id=int(request.data['propertytype_id']))
                listing_obj.property_type_id = property_obj

            if 'roomtype_id' in request.data:
                room_obj = room_type.objects.get(id=int(request.data['roomtype_id']))
                listing_obj.room_type = room_obj

            if 'is_flexible' in request.data:
                  listing_obj.is_flexible = request.data['is_flexible']  

            if 'city_id' in request.data:
                city_1 = city.objects.get(city_name=request.data['city_id'])
                print city_1
                if not city_1:
                    city_obj = city.objects.create(city_name=request.data['city_id'])
                    listing_obj.city_id = city_obj
                else:
                    listing_obj.city_id=city_1

            if 'monthly_rent' in request.data:
                listing_obj.monthly_rent = request.data['monthly_rent']

            if 'deposit' in request.data:
                listing_obj.deposit = request.data['deposit']

            if 'commision' in request.data:
                listing_obj.commision = request.data['commision']

            if 'yearly_rent' in request.data:
                listing_obj.yearly_rent = request.data['yearly_rent']


            if 'description' in request.data:
                listing_obj.description = request.data['description']

            if 'listing_desc_title' in request.data:
                listing_obj.listing_desc_title = request.data['listing_desc_title']


            if 'gender_choice' in request.data:
                listing_obj.gender_choice = request.data['gender_choice']

            if 'move_in_date' in request.data:
                listing_obj.move_in_date = request.data['move_in_date']

            if 'validity_for_stay' in request.data:
                listing_obj.validity_for_stay = request.data['validity_for_stay']

            if 'distance_from_airport' in request.data:
                listing_obj.distance_from_airport = request.data['distance_from_airport']

            if 'distance_from_tram' in request.data:
                listing_obj.distance_from_tram = request.data['distance_from_tram']

            if 'distance_from_busstop' in request.data:
                listing_obj.distance_from_busstop = request.data['distance_from_busstop']

            if 'distance_from_metro' in request.data:
                listing_obj.distance_from_metro = request.data['distance_from_metro']

            if 'is_secure' in request.data:
                listing_obj.is_secure = request.data['is_secure']

            if 'landmark' in request.data:
                listing_obj.landmark = request.data['landmark']
            if 'tax_area' in request.data:
                listing_obj.tax_area = request.data['tax_area']


            if 'is_active' in request.data:
                listing_obj.is_active = request.data['is_active']

            if 'latitude' in request.data:
                listing_obj.latitude = request.data['latitude']

            if 'longitude' in request.data:
                listing_obj.longitude = request.data['longitude']

            if 'area_id' in request.data:
                areaobj = area.objects.create(name=request.data['area_id'])
                listing_obj.area_id = areaobj

            if 'bedrooms' in request.data:
                listing_obj.bedrooms = request.data['bedrooms']

            if 'bathrooms' in request.data:
                listing_obj.bathrooms = request.data['bathrooms']

            if 'living_rooms' in request.data:
                listing_obj.living_room = request.data['living_rooms']

            if 'kitchen_rooms' in request.data:
                listing_obj.kitchen_room = request.data['kitchen_rooms']

            if 'furniture' in request.data:
                listing_obj.furniture = furniture_type.objects.get(id=int(request.data['furniture']))


            if 'cleaning_details' in request.data:
                listing_obj.cleaning_included = cleaning.objects.get(id=int(request.data['cleaning_details']))

            if 'bills_included' in request.data:
                listing_obj.bills_included = bills_details.objects.get(id=int(request.data['bills_included']))

            if 'scan360' in request.data:
                listing_obj.scanning_360 = scan_schemes.objects.get(id=int(request.data['scan360']))

            if 'professional_photograph' in request.data:
                listing_obj.professional_photography = photography_schemes.objects.get(id=int(request.data['professional_photograph']))

            if 'plan' in request.data:
                listing_obj.purchase_type = purchase_type.objects.get(id=int(request.data['plan']))

            listing_obj.listing_status = listing_status.objects.get(name="Approve")

            listing_obj.save()

            if 'badge_id' in request.data:
                badge_list = []
                for item in request.data['badge_id']:
                    badge_obj = badges.objects.get(id=int(item))
                    badge_list.append(badge_obj)
                listing_obj.badge_id.add(*badge_list)


            if 'nationalities' in request.data:
                nation_list = []
                for item in request.data['nationalities']:
                    country_obj = country.objects.get(id=int(item))
                    nation_list.append(country_obj)
                listing_obj.nationalities.add(*nation_list)


            if 'age_choices' in request.data:
                age_list = []
                for item in request.data['age_choices']:
                    age_obj = ages_tenant.objects.get(id=int(item))
                    age_list.append(age_obj)
                listing_obj.age_choices.add(*age_list)

            if 'rules_ids' in request.data:
                rules_list = []
                for item in request.data['rules_ids']:
                    rules_obj = rules.objects.get(id=int(item))
                    rules_list.append(rules_obj)
                listing_obj.rules_ids.add(*rules_list)

            if 'amenity_ids' in request.data:
                amenity_list = []
                for item in request.data['amenity_ids']:
                    amenity_obj = amenity.objects.get(id=int(item))
                    amenity_list.append(amenity_obj)
                listing_obj.amenity_ids.add(*amenity_list)

            if 'photo_ids' in request.data:
                photos_list = []
                for item in request.data['photo_ids']:
                    photo_obj = listing_photos.objects.create(photo=item)
                    photos_list.append(photo_obj)
                listing_obj.photo_ids.add(*photos_list)

            notification.objects.create(notification_text = str(user_obj.user.username)+' '+'created a new listing',user= user_obj, listing = listing_obj)

            return response.Response({'success': 'listing created successfully'}, status=status.HTTP_200_OK)

        except Exception as e:
            print e

        return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:

            listing_object = self.get_object()

            print ("listing_object",listing_object)

            if 'city_id' in request.data:
                city_1 = city.objects.get(city_name=request.data['city_id']['city_name'])
                print city_1
                if not city_1:
                    city_obj = city.objects.create(city_name=request.data['city_id'])
                    listing_object.city_id = city_obj
                else:
                    listing_object.city_id=city_1

            if 'location_id' in request.data:
                loc_data = request.data['location_id']

                location_obj = location()
                if 'zip_code' in loc_data:
                    location_obj.zip_code = loc_data['zip_code']
                if "street_name" in loc_data:
                    location_obj.street_name = loc_data['street_name']
                if "apartment" in loc_data:
                    location_obj.apartment = loc_data['apartment']
                location_obj.save()

                listing_object.location_id = location_obj

            if 'propertytype_id' in request.data:
                property_obj = property_type.objects.get(id=int(request.data['propertytype_id']))
                listing_object.property_type_id = property_obj

            if 'roomtype_id' in request.data:
                room_obj = room_type.objects.get(id=int(request.data['roomtype_id']))
                listing_object.room_type = room_obj

            if 'gender_choice' in request.data:
                listing_object.gender_choice = request.data['gender_choice']

            if 'security' in request.data:
                listing_object.security = request.data['security']

            if 'monthly_rent' in request.data:
                listing_object.monthly_rent = request.data['monthly_rent']

            if 'deposit' in request.data:
                listing_object.deposit = request.data['deposit']

            if 'description' in request.data:
                listing_object.description = request.data['description']


            if 'move_in_date' in request.data:
                listing_object.move_in_date = request.data['move_in_date']

            if 'validity_for_stay' in request.data:
                listing_object.validity_for_stay = request.data['validity_for_stay']

            if 'is_secure' in request.data:
                listing_object.is_secure = request.data['is_secure']

            if 'is_active' in request.data:
                listing_object.is_active = request.data['is_active']

            if 'bedrooms' in request.data:
                listing_object.bedrooms = request.data['bedrooms']

            if 'bathrooms' in request.data:
                listing_object.bathrooms = request.data['bathrooms']

            if 'plan' in request.data:
                listing_object.purchase_type = purchase_type.objects.get(id=int(request.data['plan']))

            if 'area_id' in request.data:
                try:
                    areaobj = area.objects.get(name=request.data['area_id']['name'])
                    listing_object.area_id = areaobj
                except Exception,e:
                    print ("area error",e)
                    pass

            if 'distance_from_airport' in request.data:
                listing_object.distance_from_airport = request.data['distance_from_airport']

            if 'distance_from_tram' in request.data:
                listing_object.distance_from_tram = request.data['distance_from_tram']

            if 'distance_from_busstop' in request.data:
                listing_object.distance_from_busstop = request.data['distance_from_busstop']

            if 'distance_from_metro' in request.data:
                listing_object.distance_from_metro = request.data['distance_from_metro']

            if "purchase_type.id" in request.data:
                listing_object.purchase_type = purchase_type.objects.get(id=int(request.data["purchase_type.id"]))

            if 'room_type' in request.data:
                room_obj = room_type.objects.get(id=int(request.data['room_type']['id']))
                listing_object.room_type = room_obj

            if 'listing_desc_title' in request.data:
                listing_object.listing_desc_title = request.data['listing_desc_title']

            if 'landmark' in request.data:
                listing_object.landmark = request.data['landmark']

            if 'tax_area' in request.data:
                listing_object.tax_area = request.data['tax_area']

            if 'living_rooms' in request.data:
                listing_object.living_room = request.data['living_rooms']

            if 'kitchen_rooms' in request.data:
                listing_object.kitchen_room = request.data['kitchen_rooms']

            if 'furniture' in request.data:
                listing_object.furniture = furniture_type.objects.get(id=int(request.data['furniture']))

            if 'cleaning_details' in request.data:
                listing_object.cleaning_included = cleaning.objects.get(id=int(request.data['cleaning_details']))

            if 'bills_included' in request.data:
                listing_object.bills_included = bills_details.objects.get(id=int(request.data['bills_included']))

            if 'scan360' in request.data:
                listing_object.scanning_360 = scan_schemes.objects.get(id=int(request.data['scan360']))

            if 'professional_photograph' in request.data:
                listing_object.professional_photography = photography_schemes.objects.get(
                    id=int(request.data['professional_photograph']))

            listing_object.save()

            if 'badge_id' in request.data:
                if len(request.data['badge_id']) != 0:
                    badge_list = []
                    for item in request.data['badge_id']:
                        try:
                            badge_obj = badges.objects.get(id=int(item))
                            badge_list.append(badge_obj)
                        except Exception as e:
                            badge_obj = badges.objects.get(id=int(item['id']))
                            badge_list.append(badge_obj)

                    listing_object.badge_id.add(*badge_list)

            if 'nationalities' in request.data:
                if len(request.data['nationalities']) != 0:
                    nation_list = []
                    for item in request.data['nationalities']:
                        try:
                            country_obj = country.objects.get(id=int(item))
                            nation_list.append(country_obj)
                        except Exception as e:
                            country_obj = country.objects.get(id=int(item['id']))
                            nation_list.append(country_obj)
                    listing_object.nationalities.add(*nation_list)


            if 'amenity_ids' in request.data:
                if len(request.data['amenity_ids']) != 0:
                    amenity_list = []
                    for item in request.data['amenity_ids']:
                        try:
                            amenity_obj = amenity.objects.get(id=int(item))
                            amenity_list.append(amenity_obj)
                        except Exception as e:
                            amenity_obj = amenity.objects.get(id=int(item['id']))
                            amenity_list.append(amenity_obj)

                    listing_object.amenity_ids.add(*amenity_list)

            if 'rules_ids' in request.data:
                if len(request.data['rules_ids']) != 0:
                    rules_list = []
                    for item in request.data['rules_ids']:
                        try:
                            rule_obj = rules.objects.get(id=int(item))
                            rules_list.append(rule_obj)
                        except Exception as e:
                            rule_obj = rules.objects.get(id=int(item['id']))
                            rules_list.append(rule_obj)

                    listing_object.rules_ids.add(*rules_list)

            if 'photo_ids' in request.data:
                if len(request.data['photo_ids']) != 0:
                    listing_object.photo_ids = []
                    photos_list = []
                    for item in request.data['photo_ids']:
                        try:
                            photo_obj = listing_photos.objects.create(photo=item)
                            photos_list.append(photo_obj)
                        except Exception as e:
                           print ("e",e)
                           pass

                    listing_object.photo_ids.add(*photos_list)

            return response.Response({'success': 'listing updated successfully'}, status=status.HTTP_200_OK)
        except Exception as e:
            print e
        return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


class my_listings(ModelViewSet):
    queryset = listings.objects.all()
    serializer_class = listings_serializer
    filter_class = ProductFilter

    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)


    def list(self, request, *args, **kwargs):
        roomuserID = request.GET.get('userid')
        try:
            queryset = listings.objects.filter(user_id=roomuserID).order_by('-created_at')
            print queryset
            # queryset = intrested_listings.objects.all().order_by('-created_at')
            # changing code for purpose of front end team
            serializer = listings_serializer(instance=queryset, many=True)
            qs = self.filter_queryset(queryset)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = listings_serializer(page, many=True)

            return (self.get_paginated_response(serializer.data))
        except Exception as e:
            return response.Response({'error'}, status=status.HTTP_400_BAD_REQUEST)



class intrested_listings_viewset(ModelViewSet):
    queryset = intrested_listings.objects.all()
    serializer_class = intrested_listings_serializer

    def list(self, request, *args, **kwargs):
        roomuserID = request.GET.get('userid')
        try:
            queryset = intrested_listings.objects.filter(user_id=roomuserID).order_by('-created_at')
            #queryset = intrested_listings.objects.all().order_by('-created_at')
            #changing code for purpose of front end team
            serializer = intrested_listings_serializer(instance=queryset, many=True)
            qs = self.filter_queryset(queryset)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = intrested_listings_serializer(page, many=True)


            return (self.get_paginated_response(serializer.data))
        except Exception as e:
            return response.Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):

        fav_list_obj = intrested_listings()
        fav_object = intrested_listings.objects.filter(user_id=int(request.data['user_id']),
                                                       listing_id=int(request.data['listing_id']))

        if fav_object:
            return response.Response({"error": 'You already shown intrest in  this listing'},
                                     status=status.HTTP_400_BAD_REQUEST)
        try:
            if 'user_id' in request.data:
                user_object = findroomUser.objects.get(id=int(request.data['user_id']))
                fav_list_obj.user_id = user_object

            if 'listing_id' in request.data:
                listing_object = listings.objects.get(id=int(request.data['listing_id']))
                fav_list_obj.listing_id = listing_object
            fav_list_obj.save()

            notification.objects.create(notification_text=str(user_object.user.username) + ' ' + 'favourated your listing',
                                        user=listing_object.user_id,listing=listing_object,other_user=user_object)

            return response.Response({"message": 'You have successfully favourited this listing'},
                                     status=status.HTTP_200_OK)
        except Exception as e:
            print (e)
            return response.Response({'Error': e.message})



    def update(self, request, *args, **kwargs):
        favourite_object = self.get_object()
        try:
            if 'user_id' in request.data:
                user_object = findroomUser.objects.get(id=int(request.data['user_id']))
                favourite_object.user_id = user_object

            if 'listing_id' in request.data:
                listing_object = listings.objects.get(id=int(request.data['listing_id']))
                favourite_object.listing_id = listing_object

            favourite_object.save()
            return response.Response({'success': ' fav listing updated successfully'}, status=status.HTTP_200_OK)
        except Exception as e:
            print e
        return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    # def destroy(self, request, pk):
    #   try:
    #       print ("pk", pk)
    #       fav_object = intrested_listings.objects.get(user_id=int(pk))
    #       fav_object.delete()
    #       return response.Response({"success": 'You are no longer intrested in this listing'}, status=status.HTTP_200_OK)
    #   except Exception as e:
    #       print e
    #       return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

from rest_framework.viewsets import ViewSet

class Listing_Status_Change_viewset(ViewSet):
    def create(self, request, *args, **kwargs):
        print ("request.data",request.data)
        try:
            listing_obj = listings.objects.get(id=int(request.data['listing_id']))
            if listing_obj:
                listing_obj.listing_status = listing_status.objects.get(name=request.data['listing_status'])
                if request.data['listing_status'] == "Approved":
                     listing_obj.is_listable = True
                listing_obj.save()
                return response.Response({"response": "Listing Status Successfully Updated"}, status=200)
        except Exception, e:
            return response.Response({'response': e }, status=status.HTTP_400_BAD_REQUEST)

class delete_intrest(ViewSet):
    def create(self, request, *args, **kwargs):
        item = intrested_listings.objects.filter(user_id=int(request.data['roomuserID']),listing_id=int(request.data['listing_id']))

        if not item:
            return response.Response({'error': 'Favourite does not exist'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            item.delete()
            return response.Response({"success": "Favourite deleted successfully"}, status=200)

class listing_photos_viewset(ModelViewSet):
    queryset = listing_photos.objects.all()
    serializer_class = listing_photos_serializer


class country_viewset(ModelViewSet):
    queryset = country.objects.all()
    serializer_class = country_serializer

    def list(self, request, *args, **kwargs):
        queryset = country.objects.all()
        serializer = country_serializer(queryset, many=True)

        def paginate_queryset(self, queryset, view=None):
            return None

        # for temp in serializer.data:
        #     brunch_obj = Brunch.objects.filter(id = temp['id'])
        #     brunch_data = brunch_serializer(instance=brunch_obj,many=True)
        #     temp['brunch_image'] = brunch_data.data



        return response.Response(serializer.data, status=status.HTTP_200_OK)


class state_viewset(ModelViewSet):
    queryset = state.objects.all()
    serializer_class = state_serializer


class city_viewset(ModelViewSet):
    queryset = city.objects.all()
    serializer_class = city_serializer


class location_viewset(ModelViewSet):
    queryset = location.objects.all()
    serializer_class = location_serializer


class purchaseType_viewset(ModelViewSet):
    queryset = purchase_type.objects.all()
    serializer_class = purchase_type_serializer

class area_viewset(ModelViewSet):
    queryset = area.objects.all()
    serializer_class = area_serializer

class tenant_ages_viewset(ModelViewSet):
    queryset = ages_tenant.objects.all()
    serializer_class = tenant_age_serializer
    def list(self, request, *args, **kwargs):
        queryset = ages_tenant.objects.all()
        serializer = tenant_age_serializer(instance=queryset,many=True)


        def paginate_queryset(self, queryset, view=None):

            return None

        # for temp in serializer.data:
        #     brunch_obj = Brunch.objects.filter(id = temp['id'])
        #     brunch_data = brunch_serializer(instance=brunch_obj,many=True)
        #     temp['brunch_image'] = brunch_data.data



        return response.Response(serializer.data,status=status.HTTP_200_OK)


class Property_Type_viewset(ModelViewSet):
    queryset = property_type.objects.all()
    serializer_class = property_type_serializer

class Room_Type_viewset(ModelViewSet):
    queryset = room_type.objects.all()
    serializer_class = room_type_serializer

class Request_viewset(ModelViewSet):
    queryset = requests.objects.all()
    serializer_class = request_serializer

    def list(self, request, *args, **kwargs):
        try:
            roomuserID = request.GET.get('userid')

            queryset = requests.objects.filter(user_id=roomuserID).order_by('-created_at')

            serializer = request_serializer(instance=queryset, many=True)
            qs = self.filter_queryset(queryset)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = request_serializer(page, many=True)
            return (self.get_paginated_response(serializer.data))
        except Exception as e:
            return response.Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)




    def create(self, request, *args, **kwargs):
        # checking input put field are equal to database fields
        # print ("request data",request.data)
        allowed_fields = ['user_id', 'name', 'email', 'url','how_many_properties','mobile']
        mandatory_fields = ['user_id', 'name','email']
        input_keys = request.data.keys()
        temp = set(input_keys) - set(allowed_fields)

        if len(temp):
            temp_string = ','.join(str(x) for x in temp)
            return response.Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
        # implementing mandatory fields
        mandatory_temp = set(mandatory_fields) - set(input_keys)
        if len(mandatory_temp):
            temp_string = ','.join(str(x) for x in mandatory_temp)
            return response.Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
        try:  
            sender = request.data['user_id']
            if sender:
                senderobj = findroomUser.objects.get(id=int(sender))
                request_obj=requests(user_id=senderobj)
                if 'name' in request.data:
                    request_obj.name = request.data['name']
                if 'email' in request.data:
                    request_obj.email = request.data['email']
                if 'mobile' in request.data:
                    request_obj.mobile = request.data['mobile']
                if 'url' in request.data:
                    request_obj.url = request.data['url']
                if 'how_many_properties' in request.data:
                    request_obj.how_many_properties = request.data['how_many_properties']
                request_obj.save()
                return response.Response({'success': 'request has been posted successfully'}, status=status.HTTP_200_OK)
            else:
                return response.Response({'error': "Invalid User ID"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception, e:
            print (e)
            return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

from rest_framework import viewsets
class all_requests(ModelViewSet):
    queryset = requests.objects.all().order_by('-updated_at')

    serializer_class = request_serializer


    def list(self, request, *args, **kwargs):
        try:
            queryset = requests.objects.all().order_by('-updated_at')

            serializer = request_serializer(instance=queryset, many=True)
            qs = self.filter_queryset(queryset)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = request_serializer(page, many=True)
            return (self.get_paginated_response(serializer.data))
        except Exception as e:
            return response.Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)



class paid_listings(ModelViewSet):
    queryset = listings.objects.all()
    serializer_class = listings_serializer

    def list(self, request, *args, **kwargs):
        try:
            queryset = listings.objects.exclude(purchase_type__purchase_name='Basic').order_by('-updated_at')

            serializer = listings_serializer(instance=queryset, many=True)
            qs = self.filter_queryset(queryset)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = listings_serializer(page, many=True)
            return (self.get_paginated_response(serializer.data))
        except Exception as e:
            return response.Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)


class all_furnitures(ViewSet):
    def list(self,request):
        queryset = furniture_type.objects.all()
        serializer = furniture_serializer(instance=queryset,many=True)
        return response.Response(serializer.data,status=status.HTTP_200_OK)


class get_all_cleaning_basis(ViewSet):
    def list(self,request):
        queryset = cleaning.objects.all()
        serializer = cleaning_serializer(instance=queryset,many=True)
        return response.Response(serializer.data,status=status.HTTP_200_OK)

class get_bill_details(ViewSet):
    def list(self,request):
        queryset = bills_details.objects.all()
        serializer = billing_serializer(instance=queryset,many=True)
        return response.Response(serializer.data,status=status.HTTP_200_OK)


class get_photo_schemes(ViewSet):
    def list(self,request):
        queryset = photography_schemes.objects.all()
        serializer = photo_scheme_serializer(instance=queryset,many=True)
        return response.Response(serializer.data,status=status.HTTP_200_OK)

class get_scan360_schemes(ViewSet):
    def list(self,request):
        queryset = scan_schemes.objects.all()
        serializer = scan_schemes_serializer(instance=queryset,many=True)
        return response.Response(serializer.data,status=status.HTTP_200_OK)