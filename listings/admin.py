# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(listings)
admin.site.register(listing_photos)
admin.site.register(intrested_listings)
# admin.site.register(messages)
admin.site.register(country)
admin.site.register(state)
admin.site.register(city)
admin.site.register(ages_tenant)
admin.site.register(purchase_type)
admin.site.register(area)
admin.site.register(location)
admin.site.register(property_type)
admin.site.register(room_type)
admin.site.register(listing_status)
admin.site.register(requests)
admin.site.register(furniture_type)
admin.site.register(bills_details)
admin.site.register(cleaning)
admin.site.register(scan_schemes)
admin.site.register(photography_schemes)


