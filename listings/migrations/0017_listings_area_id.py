# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-04 10:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0016_area'),
    ]

    operations = [
        migrations.AddField(
            model_name='listings',
            name='area_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='listings.area'),
        ),
    ]
