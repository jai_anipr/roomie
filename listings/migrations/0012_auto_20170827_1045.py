# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-27 10:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0011_auto_20170827_1040'),
    ]

    operations = [
        migrations.CreateModel(
            name='ages_tenant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tenant_age', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='listings',
            name='gender_choice',
            field=models.CharField(blank=True, choices=[('male', 'male'), ('female', 'female'), ('either', 'either')], max_length=6, null=True),
        ),
        migrations.AddField(
            model_name='listings',
            name='age_choices',
            field=models.ManyToManyField(blank=True, to='listings.ages_tenant'),
        ),
    ]
