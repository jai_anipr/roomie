# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from users.models import findroomUser
from listings.models import *



class message(models.Model):
    sender=models.ForeignKey(findroomUser,on_delete=models.CASCADE,related_name='sender')
    recipient=models.ForeignKey(findroomUser,on_delete=models.CASCADE,related_name='recipient')
    listing_id=models.ForeignKey(listings,on_delete=models.CASCADE,related_name='listing',null=True,blank=True)
    text=models.TextField()
    text_created_at=models.DateTimeField(auto_now_add=True)

    def __str__(self):
    	return '%s' % (self.recipient)




