# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from rest_framework.viewsets import ViewSet
from rest_framework .viewsets import ModelViewSet
from .models import *
from rest_framework import request,response,status
from .serializers import *
import django_filters
from  rest_framework import filters

# class create_message(ViewSet):
#
#     def create(self, request, *args, **kwargs):
#         roomuserID = request.GET.get('userid')
#         if roomuserID:
#
#             message_ob=message(sender=roomuserID)
#
#             if 'recipient' in request.data:
# 				user_obj = findroomUser.objects.get(id=int(request.data['recipient']))
# 				message_ob.recipient = user_obj
#             if 'text' in request.data:
#                 message_ob.text = request.data['text']
#
#             message_ob.save()
#             return response.Response({'success': 'message sent successfully'}, status=status.HTTP_200_OK)
#         else:
#             return response.Response('error')


class inbox_messaging_viewset(ModelViewSet):
    queryset = message.objects.all()
    serializer_class = messaging_serializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter)
    search_fields = ('text',)
    # filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('sender',)

    def list(self, request, *args, **kwargs):
        roomuserID = request.GET.get('userid')

        if roomuserID:
            queryset = message.objects.filter(recipient=roomuserID).order_by('-text_created_at')
            serializer = messaging_serializer(queryset,many=True)
            qs = self.filter_queryset(queryset)
            page = self.paginate_queryset(qs)
            if page is not None:
                serializer = messaging_serializer(page, many=True)
                return (self.get_paginated_response(serializer.data))
        else:
            return response.Response('error',{"Pleae provide user id"})


class sent_messaging_viewset(ModelViewSet):
    queryset = message.objects.all()
    serializer_class = messaging_serializer
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    search_fields = ('text',)

    # filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('recipient',)

    def list(self, request, *args, **kwargs):
        roomuserID = request.GET.get('userid')
        try:
            if roomuserID:
                queryset = message.objects.filter(sender=roomuserID).order_by('-text_created_at')
                serializer = messaging_serializer(queryset,many=True)
                qs = self.filter_queryset(queryset)
                page = self.paginate_queryset(qs)
                if page is not None:
                    serializer = messaging_serializer(page, many=True)
                    return (self.get_paginated_response(serializer.data))
                    
        except Exception, e:
            return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)

    def create(self, request, *args, **kwargs):
        # checking input put field are equal to database fields
        # print ("request data",request.data)
        allowed_fields = ['sender', 'recipient_id', 'listing_id', 'text']
        mandatory_fields = ['sender', 'recipient_id','text']
        input_keys = request.data.keys()
        temp = set(input_keys) - set(allowed_fields)

        if len(temp):
            temp_string = ','.join(str(x) for x in temp)
            return response.Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
        # implementing mandatory fields
        mandatory_temp = set(mandatory_fields) - set(input_keys)
        if len(mandatory_temp):
            temp_string = ','.join(str(x) for x in mandatory_temp)
            return response.Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
        try:  
            sender = request.data['sender']
            if sender:
                senderobj = findroomUser.objects.get(id=int(sender))
                message_obj=message(sender=senderobj)
                if 'recipient_id' in request.data:
                  user_obj = findroomUser.objects.get(id=int(request.data['recipient_id']))
                  message_obj.recipient = user_obj
                if 'text' in request.data:
                    message_obj.text = request.data['text']
                message_obj.save()
                return response.Response({'success': 'message sent successfully'}, status=status.HTTP_200_OK)
            else:
                return response.Response({'error': "Invalid User ID"}, status=status.HTTP_400_BAD_REQUEST)
        except Exception, e:
            print (e)
            return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)



class message_conv(ModelViewSet):
    queryset = message.objects.all()
    serializer_class = messaging_serializer
    def create(self, request, *args, **kwargs):
        # sender_id=request.GET.get('sender_id')
        # recipient_id = request.GET.get('recepient_id')
        try:
            queryset = message.objects.filter(sender=request.data['sender_id'],recipient = request.data['recipient_id'])|message.objects.filter(sender=request.data['recipient_id'],recipient = request.data['sender_id']).order_by('text_created_at')

            serializer = messaging_serializer(queryset, many=True)
            # qs = self.filter_queryset(queryset)
            # page = self.paginate_queryset(qs)
            # if page is not None:
            #     serializer = messaging_serializer(page, many=True)
            return response.Response(serializer.data)
        except Exception as e :
            return response.Response({"error":e.message}, status=status.HTTP_400_BAD_REQUEST)




class latest_msgs(ModelViewSet):
    queryset = message.objects.all()
    serializer_class = messaging_serializer

    def list(self, request, *args, **kwargs):
        userid=request.GET.get('userid')
        try:

            queryset = message.objects.order_by('sender','recipient','text_created_at').filter(sender=userid).distinct('sender','recipient')|message.objects.order_by('sender','recipient','-text_created_at').filter(recipient=userid).distinct('sender','recipient')


            serializer = messaging_serializer(instance=queryset,many=True)

            return response.Response(serializer.data,status=status.HTTP_200_OK)
        # querset1 = message.objects.order_by('sender','recipient','-text_created_at').distinct('sender','recipient')
        # messages = Message.objects.order_by('fromUser', 'toUser', '-createdAt').distinct('fromUser', 'toUser')

        except Exception as e:
            return response.Response({'error':e.message},status=status.HTTP_400_BAD_REQUEST)





























        