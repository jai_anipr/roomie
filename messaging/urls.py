from django.conf.urls import include, url
from django.contrib import admin

from rest_framework.routers import SimpleRouter

from .views import *

router = SimpleRouter()

router.register(r'inbox',inbox_messaging_viewset , 'inbox')
router.register(r'messages',sent_messaging_viewset, 'messages')
#router.register(r'create_message',create_message, 'create_message')
router.register(r'message_conv',message_conv, 'message_conv')
router.register(r'latest_conv',latest_msgs, 'latest_conv')

urlpatterns = router.urls

