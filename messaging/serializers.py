from .models import *
from rest_framework.serializers import ModelSerializer
from users.serializers import *
class messaging_serializer(ModelSerializer):
	sender = FindRoomLimitSerializer(read_only=False)
	recipient = FindRoomLimitSerializer(read_only=False)
	
	class Meta:
		model = message
		fields = '__all__'

