
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from rest_framework import serializers

# Create your models here.
class profile_photos(models.Model):
    photo = models.CharField(max_length=1000,blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        
        return '%s' % (self.photo)


class work_expierence(models.Model):
    employer = models.CharField(max_length=100, null=True, blank=True)
    position = models.CharField(max_length=100, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    work_description = models.TextField(null=True, blank=True)
    currently_employeed = models.NullBooleanField(default=True,null=True, blank=True)

    def __str__(self):
        return '%s' % (self.employer)


class verify_status(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.name)

class verification_user(models.Model):
    emirates_id = models.IntegerField(null=True,blank=True)
    expiry_date =  models.DateField(null=True,blank=True)
    id_front_photo = models.CharField(null=False, default="", max_length=1000, blank=True)
    id_back_photo = models.CharField(null=False, default="", max_length=1000, blank=True)
    passport_id = models.IntegerField(null=True, blank=True)
    passport_expiry_date = models.DateField(null=True, blank=True)
    passport_front_photo = models.CharField(null=False, default="", max_length=1000, blank=True)
    passport_back_photo = models.CharField(null=False, default="", max_length=1000, blank=True)
    approve_status = models.ForeignKey(verify_status, blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    def __str__(self):
        return '%s' % (self.emirates_id)

class findroomUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_viewable = models.NullBooleanField(blank=True,null=True,default=True)
    phone = models.CharField(max_length=50,null=True,blank=True)
    social_login_image = models.TextField(default="", blank=True)
    social_login_email = models.TextField(default="", blank=True)
    activate_token = models.CharField(null=False, default="", max_length=500, blank=True)
    forgot_pass_token = models.CharField(null=False, default="", max_length=500, blank=True)
    #dob = models.DateField(null=True,blank=True)
    dob = models.CharField(null=False, default="", max_length=50, blank=True)
    # city = models.ForeignKey(city,null=True,blank=True)
    prefered_location = models.ForeignKey("listings.city", null=True, blank=True)
    gender = models.CharField(max_length=100,null=True,blank=True)
    # deactivate_account = models.NullBooleanField(blank=True,null=True,default=True)
    notify_email = models.NullBooleanField(blank=True,null=True,default=True)
    notify_sms = models.NullBooleanField(blank=True,null=True,default=True)
    cover_image = models.CharField(null=False, default="", max_length=1000, blank=True)
    description = models.TextField(null=False,blank=True)
    passport = models.CharField(max_length=100,null=True,blank=True)
    national_id = models.CharField(max_length=100,null=True,blank=True)
    budgetmaxvalue = models.IntegerField(null=True,blank=True)
    move_in_date = models.DateField(null=True,blank=True)
    budgetminvalue = models.IntegerField(null=True,blank=True)
    work_exp = models.ManyToManyField(work_expierence,blank=True)
    is_facebook_uesr = models.BooleanField(default=False)
    # driving_license = models.CharField(max_length=100,null=True,blank=True)
    user_verification = models.ForeignKey(verification_user,null=True,blank=True)

    photo_ids = models.ManyToManyField(profile_photos,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        
        return '%s' % (self.user)

class UserSessions(models.Model):
    user = models.ForeignKey(User,related_name='user_sessions')
    session = models.ForeignKey(Session, related_name='user_sessions',
                                   on_delete=models.CASCADE)

    def __str__(self):
        return '%s - %s' % (self.user, self.session.session_key)



