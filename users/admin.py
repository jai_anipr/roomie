# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(findroomUser)
admin.site.register(work_expierence)
admin.site.register(profile_photos)
admin.site.register(verification_user)
admin.site.register(verify_status)
