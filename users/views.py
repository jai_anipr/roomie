
# import required libraries and dependencies
from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from serializers import *
from .models import *
from django.contrib.sessions.models import Session
from rest_framework.decorators import detail_route
from django.contrib.auth import authenticate, login, logout
from rest_framework import status, viewsets
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token
from listings.models import city
# imports from settings.py
from roomi.settings import *
import django_filters
from  rest_framework import filters
# Permission classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.authentication import BasicAuthentication
from .permissions import *
from django.contrib.auth.signals import user_logged_out
from rest_framework_jwt.settings import api_settings
from datetime import datetime, timedelta
import jwt
import json, requests, ast
import datetime
#mail sending plugins
from django.template import Context, RequestContext
from django.template.loader import get_template
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string


jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

import urllib
import json

def GetCurrentLocation(request):
  try:
	client_id = request.META.get('REMOTE_ADDR')
	socket = urllib.urlopen('http://freegeoip.net/json/%s' %client_id)
	json_text = socket.read()
	socket.close()
	js_object = json.loads(json_text)
	city = js_object['city']
	state = js_object['region_name']
  except Exception, e:
	print (e)
	city = ''
	state = ''

  return city, state

def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	print("ip address",ip)
	return ip


def create_token(userid):
	payload = {
	'sub': str(userid),
		'iat': datetime.datetime.now(),
		'exp': datetime.datetime.now() + timedelta(days=3000)
	}
	token = jwt.encode(payload, SECRET_KEY)
	return token.decode('unicode_escape')

# sending activation main method
def send_activation_mail(request,data, token, mail_type):
	ctx = {
				"email": str(data['email']),
				"token": token,
				"server_url": request.get_host()
			}
	if mail_type == 'activation_mail':
		# activation template
		subject = "Account Activation"
		pwdtemplate = get_template('registeremail.html')

	elif mail_type == 'signup_mail':
		# forgot template
		subject = "Welcome to Findroom.ae"
		# pwdtemplate = get_template('confirmaccount.html')
		htmlt = render_to_string('confirmaccount.html', ctx)
	elif mail_type == 'forgot_mail':
		subject = "Password Reset Request"
		pwdtemplate = get_template('forgotpassword.html')
		htmlt = render_to_string('forgotpassword.html', ctx)
	else:
		pwdtemplate = get_template('send_activation.html')
	# htmlt = pwdtemplate.render(content)
	to = [str(data['email'])]
	by = "noreply@findaroom.ae"
	msg = EmailMultiAlternatives(subject, "", by, to)
	msg.attach_alternative(htmlt, "text/html")
	msg.send()
from listings.models import listings,intrested_listings
from listings.serializers import listings_serializer

class RoomUsersViewSet(viewsets.ModelViewSet):
	queryset = findroomUser.objects.all()
	serializer_class = FindRoomSerializer

	# authentication_classes = (BasicAuthentication,)
	# permission_classes = (UserPermission,)

	def list(self, request,*args):
		queryset = findroomUser.objects.all()
		serializer = FindRoomSerializer(queryset, many=True)
		#qs = self.filter_queryset(queryset)
		page = self.paginate_queryset(queryset)
		if page is not None:
			serializer = FindRoomSerializer(page, many=True)
		return (self.get_paginated_response(serializer.data))

	def create(self, request,*args):
		# checking input put field are equal to database fields
		allowed_fields = ['first_name', 'last_name', 'email', 'password']
		mandatory_fields = ['email', 'password']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
		# implementing mandatory fields
		mandatory_temp = set(mandatory_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in mandatory_temp)
			return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
		try:
			user = User.objects.create_user(request.data['email'], request.data['email'], request.data['password'])
			user.is_active = True
			try:
				user.save()
				user_details = findroomUser(user=user)
				user_details.save()
				c_group = Group.objects.get(name='User')
				user.groups.add(c_group)
				token = create_token(user.id)
				send_activation_mail(request,request.data, token,'signup_mail')
				return Response({'success': {'message':'You have been signup successfully !',"user_id":user_details.id,"email":request.data['email']}}, status=status.HTTP_200_OK)
			except Exception as e:
				print(e)
				return Response({'message': e.message}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response({'message': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)

	def retrieve(self, request, pk=None):
		userid = request.GET.get('userids')
		try:
			queryset = findroomUser.objects.filter(pk=pk)
			serializer = FindRoomSerializer(queryset,many=True)
			for i in serializer.data:
				my_listings = listings.objects.filter(user_id=pk,is_listable=True)
				list_data = listings_serializer(instance=my_listings,many=True)
				i['listings_data']= list_data.data
				if userid:
					for listing in i['listings_data']:
						print listing['id']
					
						fav_ob = intrested_listings.objects.filter(listing_id=listing['id'],user_id=int(userid))
						if fav_ob:
							listing['i_favourited_this']=True
						else:
							listing['i_favourited_this']=False

					

			return Response(serializer.data[0], status=200)
		except Exception,e:
			print("error",e)
			return Response({'error': 'Invalid User Id'}, status=status.HTTP_400_BAD_REQUEST)
	

	def update(self, request, pk=None):
		try:
			# checking input put field are equal to database fields
			allowed_fields = ['first_name', 'last_name', 'password','email','national_id','id','social_login_email','photo_ids','updated','description','created','dob','gender','position','is_facebook_user','user_verification',
							  'confirmpassword','notify_sms','notify_email','is_viewable','prefered_location', 'is_facebook_uesr','listings_data','verify_statuss','current_password','budgetmaxvalue','budgetminvalue','work_exp','newpassword','deactivate_account','move_in_date','current_password','mobile', 'profile_pic','user_id','profile_picture','employer','passport','activate_token','work_description','social_login_image','phone','user','is_viewable','forgot_pass_token','cover_image']

			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)

			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# checking password and confirm password field values
			findaroom_obj = findroomUser.objects.get(pk=pk)
			password = request.data.get('newpassword', None)
			print password
			c_password = request.data.get('confirmpassword', None)
			print  c_password
			if password and c_password:
				if password != c_password:
					return Response({'error': 'password and confirm password did not match'}, status=status.HTTP_400_BAD_REQUEST)
				else:
					current_user = User.objects.get(id=findaroom_obj.user.id)
					print current_user.id
					success = current_user.check_password(request.data['current_password'])


					if success:
						#current_user.set_password((request.data['new_password']))
						#print request.data['newpassword']
						current_user.set_password(request.data['newpassword'])
						current_user.save()
						return Response({'sucess':'password changed'},status = status.HTTP_200_OK)
						print 'hello'
					else:
						return Response({'error': 'old Password incorrect'}, status=status.HTTP_400_BAD_REQUEST)
			if findaroom_obj:
				user_obj = User.objects.get(id=findaroom_obj.user.id)
				if 'national_id' in request.data:
					findaroom_obj.national_id = request.data['national_id']
				if 'gender' in request.data:
					findaroom_obj.gender = request.data['gender']
				if 'phone' in request.data:
					findaroom_obj.phone = request.data['phone']
				if 'deactivate_account' in request.data:
					findaroom_obj.deactivate_account = request.data['deactivate_account']
					if request.data['deactivate_account'] == "False":
						user_obj.is_active = False
					else:
						user_obj.is_active = True

				if 'description' in request.data:
					findaroom_obj.description = request.data['description']
				if 'user' in request.data:
					if "first_name" in request.data['user']:	
						user_obj.first_name = request.data['user']['first_name']
				if 'user' in request.data:
					if "last_name" in request.data['user']:	
						user_obj.last_name = request.data['user']['last_name']
				if 'email' in request.data:
					user_obj.email = request.data['email']
				if 'dob' in request.data:
					findaroom_obj.dob = request.data['dob']
				if 'is_viewable' in request.data:
					findaroom_obj.is_viewable = request.data['is_viewable']

				if 'notify_email' in request.data:
					findaroom_obj.notify_email = request.data['notify_email']
				if 'notify_sms' in request.data:
					findaroom_obj.notify_sms = request.data['notify_sms']
				if 'prefered_location' in request.data:
					city_ob = city.objects.get(id=request.data['prefered_location'])
					findaroom_obj.prefered_location = city_ob

				if 'budgetmaxvalue' in request.data:
					findaroom_obj.budgetmaxvalue = request.data['budgetmaxvalue']
				if 'budgetminvalue' in request.data:
					findaroom_obj.budgetminvalue = request.data['budgetminvalue']



				if 'move_in_date' in request.data:
					findaroom_obj.move_in_date = request.data['move_in_date']


				if 'cover_image' in request.data:
					cover_image = request.data['cover_image']
					try:
						res = cloudinary.uploader.upload(cover_image[0])
						findaroom_obj.cover_image = res['url']
					except Exception, e:
						print (e)


				if 'verify_statuss' in request.data:
					verify_data = request.data['verify_statuss']

					verify_obj=verification_user()

					if 'emirates_id' in verify_data:
						verify_obj.emirates_id = verify_data['emirates_id']
					if 'expiry_date' in verify_data:
						verify_obj.expiry_date = verify_data['expiry_date']
					if 'id_front_photo' in verify_data:
						verify_obj.id_front_photo = verify_data['id_front_photo']
					if 'id_back_photo' in verify_data:
						verify_obj.id_back_photo = verify_data['id_back_photo']
					if 'passport_number' in verify_data:
						verify_obj.passport_id = verify_data['passport_number']
					if 'passport_expiry_date' in verify_data:
						verify_obj.passport_expiry_date = verify_data['passport_expiry_date']
					if 'passport_front_photo' in verify_data:
						verify_obj.passport_front_photo = verify_data['passport_front_photo']
					if 'passport_back_photo' in verify_data:
						verify_obj.passport_back_photo = verify_data['passport_back_photo']

					k=verify_status.objects.get(name='Pending')
					print k
					verify_obj.approve_status = k


					verify_obj.save()
					findaroom_obj.user_verification = verify_obj

				# print ("user_obj",user_obj)
				user_obj.save()
				findaroom_obj.save()

				if 'photo_ids' in request.data:
					findaroom_obj.photo_ids = []
					photos_list = []
					for item in request.data['photo_ids']:
						photo_obj = profile_photos.objects.create(photo=item)
						photos_list.append(photo_obj)
					findaroom_obj.photo_ids.add(*photos_list)

				if 'work_exp' in request.data:
					
					findaroom_obj.work_exp = []
					work_list = []
					for work_data in request.data['work_exp']:
					#work_data = request.data['work_exp']
						work_obj = work_expierence()
						if 'employer' in work_data:
							work_obj.employer = work_data['employer']
						if 'position' in work_data:
							work_obj.position = work_data['position']
						if 'start_date' in work_data:
							work_obj.start_date = work_data['start_date']
						if 'end_date' in work_data:
							work_obj.end_date = work_data['end_date']
						if 'work_description' in work_data:
							work_obj.work_description = work_data['work_description']
						if 'currently_employeed' in work_data:
							work_obj.currently_employeed = work_data['currently_employeed']

						for i in work_list:
							if work_obj.employer == i.employer:
								pass

						else:
							work_obj.save()
							work_list.append(work_obj)

					findaroom_obj.work_exp.add(*work_list)

				return Response({'response': 'successfully updated user information'}, status=status.HTTP_200_OK)
			else:
				return Response({'error': 'Invalid User'}, status=status.HTTP_400_BAD_REQUEST)                
		except Exception as e:
			print (e)
			return Response({'error': 'something went wrong'}, status=status.HTTP_400_BAD_REQUEST)

	def destroy(self, request,pk=None):
		try:
			user = findroomUser.objects.get(id=pk)
			user_obj = User.objects.get(id=user.user.id)
			if user_obj:
				user_obj.delete()
				user.delete()
			return Response({'Success': 'Successfully deleted the User.'}, status=status.HTTP_200_OK)
		except Exception, e:
			print (e)
			return Response({"error":e.message}, status=401)

class LoginViewset(ViewSet):
	def create(self, request):
		# city, state = GetCurrentLocation(request)
		# request_ip = get_client_ip(request)

		# checking input put field are equal to database fields
		allowed_fields = ['email', 'password','message','user_id']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		# mandatory_temp = set(allowed_fields) - set(input_keys)
		# if len(mandatory_temp):
		# 	temp_string = ','.join(str(x) for x in allowed_fields)
		# 	return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:
			user = authenticate(username=request.data['email'], password=request.data['password'])
			if user is not None:
				login(request, user)
				if request.user.is_authenticated():
					session = Session.objects.filter(session_key=request.session.session_key)
					if session:
						user_session = UserSessions.objects.create(user=request.user, session=session[0])

				payload = jwt_payload_handler(user)

				User = findroomUser.objects.get(user=user.id)
				if api_settings.JWT_ALLOW_REFRESH:
					payload['orig_iat'] = timegm(
						datetime.utcnow().utctimetuple()
					)
				token =  jwt_encode_handler(payload)
				user_data = {}
				user_data['first_name'] = user.first_name
				user_data['last_name'] = user.last_name
				user_data['cover_image'] = User.cover_image
				user_data['user_id'] = User.id
				user_data['email'] = user.email
				request.session['user_id'] = user.id
			
				return Response({'token': token,
								 'user':[user_data],
								 'groups': user.groups.all().values_list('name', flat=True),
								 'message': 'User successfully logged in'}, status=200)
			else:
				return Response({"message": "Invalid Credentials !"}, status=401)
		except Exception as e:
			print ("error",e)
			return Response({"error": e.message}, status=400)

class LogoutViewset(ViewSet):
	def list(self, request):
		try:
			user = getattr(request, 'user', None)
			if hasattr(user, 'is_authenticated') and not user.is_authenticated():
				user = None
			user_logged_out.send(sender=user.__class__, request=request, user=user)

			request.session.flush()
			if hasattr(request, 'user'):
				from django.contrib.auth.models import AnonymousUser
				request.user = AnonymousUser()
				return Response({"success": "Logged out successfully !"}, status=200)
		except Exception as e:
			return Response({"error": e.message}, status=400)


class ForgotPasswordViewset(ViewSet):
	def create(self,request):
		try:
			# ---- parameter verifications ----
			allowed_fields = ['email']
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(allowed_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in allowed_fields)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			try:
				user = User.objects.get(email=request.data['email'])
			except Exception as e:
				print e
				return Response({'error': 'no account found with this email ...'}, status=status.HTTP_400_BAD_REQUEST)

			if not user:
				return Response({'error': 'no email found to this account ...'}, status=status.HTTP_400_BAD_REQUEST)
			# sending forgot password link
			obj = {'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email}
			token = create_token(user.id)
			try:
				FrUser = findroomUser.objects.get(user_id=user)
				FrUser.forgot_pass_token = token
				FrUser.save()
				send_activation_mail(request, obj, token, 'forgot_mail')
				return Response({'message': 'forgot password link successfully sent'}, status=status.HTTP_200_OK)
			except Exception as e:
				print (e)
				return Response({'error': 'no user account found with this email'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print (e)
			return Response({'error': 'some thing went wrong ...'}, status=status.HTTP_400_BAD_REQUEST)

class ForgotPasswordProcessViewset(ViewSet):
	def create(self,request):
		try:
			# ---- parameter verifications ----
			allowed_fields = ['email', 'token', 'password']
			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(allowed_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in allowed_fields)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)
			try:
				user = User.objects.get(email=request.data['email'])
			except Exception as e:
				return Response({'error': 'no account found with this email'}, status=status.HTTP_400_BAD_REQUEST)
			try:
				FrUser = findroomUser.objects.get(forgot_pass_token=request.data['token'], user_id=user)
				user.set_password(request.data['password'])
				user.save()
				FrUser.forgot_pass_token = ''
				FrUser.save()
				return Response({'message': 'successfully updated new password'}, status=status.HTTP_200_OK)
			except Exception as e:
				print e
				return Response({'error': 'no token or user found'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return Response({'error': 'no account found with this email'}, status=status.HTTP_400_BAD_REQUEST)

class FacebookLoginViewset(ViewSet):
	def create(self,request):
		try:

			# allowed fields 
			allowed_fields = ['id','email', 'name', 'first_name','last_name','picture','gender']

			# mandatory fields
			mandatory_fields = ['id']

			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(mandatory_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in mandatory_temp)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# checking whether user already exist or not
			if 'id' in request.data:

				# if user exist
				try:
					user_obj = User.objects.get(username=request.data['id'])
					if "first_name" in request.data:
						user_obj.first_name = request.data['first_name']
					if 'user_obj':
						findUser = findroomUser.objects.get(user=user_obj.id)
						payload = jwt_payload_handler(user_obj)
						if api_settings.JWT_ALLOW_REFRESH:
							payload['orig_iat'] = timegm(
								datetime.utcnow().utctimetuple()
							)
						token =  jwt_encode_handler(payload)

						user_data = {}
						user_data['first_name'] = user_obj.first_name
						user_data['last_name'] = user_obj.last_name
						user_data['user_id'] = findUser.id
						user_data['cover_image'] = findUser.cover_image
						user_data['email'] = user_obj.email
						return Response({'token': token,
									 'user':user_data,
									 'id' : findUser.id,
									 'groups': user_obj.groups.all().values_list('name', flat=True),
									 'message': 'User successfully logged in'}, status=200)
				except Exception,e:
					print ("e",e)
					user_data = {}
					# if user doesn't exist
					try:
						user = User.objects.create_user(request.data['id'],request.data['id'])
						user.is_active = True
						try:
							if "email" in request.data:
								user.email = request.data['email']
							if "first_name" in request.data:
								user.first_name = request.data['first_name']
							if "last_name" in request.data:
								user.last_name = request.data['last_name']
							user.save()
							user_details = findroomUser(user=user)
							if 'mobile' in request.data:
								user_details.mobile = request.data['mobile']
							if "gender" in request.data:
								user_details.gender = request.data['gender']
							if 'picture' in request.data:
								user_details.cover_image = request.data['picture']['data']['url']
							user_details.is_facebook_uesr = True
							user_details.save()

							c_group = Group.objects.get(name='User')
							user.groups.add(c_group)
							token = create_token(user.id)
							user_details.save()
							user_data = {}
							user_data['first_name'] = user.first_name
							user_data['last_name'] = user.last_name
							user_data['user_id'] = user_details.id
							user_data['cover_image'] = user_details.cover_image
							user_data['email'] = user.email
							return Response({'token': token,
										 'user':user_data,
										 'id' : user_details.id,
										 'groups': user.groups.all().values_list('name', flat=True),
										 'message': 'User successfully logged in'}, status=200)
						except Exception as e:
							print ("error e",e)
							return Response({'error': 'Email already exists'}, status=status.HTTP_400_BAD_REQUEST)
					except Exception as e:
						print("last except part",e)
						return Response({'error': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)
					return Response({'token': token.key,'user':temp,'id':user_details.id,'message': 'user successfully logged in'}, status=status.HTTP_200_OK)
		except Exception as e:
			return Response({'error': 'unable to process...'}, status= status.HTTP_400_BAD_REQUEST)




class LinkedinLoginViewset(ViewSet):
	def create(self,request):
		try:

			# allowed fields
			allowed_fields = ['id','email', 'headline','name','numConnections','pictureUrl', 'first_name','last_name','gender']

			# mandatory fields
			mandatory_fields = ['id']

			input_keys = request.data.keys()
			temp = set(input_keys) - set(allowed_fields)
			if len(temp):
				temp_string = ','.join(str(x) for x in temp)
				return Response({'error': 'invalid fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# implementing mandatory fields
			mandatory_temp = set(mandatory_fields) - set(input_keys)
			if len(mandatory_temp):
				temp_string = ','.join(str(x) for x in mandatory_temp)
				return Response({'error': 'mandatory fields: '+ temp_string}, status=status.HTTP_400_BAD_REQUEST)

			# checking whether user already exist or not
			if 'id' in request.data:

				# if user exist
				try:
					user_obj = User.objects.get(username=request.data['id'])
					if "first_name" in request.data:
						user_obj.first_name = request.data['first_name']
					if 'user_obj':
						findUser = findroomUser.objects.get(user=user_obj.id)
						payload = jwt_payload_handler(user_obj)
						if api_settings.JWT_ALLOW_REFRESH:
							payload['orig_iat'] = timegm(
								datetime.utcnow().utctimetuple()
							)
						token =  jwt_encode_handler(payload)

						user_data = {}
						user_data['first_name'] = user_obj.first_name
						user_data['last_name'] = user_obj.last_name
						user_data['user_id'] = findUser.id
						user_data['cover_image'] = findUser.cover_image
						user_data['email'] = user_obj.email
						return Response({'token': token,
									 'user':user_data,
									 'id' : findUser.id,
									 'groups': user_obj.groups.all().values_list('name', flat=True),
									 'message': 'User successfully logged in'}, status=200)
				except Exception,e:
					print ("e",e)
					user_data = {}
					# if user doesn't exist
					try:
						user = User.objects.create_user(request.data['id'],request.data['id'])
						user.is_active = True
						try:
							if "email" in request.data:
								user.email = request.data['email']
							if "first_name" in request.data:
								user.first_name = request.data['first_name']
							if "last_name" in request.data:
								user.last_name = request.data['last_name']
							user.save()
							user_details = findroomUser(user=user)
							if 'mobile' in request.data:
								user_details.mobile = request.data['mobile']
							if "gender" in request.data:
								user_details.gender = request.data['gender']
							if 'picture' in request.data:
								user_details.cover_image = request.data['picture']['data']['url']
							user_details.is_facebook_user = True
							user_details.save()
							c_group = Group.objects.get(name='User')
							user.groups.add(c_group)
							token = create_token(user.id)
							user_details.save()
							user_data = {}
							user_data['first_name'] = user.first_name
							user_data['last_name'] = user.last_name
							user_data['user_id'] = user_details.id
							user_data['cover_image'] = user_details.cover_image
							user_data['email'] = user.email
							return Response({'token': token,
										 'user':user_data,
										 'id' : user_details.id,
										 'groups': user.groups.all().values_list('name', flat=True),
										 'message': 'User successfully logged in'}, status=200)
						except Exception as e:
							print ("error e",e)
							return Response({'error': 'Email already exists'}, status=status.HTTP_400_BAD_REQUEST)
					except Exception as e:
						print("last except part",e)
						return Response({'error': 'It seems you already have an account !'}, status=status.HTTP_400_BAD_REQUEST)
					return Response({'token': token.key,'user':temp,'id':user_details.id,'message': 'user successfully logged in'}, status=status.HTTP_200_OK)
		except Exception as e:
			return Response({'error': 'unable to process...'}, status= status.HTTP_400_BAD_REQUEST)

class adminlogin(ViewSet):
	def create(self,request):
		allowed_fields = ['email', 'password','role']
		input_keys = request.data.keys()
		temp = set(input_keys) - set(allowed_fields)

		if len(temp):
			temp_string = ','.join(str(x) for x in temp)
			return Response({'error': 'invalid fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		# implementing mandatory fields
		mandatory_temp = set(allowed_fields) - set(input_keys)
		if len(mandatory_temp):
			temp_string = ','.join(str(x) for x in allowed_fields)
			return Response({'error': 'mandatory fields: ' + temp_string}, status=status.HTTP_400_BAD_REQUEST)

		try:

				user = authenticate(username=request.data['email'], password=request.data['password'])

				if user is not None:

						login(request, user)
						if request.user.is_authenticated():
							session = Session.objects.filter(session_key=request.session.session_key)
							if session:
								user_session = UserSessions.objects.create(user=request.user, session=session[0])
						payload = jwt_payload_handler(user)
						a = user.groups.filter(name='Manager').count()
						print (a)
						if a == 1:
							User = findroomUser.objects.get(user=user.id)
							if api_settings.JWT_ALLOW_REFRESH:
								payload['orig_iat'] = timegm(
									datetime.utcnow().utctimetuple()
								)
							token = jwt_encode_handler(payload)
							user_data = {}
							user_data['first_name'] = user.first_name
							user_data['last_name'] = user.last_name
							user_data['user_id'] = user.id
							user_data['email'] = user.email
							request.session['auth_user_id'] = user.id
							request.session['buddy_id'] = User.id
							print ("request session", request.session)
							return Response({'token': token,
											 'user': [user_data],
											 'id': User.id,
											 'groups': user.groups.all().values_list('name', flat=True),
											 'message': 'User successfully logged in'}, status=200)
				else:
					return Response({"message": "Invalid Credentials !"}, status=401)

		except Exception as e:
			print ("error", e)
		return Response({"error",e}, status=400)

class verify_status_change(viewsets.ViewSet):
	def create(self,request, *args, **kwargs):
		try:
			user_obj = findroomUser.objects.get(id=int(request.data['user_id']))
			if user_obj:
				user_obj.user_verification.approve_status = verify_status.objects.get(name = request.data['change_status'])

				user_obj.user_verification.save()
				user_obj.save()

				print 'verified'

				print user_obj.user_verification.approve_status.id

				# if request.data['change_status']=='aprooved':
				# 	pass

				return Response({"response": "user verification Status Successfully Updated"}, status=200)
		except Exception as e:
			return Response({'response':e}, status=status.HTTP_400_BAD_REQUEST)



