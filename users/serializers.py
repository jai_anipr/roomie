
# import required libraries
from models import *
from rest_framework import serializers
from django.contrib.auth.models import Group
from rest_framework.response import Response

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = "__all__"

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ('id','first_name', 'last_name', 'email', 'username', 'groups',)


class UserSerializerLimit(serializers.ModelSerializer):
    
    class Meta:
        model = User
        fields = ('first_name', 'last_name',)


class FindRoomSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=False)

    class Meta:
        model = findroomUser
        depth =2
        fields = "__all__"


class FindRoomLimitSerializer(serializers.ModelSerializer):
    
    user = UserSerializerLimit(read_only=False)
    
    class Meta:
        model = findroomUser
        depth =2
        fields = ('id','cover_image','user',)
