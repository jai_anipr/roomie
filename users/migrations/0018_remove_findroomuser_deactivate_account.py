# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-06 12:02
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0017_auto_20170906_1120'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='findroomuser',
            name='deactivate_account',
        ),
    ]
