# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-10-20 12:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0027_listings_is_listable'),
        ('users', '0023_auto_20171016_0951'),
    ]

    operations = [
        migrations.AddField(
            model_name='findroomuser',
            name='notify_email',
            field=models.NullBooleanField(default=True),
        ),
        migrations.AddField(
            model_name='findroomuser',
            name='notify_sms',
            field=models.NullBooleanField(default=True),
        ),
        migrations.AddField(
            model_name='findroomuser',
            name='prefered_location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='listings.city'),
        ),
        migrations.AddField(
            model_name='verification_user',
            name='passport_back_photo',
            field=models.CharField(blank=True, default='', max_length=1000),
        ),
        migrations.AddField(
            model_name='verification_user',
            name='passport_expiry_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='verification_user',
            name='passport_front_photo',
            field=models.CharField(blank=True, default='', max_length=1000),
        ),
        migrations.AddField(
            model_name='verification_user',
            name='passport_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
