# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-04 13:07
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_findroomuser_dob'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='findroomuser',
            name='gender',
        ),
    ]
