# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-10-12 09:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0020_findroomuser_is_facebook_uesr'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile_photos',
            name='photo',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
