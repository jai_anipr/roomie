# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-28 12:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_remove_findroomuser_driving_license'),
    ]

    operations = [
        migrations.AlterField(
            model_name='findroomuser',
            name='activate_token',
            field=models.CharField(blank=True, default='', max_length=500),
        ),
        migrations.AlterField(
            model_name='findroomuser',
            name='cover_image',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='findroomuser',
            name='dob',
            field=models.CharField(blank=True, default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='findroomuser',
            name='forgot_pass_token',
            field=models.CharField(blank=True, default='', max_length=500),
        ),
    ]
