
#import required libraries
from .views import *
from rest_framework.routers import format_suffix_patterns, url
from rest_framework.routers import SimpleRouter
from users import views
from rest_framework_jwt.views import verify_jwt_token
from django.contrib import admin



# routing starts here
login_view_set = LoginViewset.as_view({
	'post': 'create'
})
logout_view_set = LogoutViewset.as_view({
	'get': 'list'
})
forgot_password = ForgotPasswordViewset.as_view({
	'post': 'create'
})
users_facebook_login = FacebookLoginViewset.as_view({
	'post': 'create'
})

users_forgot_process = ForgotPasswordProcessViewset.as_view({
    'post': 'create'
})

router = SimpleRouter()
router.register(r'users', views.RoomUsersViewSet, 'users')
router.register(r'linkedin',views.LinkedinLoginViewset,'linkedin')
router.register(r'admin_login',adminlogin , 'admin_login')
router.register(r'verify_status_change',verify_status_change , 'verify_status_change')


urlpatterns = router.urls

urlpatterns += format_suffix_patterns([
    url(r'^login$',login_view_set, name='login-view-set'),
    url(r'^logout$',logout_view_set, name='logout-view-set'),
    url(r'^forgot-password$', forgot_password, name='forgot-password'),
    url(r'^forgot-password-process$',users_forgot_process, name='users_forgot_process'),
    url(r'^facebook-login$', users_facebook_login, name='users_facebook_login'),

    url(r'^api-token-verify/', verify_jwt_token),
])
