adminapp.controller("CountryCtrl", function($scope, Country, $stateParams, $state, $http,$auth){
	// CountryCtrl controller calling 

	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// fetching countries
	Country.getCountries().then(function(success){
		console.log("data", success);
		$scope.countries = success.results;
		$scope.countrylength = $scope.countries.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function (error){
		console.log("data", error)
	})
	// end

	// delete country function
	$scope.delete = function(country){
		console.log("id", country.id);
		
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			Country.deleteCountry(country.id).then(function(success){
				console.log("data", success);
				$state.reload()
				// $scope.countries = success;
			}, function (error){
				console.log("data", error)
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		});
	}
	
	// end
	//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/country/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.countries = success.data.results;
	    	$scope.countrylength = $scope.countries.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/country/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.countries = success.data.results;
	    	$scope.countrylength = $scope.countries.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});