adminapp.controller("CityCtrl", function($scope, City, $http,$auth, $state){
	// CityCtrl controller calling
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    } 

	// fetching city data
	City.getCities().then(function(success){
		console.log("data city", success);
		$scope.cities = success.results
	    $scope.citylength = $scope.cities.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("data error", error)
	})
	// end 
	$scope.deleteCity = function(city){
		/*City.deleteCity(city.id).then(function(success){
			console.log("data city", success);
		}, function(error){
			console.log("data error", error)
		})*/
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			City.deleteCity(city.id).then(function(success){
				console.log("data city", success);
				$state.reload();
			}, function(error){
				console.log("data error", error)
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		});
	}

	//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/city/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.cities = success.data.results;
	    	$scope.citylength = $scope.cities.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/city/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.cities = success.data.results;
	    	$scope.citylength = $scope.cities.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});