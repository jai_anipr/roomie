adminapp.controller("PaidListingsCtrl", function($scope, Listings, $state, $http,$auth){
	// BrunchCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

	Listings.getPaidListings().then(function(success){
		console.log("success",success);
		$scope.listings = success.results;
		$scope.listingslength = $scope.listings.length;
	  	$scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	},function(error){
		console.log("error",error)
	})

	$scope.Delete = function(listing){
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			Listings.deleteListing(listing.id).then(function(success){
	    		console.log("success", success);
	    		swal("Deleted!", "Deleted Successfully!.", "success");
		  		$state.reload()
	    	},function(error){
	    		console.log("error", error);
	    	})
		});
	}
	//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/paid_listings/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.listings = success.data.results;
	    	$scope.listingslength = $scope.listings.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/paid_listings/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.listings = success.data.results;
	    	$scope.listingslength = $scope.listings.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});