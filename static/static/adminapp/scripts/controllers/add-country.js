adminapp.controller("AddCountryCtrl", function($scope, Country, $state,$auth){
	// AddCountryCtrl controller calling
	//Adding authuntication 
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// posting country data
	$scope.data = {name : ''};
	$(".load-button").hide();
	$scope.AddCountry = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		// console.log("data", $scope.data);
		Country.createCountry($scope.data).then(function(success){
			console.log("data success", success);
			$(".load-button").hide();
			$(".btn-primary").show();
			$state.go("country");
			swal("Success!", "Created Successfully!", "success");
		}, function(error){
			console.log("data error", error);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}	
});