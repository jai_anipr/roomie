adminapp.controller("EditInterestCtrl", function($scope, $stateParams, Interest,$auth,$state){
	// EditInterestCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	console.log("id", $stateParams.interestId);
	// fetching interest data
	Interest.getInterest($stateParams.interestId).then(function(success){
		console.log("data", success);
		$scope.data = success
		
	}, function(error){
		console.log("data", error);

	})
	// end
	// updateting Interest data
	$scope.editInterest = function(){
		Interest.updateInterest($stateParams.interestId, $scope.data).then(function(success){
			console.log("data", success);
			$state.go('interest')
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("data", error);
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});