adminapp.controller("RequestsCtrl", function($scope,Request,$auth, $http){
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	Request.getRequests().then(function(success){
      $scope.requests = success.results;
      $scope.totallength = success.count;
      if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
    },function(error){
    });
    $scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/all_requests/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.requests = success.data.results;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/all_requests/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.requests = success.data.results;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});