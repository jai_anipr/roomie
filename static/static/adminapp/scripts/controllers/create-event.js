adminapp.controller("CreateEventCtrl", function($scope, Event,$auth, $http){

	// CreateEventCtrl controller calling
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

    // Fetching brunches from back end
	  $http.get("/api/v1/brunches/")
      .then(function(response) {
           $scope.brunches = response.data.results;
      });

       // Fetching Event Type from back end
	  $http.get("/api/v1/event-types/")
      .then(function(response) {
           $scope.event_types = response.data.results;
      });

	$scope.EventAdd = function(){
		console.log("data", $scope.data);
		Event.createEvent($scope.data).then(function(success){
			console.log("success", success)
		}, function(error){
			console.log("error", error)
		})
	}
});