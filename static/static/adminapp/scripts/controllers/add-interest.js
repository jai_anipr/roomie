adminapp.controller("AddInterestCtrl", function($scope, Interest, $state,$auth){
	// AddInterestCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// addInterest data
	$(".load-button").hide();
	$scope.addInterest = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		Interest.createInterest($scope.data).then(function(success){
			console.log("data", success);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Created Successfully!", "success");
			$state.go("interest");
		}, function(error){
			console.log("data", error);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});