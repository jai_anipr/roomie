adminapp.controller("EditHobbieCtrl", function($scope, Hobbies, $stateParams,$auth,$state){
	// EditHobbieCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// EditCategoryCtrl controller calling 
	console.log("id", $stateParams.HobbyId);
	//fetching category data
	Hobbies.getHobbie($stateParams.HobbyId).then(function(success){
		console.log("success", success);
		$scope.data = success;
	}, function(error){
		console.log("success", error);
	})
 	// end
 	// updating Category data
 	$scope.updateHobbie = function(){
 		Hobbies.updateHobbies($stateParams.HobbyId, $scope.data).then(function(success){
			console.log("success", success);
			$state.go('hobbie')
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("success", error);
			swal("Error!", "Something Went Wrong!", "error");
		})
 	}
 	// end
});