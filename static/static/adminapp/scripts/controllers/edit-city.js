adminapp.controller("EditCityCtrl", function($scope,$auth,$stateParams,City,Country,$state){
	// EditCityCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

    console.log("id", $stateParams.CityId);

	// fetching tag data
	$scope.data = {"city_name":""}
	City.getCity($stateParams.CityId).then(function(success){
		console.log("data", success);
		$scope.data = success;
		$scope.data.country_id = $scope.data.country_id.id;
		
	}, function(error){
		console.log("error", error)
	})

	//Fetching countries from backend
	Country.getCountries().then(function(success){
        $scope.countries = success.results;
	},
		function(error){
			console.log(error);
		}
	)
	// end
	// update tag data
	$(".load-button").hide();
	$scope.updateCity = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		console.log($scope.data,'$scope.data$scope.data')
		City.updateCity($stateParams.CityId, $scope.data).then(function(success){
			console.log("data", success);
			$state.go('city');
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("error", error)
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end

});