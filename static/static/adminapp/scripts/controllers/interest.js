adminapp.controller("InterestCtrl", function($scope, Interest,$http,$auth,$state){
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

	// InterestCtrl controller calling 
	Interest.getInterests().then(function(success){
		console.log("data", success);
		$scope.interests = success.results;
		$scope.interestslength = $scope.interests.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("data", error);
	})

	// delete Interest data
	$scope.deleteInterest = function(interest){
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			Interest.deleteInterest(interest.id).then(function(success){
				console.log("data", success);
				$state.reload()
			}, function(error){
				console.log("data", error);
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		});
	}
	// end

		//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/interests/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.interests = success.data.results;
	    	$scope.interestslength = $scope.interests.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/interests/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.interests = success.data.results;
	    	$scope.interestslength = $scope.interests.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});