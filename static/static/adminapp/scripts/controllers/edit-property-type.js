
adminapp.controller("EditPropertyTypeCtrl", function($scope, $stateParams, PropertyType,$auth,$state){
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	console.log("id", $stateParams.propertyTypeId);
	//fetching PropertyType data
	PropertyType.getPropertyType($stateParams.propertyTypeId).then(function(success){
		console.log("success", success);
		$scope.data = success;
	}, function(error){
		console.log("success", error);
	})
 	// end
 	// updating PropertyType data
 	$scope.updatePropertyType = function(){
 		PropertyType.updatePropertyType($stateParams.propertyTypeId, $scope.data).then(function(success){
			console.log("success", success);
			$state.go('property-type');
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("success", error);
			swal("Error!", "Something Went Wrong!", "error");
		})
 	}
 	// end
});