adminapp.controller("RoomTypeCtrl", function($scope, RoomType, $state, $http,$auth){
	// RoomTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// fetching roomtypes data
	RoomType.getRoomTypes().then(function(success){
		console.log("data", success);
		$scope.roomtypes = success.results;
		$scope.roomtypeslength = $scope.roomtypes.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("error", error)
	})
	// end
	// delete tag data
	$scope.Delete = function(roomtype){
		// console.log("tag", tag.id);
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			RoomType.deleteRoomType(roomtype.id).then(function(success){
				console.log("data", success);
				$state.reload();
			}, function(error){
				console.log("error", error)
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		});
	}

	// ends

	//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/room_type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.roomtypes = success.data.results;
	    	$scope.roomtypeslength = $scope.roomtypes.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/room_type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.roomtypes = success.data.results;
	    	$scope.roomtypeslength = $scope.roomtypes.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});