adminapp.controller("AddMusicTypeCtrl", function($scope,$state,$auth,MusicTypes){
	// Add AddMusicTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// posting Music type Data
	$scope.addMusicType = function(data){
		console.log("$scope.data",$scope.data);
		$scope.loading=true;
		MusicTypes.createMusicType($scope.data).then(function(success){
			$scope.loading=false;
			console.log("data music type", success);
			$state.go("music-type");
		}, function(error){
			$scope.loading=false;
			console.log("data error", error)
		})
	}
	// end 
	// get Music Types
	MusicTypes.getMusicTypes().then(function(success){
        $scope.musictypes = success.results;
	},
		function(error){
			console.log(error);
		}
	)
});