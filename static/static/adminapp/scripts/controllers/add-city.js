adminapp.controller("AddCityCtrl", function($scope, City, Country,$state,$auth){
	// Add CityCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// posting city data
	$scope.AddCity = function(data){
		console.log("$scope.data",$scope.data);
		City.createCity($scope.data).then(function(success){
			console.log("data city", success);
			$state.go("city");
		}, function(error){
			console.log("data error", error)
		})
	}
	// end 
	// get city
	Country.getCountries().then(function(success){
        $scope.countries = success.results;
	},
		function(error){
			console.log(error);
		}
	)
});