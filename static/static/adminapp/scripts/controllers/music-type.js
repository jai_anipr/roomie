adminapp.controller("MusicTypeCtrl", function($scope, MusicTypes, $state,$http,$auth){
	// MusicTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

	MusicTypes.getMusicTypes().then(function(success){
		console.log("data", success);
		$scope.musictypes = success.results;
		$scope.musictypeslength = $scope.musictypes.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("data", error);
	})

	// delete Interest data
	$scope.deleteMusicType = function(musictype){
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			MusicTypes.deleteMusicType(musictype.id).then(function(success){
				console.log("data", success);
				$state.reload()
			}, function(error){
				console.log("data", error);
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		});
	}
	// end

	// Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/music-type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.musictypes = success.data.results;
	    	$scope.musictypeslength = $scope.musictypes.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/music-type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.musictypes = success.data.results;
	    	$scope.musictypeslength = $scope.musictypes.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});