adminapp.controller("DashboardCtrl", function($scope,$auth, Users, $http,$auth,$state,Listings, Users){
	
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

	//Fetching the count of brunches,buddies,bookings and events count
      $http.get("api/v1/app_count/")
      .then(function(response) {
           $scope.count = response.data[0];
      });


      // Listings count
      $http.get("api/v1/listings-count")
      .then(function(response) {
          $scope.listing_count = response.data.listing_count;
      },function(error){
        console.log("Listing error",error)
      });

       // User count
      $http.get("api/v1/users-count")
      .then(function(response) {
          $scope.user_count = response.data.users_count;
      },function(error){
        console.log("user error",error)
      });

      // free listing count
      $http.get("api/v1/paid-listings-count")
      .then(function(response) {
          $scope.paid_listing_count = response.data.paid_listings_count;
      },function(error){
        console.log("user error",error)
      });

      // paid listing count
      $http.get("api/v1/free-listings-count")
      .then(function(response) {
          $scope.free_listing_count = response.data.free_listings_count;
      },function(error){
        console.log("user error",error)
      });
  


});