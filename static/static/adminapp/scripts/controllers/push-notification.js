adminapp.controller("PushNotificationCtrl", function($scope,PushNotification,$auth){
	// PushNotificationCtrl controller calling
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

 	$scope.modernBrowsers = [
	    { name: "Opera",              maker: "(Opera Software)"},
	    { name: "Internet Explorer",  maker: "(Microsoft)"},
	    { name: "Firefox",            maker: "(Mozilla Foundation)"},
	    { name: "Safari",             maker: "(Apple)"},
	    { name: "Chrome",             maker: "(Google)"}
	];

	$scope.SendPushNotification = function(){
		PushNotification.PushNotify($scope.push).then(function(success){
			console.log("push response", success);
			swal("Success!", "Successfully sent push", "success");
		}, function(error){
			console.log("data error", error)
		})
	} 
});