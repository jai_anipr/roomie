adminapp.controller("EventCtrl", function($scope, Event, $state, $http,$auth){
	// EventCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	Event.getEvents().then(function(success){
		console.log("success", success)
		$scope.events = success.results;
		$scope.eventslength = $scope.events.length;
		$scope.private_events =[];
		$scope.public_events = [];
		angular.forEach($scope.events,function(eventdata){
			if(eventdata.security_id.name == 'Private'){
				$scope.private_events.push(eventdata);
			}
			else{
				$scope.public_events.push(eventdata);
			}
		});
	    $scope.total_public_events_length = $scope.public_events.length;
	    $scope.total_length = $scope.private_events.length;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("success", error)
	});

	$scope.Delete = function(event){
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			Event.deleteEvent(event.id).then(function(success){
	    		console.log("success", success);
	    		$state.reload()
	    		
	    	},function(error){
	    		console.log("error", error);
	    	})
	    	swal("Deleted!", "Deleted Successfully!.", "success");
		});
	}
	
	//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/events/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.events = success.data.results;
	    	$scope.private_events =[];
			$scope.public_events = [];
			angular.forEach($scope.events,function(eventdata){
				if(eventdata.security_id.name == 'Private'){
					$scope.private_events.push(eventdata);
				}
				else{
					$scope.public_events.push(eventdata);
				}
			});
		    $scope.total_public_events_length = $scope.public_events.length;
		    $scope.total_length = $scope.private_events.length;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/events/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.events = success.data.results;
	    	$scope.private_events =[];
			$scope.public_events = [];
			angular.forEach($scope.events,function(eventdata){
				if(eventdata.security_id.name == 'Private'){
					$scope.private_events.push(eventdata);
				}
				else{
					$scope.public_events.push(eventdata);
				}
			});
		    $scope.total_public_events_length = $scope.public_events.length;
		    $scope.total_length = $scope.private_events.length;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});