adminapp.controller("EditCountryCtrl", function($scope, Country, $stateParams,$auth,$state){
	// EditCountryCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

	// feteching data from server in update country
	id = $stateParams.countryId;
	console.log("id", $stateParams.countryId);
	if(id){
		Country.getCountry(id).then(function(success){
			console.log("data", success);
			$scope.data = success;
		}, function (error){
			console.log("data", error)
		})
	}else{
		console.log("error else")
	}

	// update country function
	$scope.UpdateCountry = function(){
		Country.updateCountry(id, $scope.data).then(function(success){
			console.log("success", success)
			$state.go('country')
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("error", error)
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
});