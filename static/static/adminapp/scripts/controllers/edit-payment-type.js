adminapp.controller("EditPaymentTypeCtrl", function($scope, $stateParams, PaymentType,$auth,$state){
	// EditPaymentTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	console.log("state", $stateParams.paymentTypeId);
	// fetch getPaymentType function
	PaymentType.getPaymentType($stateParams.paymentTypeId).then(function(success){
		console.log("data", success);
		$scope.data = success;
	}, function(error){
		console.log("error", error);
	})
	// end
	// update getPaymentType function 
	$scope.updatePaymentType = function(){
		console.log("data", $scope.data, $stateParams.paymentTypeId);
		$scope.loading=true;
		PaymentType.updatePaymentType($stateParams.paymentTypeId, $scope.data).then(function(success){
			console.log("data", success);
			$scope.loading=false;
			$state.go('payment-type');
			$scope.data = success;
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			$scope.loading=false;
			console.log("error", error);
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});