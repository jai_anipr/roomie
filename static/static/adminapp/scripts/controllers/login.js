adminapp.controller("LoginCtrl", function($scope, Login,$state,$auth,$window){
 console.log("Login Controller");
 $scope.user = {"email":"","password":"","role":""}

$scope.user_id = JSON.parse(localStorage.getItem('user_id'));
$scope.userdata = JSON.parse(localStorage.getItem('userdata'));

 $scope.login = function(user){
 	$scope.user.role = "manager";
 		console.log($scope.user,'Login Details')
		$scope.loading = true;
		Login.Login($scope.user).then(function(success){
			console.log("Login success",success);
			$scope.loading = false;
			localStorage.setItem('user_id', JSON.stringify(success.id));
			localStorage.setItem('userdata', JSON.stringify(success.user[0]));
			$auth.setToken(success.token);
			if($auth.isAuthenticated()){				
				$scope.LoggedIn = true;
			}
			$state.go('adminhome');
		},function(error){
			console.log("login invalid",error);
			swal("error", "Invalid Credentials. Please try again", "error")
			
		})
	}
	$scope.Logout = function(){
      console.log("Logout");
      $state.go('login');
      $scope.LoggedIn = false
      $auth.removeToken()
      $auth.logout()
      $window.localStorage.clear();
      // $localStorage.$reset();
    }
});