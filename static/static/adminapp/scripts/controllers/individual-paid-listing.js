adminapp.controller("IndividualpaidListingCtrl", function($scope, $state,$auth, Listings){
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	console.log("id", $state.params.PaidId);
	Listings.getListing($state.params.PaidId).then(function(success){
		console.log("success", success);
		$scope.listing = success;
		//For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date($scope.listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          $scope.listing['showbadge'] = diffDays;
        //End Badges

        //code for finding age
         $scope.dob = $scope.listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
          if(age){
            $scope.listing['userdob'] = age;
           }
           //End Finding age
		$scope.showMaplocations(success);
	}, function(error){
		console.log("error", error);
	})
	$scope.showMaplocations = function(location){
			console.log("data@", location);
	    var map;
	    var bounds = new google.maps.LatLngBounds();
	    var mapOptions = {
	        mapTypeId: 'roadmap'
	    };
	                    
	    // Display a map on the web page
	    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
	    map.setTilt(50);
	        
	    // Multiple markers location, latitude, and longitude
	    /*var markers = [
	        ['Brooklyn Museum, NY', 40.671531, -73.963588],
	        ['Brooklyn Public Library, NY', 40.672587, -73.968146],
	        ['Prospect Park Zoo, NY', 40.665588, -73.965336]
	    ];*/
	    // 
	    // Multiple markers location, latitude, and longitude
        var markers = [];
        //console.log("locations", location);
        //angular.forEach(location, function(obj){
          var listingmap = []
          // console.log("data for obj", obj)
          listingmap.push(location.location_id.street_name);
          listingmap.push(location.latitude)
          listingmap.push(location.longitude)
          markers.push(listingmap)
        // })
	                        
	    // Info window content
        var infoWindowContent = [];
        //angular.forEach(location, function(obj){
          // console.log("location", obj);
          var listingdesc = []
          var listingMapDesc = '<div class="info_content">' +'<img src='+location.photo_ids[0].photo+' class="image-map"/>' +'<h3>'+location.location_id.street_name+'</h3>' +'<p>'+ location.description +'</p>' + '</div>'
          listingdesc.push(listingMapDesc);
          infoWindowContent.push(listingdesc)
        // })
	        
	    // Add multiple markers to map
	    var infoWindow = new google.maps.InfoWindow(), marker, i;
	    
	    // Place each marker on the map  
	    for( i = 0; i < markers.length; i++ ) {
	        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
	        bounds.extend(position);
	        marker = new google.maps.Marker({
                position: position,
                icon : "/static/images/map.png",
                map: map,
                title: markers[i][0]
            });
	        
	        // Add info window to marker    
	        google.maps.event.addListener(marker, 'click', (function(marker, i) {
	            return function() {
	                infoWindow.setContent(infoWindowContent[i][0]);
	                infoWindow.open(map, marker);
	            }
	        })(marker, i));

	        // Center the map to fit all markers on the screen
	        map.fitBounds(bounds);
	    }

	    // Set zoom level
	    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
	        this.setZoom(14);
	        google.maps.event.removeListener(boundsListener);
	    });
	    
	}
	// Load initialize function

	// approve listing function
	$scope.ApproveListing = function(listing_id){
		console.log("ApproveListing calling",listing_id);
		swal({
		  title: "Are you sure?",
		  text: "You Want to Approve This Listing !",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, Updated it!",
		  closeOnConfirm: false
		},
		function(){
			var listing_info = {"listing_id":listing_id,"listing_status":"Approved"}
			Listings.UpdateListingStatus(listing_info).then(function(success){
				console.log("success",success)
			},function(error){
				console.log("error",error)
			})
		  	swal("Updated!", "Listing Status Updated Successfully!.", "success");
		  	$state.reload()
		});

		
	}

	// reject listing
	$scope.RejectListing = function(listing_id){
		swal({
		  title: "Are you sure?",
		  text: "You Want to Reject This Listing !",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, Reject it!",
		  closeOnConfirm: false
		},
		function(){
			var listing_info = {"listing_id":listing_id,"listing_status":"Rejected"}
			Listings.UpdateListingStatus(listing_info).then(function(success){
				console.log("success",success)
			},function(error){
				console.log("error",error)
			})
		  	swal("Updated!", "Listing Status Updated Successfully!.", "success");
		  	$state.reload()
		});
	}

});