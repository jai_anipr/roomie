adminapp.controller("PropertyTypeCtrl", function($scope, PropertyType, $state, $http,$auth){
	// PropertyTypeCtrl controller calling 

	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }

	//fetching category data
	PropertyType.getPropertyTypes().then(function(success){
		console.log("success", success);
		$scope.property_types = success.results;
		$scope.property_types_length = $scope.property_types.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("success", error);
	})
 	// end
 	// DeletePropertyType data
 	$scope.DeletePropertyType = function(property_type){
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			PropertyType.deletePropertyType(property_type.id, $scope.data).then(function(success){
				console.log("success", success);
				$state.reload()
			}, function(error){
				console.log("success", error);
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		});
 	}
 	// end

 	//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/property_type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.property_types = success.data.results;
	    	$scope.property_types_length = $scope.property_types.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/property_type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.property_types = success.data.results;
	    	$scope.property_types_length = $scope.property_types.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});