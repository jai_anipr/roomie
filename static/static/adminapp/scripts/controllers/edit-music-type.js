adminapp.controller("EditMusicTypeCtrl", function($scope, MusicTypes, $stateParams,$auth,$state){
	// EditMusicTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// EditCategoryCtrl controller calling 
	console.log("id", $stateParams.MusicTypeId);
	// get Music Types
	MusicTypes.getMusicType($stateParams.MusicTypeId).then(function(success){
		console.log(success,'@@@@@@@@')
        $scope.data = success;
	},
		function(error){
			console.log(error);
		}
	)
 	// updating Music Type data
 	$scope.updateMusicType = function(){
 		MusicTypes.updateMusicType($stateParams.MusicTypeId, $scope.data).then(function(success){
			console.log("success", success);
			$state.go('music-type')
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("success", error);
			swal("Error!", "Something Went Wrong!", "error");
		})
 	}
 	// end
});