adminapp.controller("AddRoomTypeCtrl", function($scope, RoomType, $state,$auth){
	// AddRoomTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// posting tag data
	$(".load-button").hide();
	$scope.data = {'room_type_name': ''};
	$scope.AddRoomType = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		console.log("data", $scope.data);
		RoomType.createRoomType($scope.data).then(function(success){
			// console.log("data", success)
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Created Successfully!", "success");
			$state.go('room-type');
		}, function(error){
			// console.log("error", error)
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});