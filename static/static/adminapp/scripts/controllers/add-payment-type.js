adminapp.controller("AddPaymentTypeCtrl", function($scope, PaymentType, $state,$auth){
	// AddPaymentTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// posting payment-type data
	$scope.data = {'price' : ''}
	$(".load-button").hide();
	$scope.AddPaymentType = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		console.log("data", $scope.data);
		PaymentType.createPaymentType($scope.data).then(function(success){
			console.log("data", success);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Created Successfully!", "success");
			$state.go("payment-type");
		}, function(error){
			console.log("error", error);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});