adminapp.controller("EditRoomTypeCtrl", function($scope, $stateParams, RoomType,$auth,$state){
	// EditRoomTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	console.log("id", $stateParams.EditId);
	// fetching getRoomType data
	RoomType.getRoomType($stateParams.EditId).then(function(success){
		console.log("data", success);
		$scope.data = success
	}, function(error){
		console.log("error", error)
	})
	// end
	// update getRoomType data
	$(".load-button").hide();
	$scope.updateRoomType = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		RoomType.updateRoomType($stateParams.EditId, $scope.data).then(function(success){
			console.log("data", success);
			$state.go('room-type');
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Updated Successfully!", "success");
		}, function(error){
			console.log("error", error)
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});