
adminapp.controller("AddPropertyTypeCtrl", function($scope, PropertyType, $state,$auth){
	// AddPropertyTypeCtrl controller calling 
	
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
    // adding PropertyType data
	$(".load-button").hide();
	$scope.AddPropertyType = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		PropertyType.createPropertyType($scope.data).then(function(success){
			console.log("success", success);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Created Successfully!", "success");
			$state.go("property-type");
		}, function(error){
			console.log("success", error);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
	// end
});