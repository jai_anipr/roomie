adminapp.controller("IndividualCtrl", function($scope, Users, $state,$auth){
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	console.log("id", $state.params.UserId)
	// get Individual user details
	Users.getuser($state.params.UserId).then(function(success){
		console.log("success", success);
		$scope.user = success
		console.log("data", $scope.user.user.first_name);
	},function(error){
		console.log("error", error);
	})
	// end
	

	// approve emirates function
	$scope.ApproveEmirates = function(user){
		console.log("UserId",user.id);
		swal({
		  title: "Are you sure?",
		  text: "Do you want approve this emairates id !",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, Updated it!",
		  closeOnConfirm: false
		},
		function(){
			var emirates_info = {"user_id":user.id,"change_status":"Approved"}
			console.log("data###############", emirates_info)
			Users.ChangeStatus(emirates_info).then(function(success){
				console.log("data@@@", success)
				swal("Updated!", "Updated Successfully!.", "success");
				$state.reload()
			}, function(error){
				console.log("data@@@error", error)
			})
		});
	}
	// reject emirates function
	$scope.RejectEmirates = function(user){
		console.log("UserId",user.id);
		swal({
		  title: "Are you sure?",
		  text: "Do you want to reject this emirates id !",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, Updated it!",
		  closeOnConfirm: false
		},
		function(){
			var emirates_info = {"user_id":user.id,"change_status":"Rejected"}
			console.log("data###############", emirates_info)
			Users.ChangeStatus(emirates_info).then(function(success){
				console.log("data@@@", success)
				swal("Updated!", "Updated Successfully!.", "success");
				$state.reload()
			}, function(error){
				console.log("data@@@error", error)
			})
		});
	}

	// delete user 
	$scope.delete = function(user){
		console.log("id", user.id);
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			Users.deleteMainUser(user.id).then(function(success){
			}, function(error){
				console.log("success", error);
			})
			swal("Deleted!", "Deleted Successfully!.", "success");
			$state.reload();
		  	$state.go('users')

		});
		
	}
	// end
});