adminapp.controller("PaymentTypeCtrl", function($scope, PaymentType, $state,$http,$auth){
	// PaymentTypeCtrl controller calling 
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// fetching getPaymentTypes data
	PaymentType.getPaymentTypes().then(function(success){
		console.log("data", success);
		$scope.payment_types = success.results;
		$scope.payment_types_length = $scope.payment_types.length;
	    $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
	}, function(error){
		console.log("error", error);
	})
	// end
	// delete payment_type function
	$scope.DeletePaymentType = function(payment_type){
		console.log("delete", payment_type.id);
		
		swal({
		  title: "Are you sure?",
		  text: "You Want to Delete it!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			PaymentType.deletePaymentType(payment_type.id).then(function(success){
				console.log("data", success);
				$state.reload();
			}, function(error){
				console.log("error", error);
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		  	$state.reload()
		});
	}
	// end

		//Pagination code
	$scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/purchase_type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.payment_types = success.data.results;
	    	$scope.payment_types_length = $scope.payment_types.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/purchase_type/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.payment_types = success.data.results;
	    	$scope.payment_types_length = $scope.payment_types.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});