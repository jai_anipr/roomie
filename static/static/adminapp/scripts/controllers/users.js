adminapp.controller("UsersCtrl", function($scope,Users, $http,$auth, $state){
	//Adding authuntication
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	// users controller calling 
	Users.getUsers().then(function(success){
      $scope.users = success.results;
	  console.log("users controller",success);
	  $scope.userlength = $scope.users.length;
	  $scope.totallength = success.count;
	  	if(success.previous){
			$scope.prevbtn = false;
		}else{
			$scope.prevbtn = true;
		}
		if(success.next){
			$scope.nextbtn = false;
		}else{
			$scope.nextbtn = true;
		}
    },function(error){
      console.log("users", error);
    });

    $scope.main = 1;
    $scope.nextPage = function() {
    	$scope.main ++;
        $http({
	        url : '/api/v1/users/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.users = success.data.results;
	    	console.log("$scope.users", success)
	    	$scope.userlength = $scope.users.length;
	    	$scope.totallength = success.data.count;
			if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
    };
    $scope.previousPage = function() {
    	if($scope.main > 1){
    	$scope.main --;
        // You could use Restangular here with a route resource.
        $http({
	        url : '/api/v1/users/?page=' +  $scope.main,
	        method : 'GET'
	      }).then(function(success){
	    	$scope.users = success.data.results;
	    	console.log("$scope.users", success)
	    	$scope.userlength = $scope.users.length;
	    	$scope.totallength = success.data.count;
	    	if(success.data.previous){
				$scope.prevbtn = false;
			}else{
				$scope.prevbtn = true;
			}
			if(success.data.next){
				$scope.nextbtn = false;
			}else{
				$scope.nextbtn = true;
			}
	      },function(error){
	          
	        }
	      );
        }
    };
});
