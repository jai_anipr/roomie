adminapp.controller("AddHobbieCtrl", function($scope, Hobbies, $state,$auth){
	// AddHobbieCtrl controller calling
	//Adding authuntication 
	if(!$auth.isAuthenticated()){
      $state.go('login');
    }
	$(".load-button").hide();
	$scope.addHobby = function(){
		$(".load-button").show();
		$(".btn-primary").hide();
		Hobbies.createHobbies($scope.data).then(function(success){
			console.log("data", success);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Success!", "Created Successfully!", "success");
			$state.go("hobbie");
		}, function(error){
			console.log("data", error);
			$(".load-button").hide();
			$(".btn-primary").show();
			swal("Error!", "Something Went Wrong!", "error");
		})
	}
});