
// Buddies service for communicate with back end
adminapp.factory('Users', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getUsers = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.getUsersCount = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users-count',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.getuser = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteUser = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id,
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
     object.deleteMainUser = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/delete_user/?userid='+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateUser = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
    object.ChangeStatus = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/verify_status_change/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end buddies service here 
