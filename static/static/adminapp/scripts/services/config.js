
// Config module service for communicate with back end
adminapp.factory('Vibes', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getVibes = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/vibes/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createVibe = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/vibes/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getVibe = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/vibes/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteVibe = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/vibes/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateVibe = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/vibes/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end vibes service here 

// MusicTypes module service for communicate with back end
adminapp.factory('MusicTypes', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getMusicTypes = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/music-type/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createMusicType = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/music-type/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getMusicType = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/music-type/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteMusicType = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/music-type/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateMusicType = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/music-type/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end music type service here 

// Authetication service for communicate with back end
adminapp.factory('Login', function($q, $http){
  var object = {};
  object.Login = function(data){
    var defered = $q.defer();
    $http({
      url : 'api/v1/admin_login/',
      method: 'POST',
      data: data
    }).then(function(success){
      defered.resolve(success.data);
    },
    function(error){
      defered.reject(error.data);
    });
    return defered.promise;
  }
  return object;
});



// Config module service for communicate with back end
adminapp.factory('Amenity', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getAmenities = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/amenities/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createAmenity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getAmenity = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteAmenity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateAmenity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Amenity service here 


// Config module service for communicate with back end
adminapp.factory('Category', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getCategories = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/categories/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createCategory = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/categories/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getCategory = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/categories/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteCategory = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/categories/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateCategory = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/categories/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Category service here 


// Config module service for communicate with back end
adminapp.factory('Tags', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getTags = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/tags/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createTag = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tags/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getTag = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/tags/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteTag = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tags/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateTag = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tags/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end tags service here 

// Config module service for communicate with back end
adminapp.factory('City', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getCities = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/cities/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createCity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getCity = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteCity = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateCity = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end city service here 


// Config module service for communicate with back end
adminapp.factory('Country', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getCountries = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/countries/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createCountry = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getCountry = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteCountry = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateCountry = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end country service here 

// Config module service for communicate with back end
adminapp.factory('Interest', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getInterests = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/intrests/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createInterest = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrests/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getInterest = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrests/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteInterest = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrests/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateInterest = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrests/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end interest service here 

// Config module service for communicate with back end
adminapp.factory('Hobbies', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getHobbies = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/hobbies/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createHobbies = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/hobbies/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getHobbie = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/hobbies/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteHobbies = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/hobbies/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateHobbies = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/hobbies/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end hobbies service here

//Property type service for communicate with back end
adminapp.factory('PropertyType', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getPropertyTypes = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/property_type/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createPropertyType = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/property_type/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getPropertyType = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/property_type/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deletePropertyType= function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/property_type/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updatePropertyType = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/property_type/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);

//Room type service for communicate with back end
adminapp.factory('RoomType', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getRoomTypes = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/room_type/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createRoomType = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/room_type/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getRoomType = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/room_type/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteRoomType= function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/room_type/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateRoomType = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/room_type/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);

//Payment type service for communicate with back end
adminapp.factory('PaymentType', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getPaymentTypes = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/purchase_type/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createPaymentType = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getPaymentType = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deletePaymentType= function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updatePaymentType = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);




