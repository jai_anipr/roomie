
// Listing service for communicate with back end
adminapp.factory('Listings', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getListings = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.getPaidListings = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/paid_listings/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.getFreeListings = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/?purchase_type__purchase_name=Basic',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.getListingsCount = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings-count',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };

    object.getListing = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/'+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteListing = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/'+id,
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateListing = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
    object.UpdateListingStatus = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listing-status-change/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end buddies service here 
adminapp.factory('Request', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getRequests = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/all_requests/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createRequest = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/requests/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
  return object;
  
}]);