// adminapp.js
var adminapp = angular.module('adminapp', [
            'ui.router',
            'oc.lazyLoad',
            'satellizer',
    ]);

adminapp.config(function($stateProvider, $urlRouterProvider,$ocLazyLoadProvider) {

    $urlRouterProvider.otherwise('/login');
    $stateProvider
        // HOME STATES AND NESTED VIEWS 
        .state('login', {
            url: '/login',
            templateUrl: "/static/adminapp/views/login.html",
            controller: "LoginCtrl"
        })
        .state('adminhome', {
            url: '/admin',
            templateUrl: '/static/adminapp/views/adminhome.html',
            controller : "DashboardCtrl"
        })
        .state('users', {
            url: '/users',
            templateUrl: '/static/adminapp/views/users.html',
            controller: "UsersCtrl"
        })
        .state('paid-listings', {
            url: '/paid-listings',
            templateUrl: '/static/adminapp/views/paid-listings.html',
            controller: "PaidListingsCtrl"
        })
        .state('individual-paid-listing', {
            url: '/individual-paid-listing/:PaidId',
            templateUrl: '/static/adminapp/views/individual-paid-listing.html',
            controller: "IndividualpaidListingCtrl"
        })
        .state('individual-free-listing', {
            url: '/individual-free-listing/:FreeListingId',
            templateUrl: '/static/adminapp/views/individual-free-listing.html',
            controller: "IndividualfreeListingCtrl"
        })
        .state('free-listings', {
            url: '/free-listings',
            templateUrl: '/static/adminapp/views/free-listings.html',
            controller: "FreeListingsCtrl"
        })
        .state('individual-event', {
            url: '/individual-event/:EventId',
            templateUrl: '/static/adminapp/views/individual-event.html',
            controller: "IndividualeventCtrl"
        })
        .state('create-event', {
            url: '/create-event',
            templateUrl: '/static/adminapp/views/create-event.html',
            controller: "CreateEventCtrl"
        })
        .state('push-notification', {
            url: '/push-notification',
            templateUrl: '/static/adminapp/views/push-notification.html',
            controller: "PushNotificationCtrl"
        })
        .state('individual-booking', {
            url: '/individual-booking/:BookingId',
            templateUrl: '/static/adminapp/views/individual-booking.html',
            controller: "IndividualBookingCtrl"
        })
        .state('city', {
            url: '/city',
            templateUrl: '/static/adminapp/views/city.html',
            controller: "CityCtrl"
        })
        .state('add-city', {
            url: '/add-city',
            templateUrl: '/static/adminapp/views/add-city.html',
            controller: "AddCityCtrl"
        })
        .state('edit-city', {
            url: '/edit-city/:CityId',
            templateUrl: '/static/adminapp/views/edit-city.html',
            controller: "EditCityCtrl"
        })
        .state('room-type', {
            url: '/room-type',
            templateUrl: '/static/adminapp/views/room-type.html',
            controller: "RoomTypeCtrl"
        })
        .state('add-room-type', {
            url: '/add-room-type',
            templateUrl: '/static/adminapp/views/add-room-type.html',
            controller: "AddRoomTypeCtrl"
        })
        .state('edit-room-type', {
            url: '/edit-room-type/:EditId',
            templateUrl: '/static/adminapp/views/edit-room-type.html',
            controller: "EditRoomTypeCtrl"
        })
        .state('property-type', {
            url: '/property-type',
            templateUrl: '/static/adminapp/views/property-type.html',
            controller: "PropertyTypeCtrl"
        })
        .state('add-property-type', {
            url: '/add-property-type',
            templateUrl: '/static/adminapp/views/add-property-type.html',

            controller: "AddPropertyTypeCtrl"
        })
        .state('edit-property-type', {
            url: '/edit-property-type/:propertyTypeId',
            templateUrl: '/static/adminapp/views/edit-property-type.html',

            controller: "EditPropertyTypeCtrl"
        })
        .state('payment-type', {
            url: '/payment-type',
            templateUrl: '/static/adminapp/views/payment-type.html',
            controller: "PaymentTypeCtrl"
        })
        .state('add-payment-type', {
            url: '/add-payment-type',
            templateUrl: '/static/adminapp/views/add-payment-type.html',
            controller: "AddPaymentTypeCtrl"
        })
        .state('edit-payment-type', {
            url: '/edit-payment-type/:paymentTypeId',
            templateUrl: '/static/adminapp/views/edit-payment-type.html',
            controller: "EditPaymentTypeCtrl"
        })
        .state('country', {
            url: '/country',
            templateUrl: '/static/adminapp/views/country.html',
            controller: "CountryCtrl"
        })
        .state('add-country', {
            url: '/add-country',
            templateUrl: '/static/adminapp/views/add-country.html',
            controller: "AddCountryCtrl"
        })
        .state('edit-country', {
            url: '/edit-country/:countryId',
            templateUrl: '/static/adminapp/views/edit-country.html',
            controller: "EditCountryCtrl"
        })
        .state('reports', {
            url: '/reports',
            templateUrl: '/static/adminapp/views/reports.html',
            controller: "ReportsCtrl"
        })
        .state('interest', {
            url: '/interest',
            templateUrl: '/static/adminapp/views/interest.html',
            controller: "InterestCtrl"
        })
        .state('add-interest', {
            url: '/add-interest',
            templateUrl: '/static/adminapp/views/add-interest.html',
            controller: "AddInterestCtrl"
        })
        .state('edit-interest', {
            url: '/edit-interest/:interestId',
            templateUrl: '/static/adminapp/views/edit-interest.html',
            controller: "EditInterestCtrl"
        })
        .state('hobbie', {
            url: '/hobbie',
            templateUrl: '/static/adminapp/views/hobbie.html',
            controller: "HobbieCtrl"
        })
        .state('add-hobbie', {
            url: '/add-hobbie',
            templateUrl: '/static/adminapp/views/add-hobbie.html',
            controller: "AddHobbieCtrl"
        })
        .state('edit-hobbie', {
            url: '/edit-hobbie/:HobbyId',
            templateUrl: '/static/adminapp/views/edit-hobbie.html',
            controller: "EditHobbieCtrl"
        })
        .state('music-type', {
            url: '/music-type',
            templateUrl: '/static/adminapp/views/music-type.html',
            controller: "MusicTypeCtrl"
        })
        .state('add-music-type', {
            url: '/add-music-type',
            templateUrl: '/static/adminapp/views/add-music-type.html',
            controller: "AddMusicTypeCtrl"
        })
        .state('edit-music-type', {
            url: '/edit-music-type/:MusicTypeId',
            templateUrl: '/static/adminapp/views/edit-music-type.html',
            controller: "EditMusicTypeCtrl"
        })
        .state('individual-user', {
            url: '/individual-user/:UserId',
            templateUrl: '/static/adminapp/views/individual-user.html',
            controller: "IndividualCtrl"
        })
        .state('requests', {
            url: '/requests',
            templateUrl: '/static/adminapp/views/requests.html',
            controller: "RequestsCtrl"
        })
});
