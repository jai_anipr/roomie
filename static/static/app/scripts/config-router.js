app.config(function($stateProvider,$urlRouterProvider,$authProvider, $urlRouterProvider,$mixpanelProvider, ngIntlTelInputProvider) {
    ngIntlTelInputProvider.set({initialCountry: 'ae'});
    $urlRouterProvider.otherwise('/');
    $mixpanelProvider.apiKey('84509a45a18b77394343e7aee75f167b');
    $stateProvider
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            templateUrl: '/static/app/views/home.html',
            controller: 'HomeCtrl'
        })
        .state('about', {
            url: '/about',
            templateUrl: '/static/app/views/about.html',
            controller: 'AboutCtrl'
        })
        .state('jobs', {
            url: '/jobs',
            templateUrl: '/static/app/views/jobs.html',
            controller: 'JobCtrl'
        })
        .state('areas', {
            url: '/areas',
            templateUrl: '/static/app/views/areas.html',
            controller: 'AreaCtrl'
        })
        .state('member-company', {
            url: '/member-company',
            templateUrl: '/static/app/views/member-company.html',
            controller: 'MemberCompanyCtrl'
        })
        .state('member-name', {
            url: '/member-name',
            templateUrl: '/static/app/views/member-name.html',
            controller: 'MemberNameCtrl'
        })
        .state('member-location', {
            url: '/member-location',
            templateUrl: '/static/app/views/member-location.html',
            controller: 'MemberLocationCtrl'
        })
        .state('mylistings', {
            url: '/mylistings',
            templateUrl: '/static/app/views/mylistings.html',
            controller: 'MyListingsCtrl'
        })
        .state('policies', {
            url: '/policies',
            templateUrl: '/static/app/views/policies.html',
            controller: 'PoliciesCtrl'
        })
        .state('post-listing', {
            url: '/post-listing',
            templateUrl: '/static/app/views/post-listing.html',
            controller: 'PostListingCtrl'
        })
        .state('press', {
            url: '/press',
            templateUrl: '/static/app/views/press.html',
            controller: 'PressCtrl'
        })
        .state('roomirocks', {
            url: '/roomirocks',
            templateUrl: '/static/app/views/roomirocks.html',
            controller: 'RoomirocksCtrl'
        })
        .state('search-listings', {
            url: '/search-listings',
            templateUrl: '/static/app/views/search-listings.html',
            controller: 'SearchListingsCtrl'
        })
        .state('team', {
            url: '/team',
            templateUrl: '/static/app/views/team.html',
            controller: 'TeamCtrl'
        })
        .state('policies-privacy', {
            url: '/policies-privacy',
            templateUrl: '/static/app/views/policies-privacy.html',
            controller: 'PoliciesPrivacyCtrl'
        })
        .state('request-password', {
            url: '/request-password',
            templateUrl: '/static/app/views/request-password.html',
            controller : 'forgotPasswordCtrl'
        })
        .state('favorites', {
            url: '/favorites',
            templateUrl: '/static/app/views/favorites.html',
            controller : "FavoritesCtrl"
        })
        .state('profile', {
            url: '/profile',
            templateUrl: '/static/app/views/profile.html',
            controller : "ProfileCtrl"
        })
        .state('edit-profile', {
            url: '/edit-profile',
            templateUrl: '/static/app/views/edit-profile.html',
            controller : "EditProfileCtrl"
        })
        .state('settings', {
            url: '/settings',
            templateUrl: '/static/app/views/settings.html',
            controller : "SettingsCtrl"
        })
        .state('individualpost', {
            url: '/individualpost/:objectId',
            templateUrl: '/static/app/views/individualpost.html',
            controller : "IndividualpostCtrl"
        })
        .state('messages', {
            url: '/messages/:objectId',
            templateUrl: '/static/app/views/messages.html',
            controller : "MessagesCtrl"
        })
        .state('allmessages', {
            url: '/messages/',
            templateUrl: '/static/app/views/messages.html',
            controller : "MessagesCtrl"
        })
        .state('edit-mylisting', {
            url: '/edit-mylisting/:objectId',
            templateUrl: '/static/app/views/edit-mylisting.html',
            controller : "EditeditMylistingCtrl"
        })
        .state('reset-password', {
            url: '/reset-password/:emailId/token/:token',
            templateUrl: '/static/app/views/reset-password.html',
            controller : "resetPasswordCtrl"
        })
        .state('confirm-email/:objectId', {
            url: '/confirm-email/:objectId',
            templateUrl: '/static/app/views/confirm-email.html',
            controller : "ConfirmAccountCtrl"
        })
        .state('user-info', {
            url: '/user-info/:userId',
            templateUrl: '/static/app/views/user-info.html',
            controller : "UserInfoCtrl"
        })
        .state('activate-account', {
            url: '/activate-account',
            templateUrl: '/static/app/views/activate-account.html',
            controller : "ActivateAccountCtrl"
        })
        .state('how-list-place', {
            url: '/how-list-place',
            templateUrl: '/static/app/views/how-list-place.html'
        })
        
});
