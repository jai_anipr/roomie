app.controller('MyListingsCtrl',function($scope,Listing, $state, $http, $auth, $location, User, Request){
	//console.log('MyListingsCtrl controller');

  //For Authuntication
  if (!$auth.isAuthenticated()) {
      $location.path("/home");
  }
	$scope.user_id = JSON.parse(localStorage.getItem('user_id'));
  $scope.request = {'user_id' : $scope.user_id}
  User.getUser($scope.user_id).then(function(success){
    $scope.request.name =  success.user.first_name
    $scope.request.email =  success.user.email
    $scope.request.mobile =  success.phone
  },function(error){
     //console.log("user error data", error);
  });
//To Fetch the Active Listings
$scope.loading=true;
  $http({
        url : '/api/v1/my_listings/?userid='+$scope.user_id+'&is_listable='+'True',
       method : 'GET'
     }).then(function(success){
      $scope.myactive_listings = success.data.results;
      $scope.activecount=success.data.count;
      $(".loading-container").hide();
      $scope.loading=false;
      if($scope.myactive_listings.length > 0){
        $scope.loadmorebtn = true;
      }else{
        $scope.loadmorebtn = false;
      }
      if(success.data.count == $scope.myactive_listings.length){
        $scope.loadmorebtn = false;
      }else{
        $scope.loadmorebtn = true;
      }
     },function(error){
      $(".loading-container").hide();
      $scope.loading=false;
         //console.log(error);
       }
     );

  //To Fetch the In Active Listings
  $scope.loading=true;
  $http({
        url : '/api/v1/my_listings/?userid='+$scope.user_id+'&is_listable='+'False',
       method : 'GET'
     }).then(function(success){
      $scope.my_inactive_listings = success.data.results;
      $scope.inactivecount=success.data.count;
      $(".loading-container").hide();
      $scope.loading=false;

      if($scope.my_inactive_listings.length > 0){
        $scope.loadmore = true;
      }else{
        $scope.loadmore = false;
      }
      if(success.data.count == $scope.my_inactive_listings.length){
        $scope.loadmore = false;
      }else{
        $scope.loadmore = true;
      }
     },function(error){
      $(".loading-container").hide();
      $scope.loading=false;
         //console.log(error);
       }
     );

	
	//Delete the listing
    $scope.deleteListing = function(id){
      swal({
        title: "Are you sure?",   
        text: "You want to delete this LIsting",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Yes, delete it!",   
        cancelButtonText: "No, cancel it!",   
        closeOnConfirm: false,   
        closeOnCancel: true 
      },
      function(isConfirm){
        if(isConfirm){
          Listing.deleteListing(id).then(function(success){
            $state.reload();
            swal({
                title:"deleted",
                text: "Your Listing has been removed from mylistings successfully!",
                type:"success",
                showConfirmButton: false,
                timer:1000
            })
          },function(error){
            //console.log("Delete listings",error);
            swal(
                {   
                  title: "Failed",
                  text: "Please try again",   
                  type: "error",   
                  confirmButtonColor: "#DD6B55",   
                  closeOnConfirm: false,   
                  closeOnCancel: false 
                })
          });
        }
        else {
        	swal("Something went wrong");
        }
      })
    }

  // load more function
  $scope.page = 1;
    $scope.myactive_listings = [];
    $scope.fetching = true;
    $scope.Loadmore = function() {
      $scope.fetching = true;
      $scope.page ++;
      $http({
        url : '/api/v1/my_listings/?page=' +  $scope.page+'&userid='+$scope.user_id+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.fetching = false;
        $scope.myactive_listings = $scope.myactive_listings.concat(success.data.results);
      $(".loading-container").hide();
      $scope.loading=false;
     if($scope.myactive_listings.length > 0){
            $scope.loadmorebtn = true;
          }else{
            $scope.loadmorebtn = false;
          }
          if(success.data.count == $scope.myactive_listings.length){
            $scope.loadmorebtn = false;
          }else{
            $scope.loadmorebtn = true;
          }
      },function(error){
        $(".loading-container").hide();
          //console.log(error);
        }
      );
    };
  // loadmore ends
  // load more function
  $scope.page = 1;
    $scope.my_inactive_listings = [];
    $scope.fetching = true;
    $scope.loadMore = function() {
      $scope.fetching = true;
      $scope.page ++;
      $http({
        url : '/api/v1/my_listings/?page=' +  $scope.page+'&userid='+$scope.user_id+'&is_listable='+'False',
        method : 'GET'
      }).then(function(success){
        $scope.fetching = false;
        $scope.my_inactive_listings = $scope.my_inactive_listings.concat(success.data.results);
      $(".loading-container").hide();
      $scope.loading=false;
        if($scope.my_inactive_listings.length > 0){
          $scope.loadmore = true;
        }else{
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.my_inactive_listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
        $(".loading-container").hide();
          //console.log(error);
        }
      );
    };
  // loadmore ends

    //Edit the listings
    // get the brunch data
    $scope.editMylisting = function(id){
       $state.go('edit-mylisting',{objectId :id});
    }
    // RequestListing start
    $scope.RequestListing = function(){
      Request.createRequest($scope.request).then(function(success){
        swal("Good job!", "Request has been posted successfully!", "success");
        $("#addRequest").modal('hide');
        $scope.page = 1;
      $scope.GetRequestListing()
      }, function(error){
        console.log("success", error)
      })
    }
     Request.getIndividualRequests($scope.user_id).then(function(success){
      // console.log("data@@@", success.results)
      $scope.requests = success.results;
      $(".loading-container").hide();
      $scope.requestscount = success.count
      if(success.count == $scope.requests.length){
          $scope.loadmorebtn = false;
        }else{
          $scope.loadmorebtn = true;
        }
    }, function(error){
      console.log("success", error)
    })

    $scope.GetRequestListing = function(){
      Request.getIndividualRequests($scope.user_id).then(function(success){
        // console.log("data@@@", success.results)
        $scope.requests = success.results
        $scope.requestscount = success.count;
        $(".loading-container").hide();
        if(success.count == $scope.requests.length){
            $scope.loadmorebtn = false;
          }else{
            $scope.loadmorebtn = true;
          }
      }, function(error){
        $(".loading-container").hide();
        console.log("success", error)
      })
    }
  // load more request function
    $scope.page = 1;
    $scope.requests = [];
    $scope.fetching = true;
    $scope.GetmoreRequests = function() {
      $scope.fetching = true;
      $scope.page ++;
      $http({
        url : '/api/v1/requests/?page=' +  $scope.page+'&userid='+$scope.user_id,
        method : 'GET'
      }).then(function(success){
        $scope.fetching = false;
        // console.log("data@@", success.data.results)
        $scope.requests = $scope.requests.concat(success.data.results);
         $(".loading-container").hide();
         if($scope.requests.length > 0){
          $scope.loadmorebtn = true;
        }else{
          $scope.loadmorebtn = false;
        }
        if(success.data.count == $scope.requests.length){
          $scope.loadmorebtn = false;
        }else{
          $scope.loadmorebtn = true;
        }
      },function(error){
        $(".loading-container").hide();
          //console.log(error);
        }
      );
    };
  // load more request function

})