app.controller('EditeditMylistingCtrl',function($scope, $state, User, Listing, $stateParams,$http,State,City,$auth,$location,$http,Area,PaymentPlan,Amenity,Rules,TenantAge,PropertyType,RoomType){
  if (!$auth.isAuthenticated()) {
      $location.path("/home");
   }
   $scope.user_id = JSON.parse(localStorage.getItem('user_id'));

  var listImages = []

   Listing.getIndividualListing($stateParams.objectId).then(function(success){
          // $scope.listing = success;
          //console.log("success",success)

          if($scope.user_id == success.user_id.id){
           // console.log("own listing")
          }else{
           // console.log("others listing")
            swal(
          "Error", 
          "Listing Not Found", 
          "error"
          )
            $state.go("mylistings");
          }
          var ages = []
          angular.forEach(success.age_choices,function(obj){
            ages.push(obj.id)
          })

          angular.forEach(success.photo_ids,function(photoObj){
            listImages.push(photoObj.photo)

          })

          $scope.profileimages = listImages

          $scope.listing = {
                            "location_id":
                            {"street_name":success.location_id.street_name,"zip_code":success.location_id.zip_code,"apartment":success.location_id.apartment},
                            "area_id":{"name":success.area_id.name},
                            "property_type_id":{"id":success.property_type_id.id},
                            "city_id" : {"city_name":success.city_id.city_name},
                            "room_type"  : {"id":success.room_type.id},
                            "bedrooms"  : success.bedrooms,
                            "bathrooms": success.bathrooms,
                            "distance_from_metro" : success.distance_from_metro,
                            "distance_from_busstop" : success.distance_from_busstop,
                            "distance_from_tram" : success.distance_from_tram,
                            "distance_from_airport" : success.distance_from_airport,
                            "age_choices"  :ages,
                            "gender_choice" : success.gender_choice,
                            "security" : success.security,
                            "monthly_rent":success.monthly_rent,
                            "deposit":success.deposit,
                            "is_flexible" : success.is_flexible,
                            "move_in_date":success.move_in_date,
                            "profileimages": $scope.profileimages,
                            "description":success.description,
                            "purchase_type.id" : success.purchase_type.id
                          }

        if(success.bedrooms){
           document.getElementById("bedroom").innerHTML = success.bedrooms;
           $scope.listing.bedrooms = success.bedrooms;
         }
         if(success.bathrooms){
           document.getElementById("bathroom").innerHTML = success.bathrooms;
           $scope.post.listing.bathrooms = success.bathrooms;
         }

         if(success.validity_for_stay=='medium'){
            $scope.slider.minValue="3"
            $scope.slider.maxValue="6"
         }else if(success.validity_for_stay=='long'){
            $scope.slider.minValue="6"
            $scope.slider.maxValue="12"
         }
         else{
            $scope.slider.minValue="1"
            $scope.slider.maxValue="3"
         }
         
         
    },function(error){
       swal(
          "Error", 
          "Listing Not Found", 
          "error"
          )
            $state.go("mylistings");
    })

  //console.log('EditeditMylistingCtrl  controller');
    $scope.selected = [];
      $scope.selectedAminities = [];
      $scope.selectedRules= [];
      $scope.post = {"listing":''};
      $scope.post.listing = {
        gender_choice : 'Either',
      }
      
        $scope.ageitems = [
      {id:1, name:'Below 20'},
          {id:2, name:'20 - 30'},
          {id:3, name:'31 - 40'},
          {id:4, name:'41 - 50'},
          {id:5, name:'51 and above'}
        ];

      $scope.toggleAges = function (item, list) {
        var idx = list.indexOf(item.id);
        if (idx > -1) {
          list.splice(idx, 1);
        }
        else {
          list.push(item.id);
        }
        $scope.post.listing.age_choices = list;
      };

      $scope.existsAges = function (item, list) {
        // console.log(list,'@@@@@@@@@@@@@@@@@@@@@@@@@@@@')
        // angular.forEach($scope.post.listing.age_choices,function(ages){
        //   list.push(ages.id);
        // });
        return list.indexOf(item.id) > -1;
      };
    $scope.toggleAminities = function (itemAminities, listAminities) {
      var idx = listAminities.indexOf(itemAminities);
      if (idx > -1) {
        listAminities.splice(idx, 1);
      }
      else {
        listAminities.push(itemAminities);
      }
       var amenity_ids = [];
       angular.forEach(listAminities, function (aminities){
          amenity_ids.push(aminities.id);
        });

      $scope.post.listing.amenity_ids = amenity_ids;
    };


    // get area data 
  Area.getAreas().then(function(success){
    $scope.areas = success.results;
    },function(error){
      //console.log("error",error)
    })
    $scope.existsAminities = function (itemAminities, listAminities) {
      return listAminities.indexOf(itemAminities) > -1;
    };

    $scope.toggleRules = function (itemRules, listRules) {
        //console.log(itemRules, listRules)
        var idx = listRules.indexOf(itemRules);
        if (idx > -1) {
          listRules.splice(idx, 1);
        }
        else {
          listRules.push(itemRules);
          //console.log(listRules)
        }
         var rules_ids = [];
         angular.forEach(listRules, function (rules){
            rules_ids.push(rules.id);
          });        
         $scope.post.listing.rules_ids = rules_ids;
     };

     $scope.existsRules = function (itemRules, listRules) {
        return listRules.indexOf(itemRules) > -1;
     };

     // Posting validity_for_stay value to backend
  $scope.sliderValue=function(slider){
    if(slider.maxValue<=3){
      $scope.post.listing.validity_for_stay= "small";
    }
    else if(slider.maxValue>=3 && slider.maxValue<=6){
      $scope.post.listing.validity_for_stay= "medium";
    }
    else{
      $scope.post.listing.validity_for_stay= "long";
    }
  } 

  $(".main-btn-save").hide();
  $(".roommate_box, .pricing_box, .amenities_box, .photos_box, .description_box, .plan_box").hide();
  $(".location_box_btn").click(function(){
    $(".roommate_box").show();
    $(".roommate_remove").removeClass("disable_class");
    $(".roommate_class").removeClass("remove_roommate_class");
    $(".location_box_btn").hide();
    $('html, body').animate({
          'scrollTop' : $(".roommate_box").position().top + 50
      });
  })
  $(".roommate_box_btn").click(function(){
    $(".pricing_box").show();
    $(".pricing_remove").removeClass("disable_class");
    $(".price_class").removeClass("remove_price_class");
    $(".roommate_box_btn").hide();
    $('html, body').animate({
          'scrollTop' : $(".pricing_box").position().top + 50
      });
  })
  $(".pricing_box_btn").click(function(){
    $(".amenities_box").show();
    $(".amenities_remove").removeClass("disable_class");
    $(".amenities_class").removeClass("remove_amenities_class");
    $(".pricing_box_btn").hide();
    $('html, body').animate({
          'scrollTop' : $(".amenities_box").position().top + 50
      });
  })
  $(".amenities_box_btn").click(function(){
    $(".photos_box").show();
    $(".photos_remove").removeClass("disable_class");
    $(".photos_class").removeClass("remove_photos_class");
    $(".amenities_box_btn").hide();
    $('html, body').animate({
          'scrollTop' : $(".photos_box").position().top + 50
      });
  })
  $(".photos_box_btn").click(function(){
    $(".description_box").show();
    $(".description_remove").removeClass("disable_class");
    $(".description_class").removeClass("remove_description_class");
    $(".photos_box_btn").hide();
    $('html, body').animate({
          'scrollTop' : $(".description_box").position().top + 50
      });
  })
  $(".description_box_btn").click(function(){
    $(".plan_box").show();
    $(".plan_remove").removeClass("disable_class");
    $(".plan_class").removeClass("remove_plan_class");
    $(".description_box_btn").hide();
    $(".main-btn-save").show();
    $('html, body').animate({
          'scrollTop' : $(".plan_box").position().top + 50
      });
  })
  $scope.roommateScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".roommate_box").position().top + 50
      });
  }
  $scope.locationScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".location_box").position().top + 50
      });
  }
  $scope.pricingScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".pricing_box").position().top + 50
      });
  }
  $scope.amenitiesScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".amenities_box").position().top + 50
      });
  }
  $scope.photosScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".photos_box").position().top + 50
      });
  }
  $scope.descriptionScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".description_box").position().top + 50
      });
  }
  $scope.planScroll = function(){
    $('html, body').animate({
          'scrollTop' : $(".plan_box").position().top + 50
      });
  }

  // left side bar scroll code start
  $(window).scroll(function(){
  if ($(this).scrollTop() > 100) {
    $('.scroll-bar-left').addClass('left-top-fixed');
  } else {
    $('.scroll-bar-left').removeClass('left-top-fixed');
  }
    });
  // end

  // range slider
  $scope.slider = {
    minValue: 1,
    maxValue: 1,
    options: {
      floor: 1,
      ceil: 12,
      translate: function(value, sliderId, label) {
        switch (label) {
          case 'model':
            return value + ' months ';
          case 'high':
            return value + ' months ';
          default:
            return '' + value
        }
      }
    }
  };  
  // end

  // get the payment plan
    PaymentPlan.getPlans().then(function(success){
      $scope.paymentplans =success.results; 
    })

    //Fetching Aminities from back end
    Amenity.getAmenities().then(function(success){
    $scope.aminities = success;     
    },function(error){
      
    })

    //Fetching rules from backend
    Rules.getRules().then(function(success){
    $scope.rules = success;     
    },function(error){
      
    })
    //Fetching state values from back end
    State.getStates().then(function(success){
    $scope.states = success.results;      
    },function(error){
      
    })
    //Fetching City Values from backend
    City.getCities().then(function(success){
    $scope.cities = success.results;      
    },function(error){
      
    })

   //Fetching tenant Age Values from backend
    TenantAge.getTenantAges().then(function(success){
    $scope.ageitems = success;  
    },function(error){
      //console.log("error",error);
    })

    //Fetching property types from backend
    PropertyType.getPropertyTypes().then(function(success){
      $scope.propertytypes =success.results; 
    })

    //Fetching room types from backend
    RoomType.getRoomTypes().then(function(success){
      $scope.roomtypes =success.results; 
    })
    
    $scope.postListing = function(){

      if($auth.isAuthenticated()){
      $scope.LoggedIn = true
    }
    else{
      $scope.LoggedIn = false
      $('#PostLoginModal').modal('show');
      return false
    }
    


      
    // $(".post-submit").hide();
    // $(".post-disabled").show();
    // $scope.user_id = JSON.parse(localStorage.getItem('user_id'));
    // $scope.loading=true;
    // console.log($scope.post.listing,'$scope.post.listing')
    // if ($scope.user_id){
    //   $scope.post.listing.user_id = $scope.user_id
    // }
    // if ($scope.post.listing.move_in_date){
    //   $scope.post.listing.move_in_date = moment($scope.post.listing.move_in_date).format('YYYY-MM-DD')
    // }
    
    // Listing.createListing($scope.post.listing).then(function(success){
    //   $scope.loading=false;
    //   $(".post-submit").show();
    //   $(".post-disabled").hide();
    //   swal("Good job!", "You are added listing successfully", "success")
    //   $state.go('mylistings');
    //   },function(error){
    //     $scope.loading=false;
    //     $(".post-submit").show();
    //     $(".post-disabled").hide();
    //     console.error("error",error)
    //     swal("Error", "Please Try Again", "error");     
    //   })
  }

$scope.AreaChange = function(id){
  $http({
        url : '/api/v1/area/'+id,
        method : 'GET'
      }).then(function(success){
        $scope.areadata = success.data
        $scope.post.listing.city_id = success.data.city_id.id
        $scope.post.listing.state_id = success.data.state_id.id
      },function(error){
          //console.log(error);
        }
      );


}
  
  $scope.PaymentPlanChange = function(paymentPlanid,price,name){
    $(".save_class").removeClass("save_btn_class")
    StartCheckout.config({
                key: "test_open_k_ec00a3257334da2e71e0",
                complete: function(params) {
                    $scope.loading = $.ajax({  
                        type: 'POST',  
                        url: 'http://ehapi.com/1axcess-payment/charge.php',
                        crossDomain: true,
                        data: { 
                            startToken: params.token.id,
                            startAmount: price*100,
                            startEmail: ''
                        }
                    }).then(function(payment_success) {
                        // payment_axcess();
                        // $scope.postListing()
                        //console.log("payment_success")
                  $scope.postListing()

                    }, function(payment_error) {
                        console.log("payment error is ---", payment_error);
                   $scope.postListing()

                        // $scope.postListing()
                    });
                }
            });

            
    if(name!="Basic"){
      StartCheckout.open({
              amount:   price*100,  // = AED 100.00
              currency: "AED"
            });
    }
  }
  

  // remove icon
  $scope.removeIcon = function($index){
    $scope.profileimages.splice($index,1);
    if($scope.profileimages.length > 1){
      $scope.notificationError = false;
      $scope.notificationErrorbtn = false;
      }else{
      $scope.notificationError = true;
      $scope.notificationErrorbtn = true;
      }
  }

//console.log("hello listing id", $stateParams.objectId)
  
  // disable past dates
  $scope.myDate = new Date();
    $scope.minDate = new Date(
       $scope.myDate.getFullYear(),
       $scope.myDate.getMonth(),
       $scope.myDate.getDate()
    );


    

  $scope.notificationErrorbtn = false;
  $scope.multipleimgdata = function(){
    cloudinary.openUploadWidget({ 
        cloud_name: 'brunch-buddies', 
        upload_preset: 'u3erc5ru'
      }, function(error, result) {

        if(result.length > 1){
        $scope.notificationError = false;
        $scope.notificationErrorbtn = false;
        }else{
        $scope.notificationError = false;
        $scope.notificationErrorbtn = false;
        }
          console.log(error, result);
          // $scope.profileimages = result;
          angular.forEach(result, function(listImage) {
        //console.log(listImage['url']);
        listImages.push(listImage['url'])
      });
      $scope.listing.profileimages = listImages;
          console.log("dataaaa", listImages)
          $scope.listing.photo_ids= listImages;
          $scope.$apply()
      });
  }


$scope.selectroom = function(roomtypes){
      angular.forEach(roomtypes, function(obj){
        if(obj.id == $scope.post.listing.roomtype_id){
          if(obj.room_type_name == 'entire room' || obj.room_type_name == 'Entire Room'){
            $(".showsize").show();
        }else{
          $(".showsize").hide();
        }
        }
      })
    }

 $scope.updateListing = function(){
  
  $scope.loading=true;

  console.log("$scope.listing",$scope.listing)
 //  console.log("$scope.post.listing",$scope.post.listing)
 //  if($scope.post.listing.city_id){
 //             $scope.post.listing.area_id = $scope.post.listing.area_id.name;
 // }
 // if($scope.post.listing.city_id){
 //     $scope.post.listing.city_id = $scope.post.listing.city_id.city_name;
 // }
 // if($scope.post.listing.property_type_id.id){
 //    $scope.post.listing.propertytype_id=$scope.post.listing.property_type_id.id;
 //  }
 //  if($scope.post.listing.room_type.id){
 //    $scope.post.listing.roomtype_id=$scope.post.listing.room_type.id;
 //  }
 //  console.log($stateParams.objectId,'@@@@@@@@@@@@@@@@2Updating',$scope.post.listing)
  Listing.updateListing($stateParams.objectId,$scope.listing).then(function(success){
      //console.log("success", success)
       swal(
          "success", 
          "Your listing has been updated successfully!", 
          "success"
          )
       $state.go("mylistings")
    },function(error){
      console.log("error", error)
       swal(
          "Error", 
          "Some thing went wrong", 
          "error"
          )
    })
}

  
   // wizard start
    $(document).ready(function () {
      //Initialize tooltips
      $('.nav-tabs > li a[title]').tooltip();
      
      //Wizard
      $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

          var $target = $(e.target);
      
          if ($target.parent().hasClass('disabled')) {
              return false;
          }
      });

      $(".next-step").click(function (e) {

          var $active = $('.wizard .nav-tabs li.active');
          $active.next().removeClass('disabled');
          nextTab($active);

      });
      $(".prev-step").click(function (e) {

          var $active = $('.wizard .nav-tabs li.active');
          prevTab($active);

      });
  });

  function nextTab(elem) {
      $(elem).next().find('a[data-toggle="tab"]').click();
  }
  function prevTab(elem) {
      $(elem).prev().find('a[data-toggle="tab"]').click();
  }
    // wizard ends
     // wizard ends
     // console.log($scope.post.listing.bedroom,'@@@@@@@@@@')
     // if($scope.post.listing.bedroom){
     //   document.getElementById("bedroom").value = $scope.post.listing.bedroom;
     // }
    $('#bedroom .dpui-numberPicker-input').on('input',function(e){
      $scope.post.listing.bedrooms =$('#bedroom .dpui-numberPicker-input').val();
        //console.log("bedroom", $scope.post.listing.bedroom)
    });
    $("#bedroom .dpui-numberPicker-decrease, #bedroom .dpui-numberPicker-increase").click(function(){
      $scope.post.listing.bedrooms =$('#bedroom .dpui-numberPicker-input').val();
       // console.log("bedroom", $scope.post.listing.bedroom)
    })
    $('#bathroom .dpui-numberPicker-input').on('input',function(e){
      $scope.post.listing.bathrooms =$('#bathroom .dpui-numberPicker-input').val();
        //console.log("bathroom", $scope.post.listing.bathroom)
    });
    $("#bathroom .dpui-numberPicker-decrease, #bathroom .dpui-numberPicker-increase").click(function(){
      $scope.post.listing.bathrooms =$('#bathroom .dpui-numberPicker-input').val();
        //console.log("bathroom", $scope.post.listing.bathroom)
    })
    $scope.Nationalities = [
        { name: 'Indian' },
        { name: 'Dubai' }
    ];
})
