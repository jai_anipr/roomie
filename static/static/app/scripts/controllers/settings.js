app.controller('SettingsCtrl',function($scope,User, $state,$auth,$location,$window,$timeout){
	if (!$auth.isAuthenticated()) {
      $location.path("/home");
  }


	//console.log('SettingsCtrl  controller');
	$scope.user_id = JSON.parse(localStorage.getItem('user_id'));
	if($scope.user_id){
		User.getUser($scope.user_id).then(function(success){
    	$scope.userInfo = success;
    	console.log("users  controller",success);
	    },function(error){
	       //console.log("user error data", error);
	    });
	}
	else{
		//console.log("no user id in cookies");
		// swal("Login Required", "Please Login")
		$state.go('home');
	}
	$scope.changePassword = function(changePassword){
   	//console.log(changePassword,'#######')
	$(".change-pwd-submit").hide();
	$(".change-pwd-disable").show();
	$scope.loading=true;
	//console.log($scope.changepassword,$scope.user_id);	
	User.updateUser($scope.user_id,$scope.changepassword).then(function(success){	
		$("#changePassword").modal('hide');
		$scope.loading=false;
		$(".change-pwd-submit").show();
		$(".change-pwd-disable").hide();
		swal("Good job!", "You are Succefully Added Listings", "success");
		},function(error){
			$scope.loading=false;
			$(".change-pwd-submit").show();
			$(".change-pwd-disable").hide();
			swal("Error", "Please Try Again", "error");
		})
	}

	

	$scope.userInfo = {"phone":"","deactivate_account":"","email":""}
	// $scope.deactivate = function(){
	//   $scope.userInfo.deactivate_account = "False";
	// }
	//Update user setting details
	$scope.updateSettingDetails = function(user){
	console.log(user,'user')
   	//console.log(changePassword,'#######')
	$(".update-user-submit").hide();
	$(".update-user-disable").show();
	$scope.loading=true;
	data={"phone":user.phone}
	User.updateUser($scope.user_id,data).then(function(success){	
		$("#changePassword").modal('hide');
		$scope.loading=false;
		$(".update-user-submit").show();
		$(".update-user-disable").hide();
		swal("Good job!", "You are Succefully updated user settings", "success");	
		
		},function(error){
			$scope.loading=false;
			$(".update-user-submit").show();
			$(".update-user-disable").hide();
			swal("Error", "Please Try Again", "error");
		})
	}

	$scope.deactivate = function(){
		$scope.userInfo.deactivate_account = "False";
		swal({
		  title: "Are you sure?",
		  text: "You Want to Deactive your account!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  closeOnConfirm: false
		},
		function(){
			User.updateUser($scope.user_id,$scope.userInfo).then(function(success){
				//$state.reload();				
			}, function(error){
				//console.log("data error", error)
			})
		  	swal("Deleted!", "Deleted Successfully!.", "success");
		  	
	    			$scope.LoggedIn = false;
			    	$scope.SignIn = false;
			    	$auth.removeToken()
			    	$auth.logout()
			    	$window.localStorage.clear();
			    	$state.go('home');
			    	$timeout(function () {
			    	 	$window.location.reload();
				    }, 1000);	    	
		});
	}
	$scope.notifyEmail = function(notifyemail){
			data={"notify_email":notifyemail}
			User.updateUser($scope.user_id,data).then(function(success){
			}, function(error){
				//console.log("data error", error)
		})
	}

		$scope.notifySms = function(notifysms){
			data = {"notify_sms":notifysms}
			User.updateUser($scope.user_id,data).then(function(success){
			}, function(error){
				//console.log("data error", error)
			})
	}
		$scope.isViewable = function(isViewable){
			data={"is_viewable":isViewable}
			User.updateUser($scope.user_id,data).then(function(success){
			}, function(error){
				//console.log("data error", error)
			})
	}


})