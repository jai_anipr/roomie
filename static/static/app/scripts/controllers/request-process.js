app.controller('forgotPasswordCtrl',function($scope,Login){
	//console.log('forgotPasswordCtrl controller');
	$scope.request = {"email":''}
	$scope.forgotPassword = function(){
		$scope.loading=true;
		$(".pwd-submit-btn").hide();
		$(".pwd-update").show();
		Login.forgotPassword($scope.request).then(function(success){
		$scope.loading=false;
		$(".pwd-submit-btn").show();
		$(".pwd-update").hide();
	    swal(
	          {   
	            title: "success",
	            text: "reset password link sent to your email.",   
	            type: "success",   
	            confirmButtonColor: "#DD6B55",   
	            closeOnConfirm: false,   
	            closeOnCancel: false,
	            timer : 3000,
	            showConfirmButton : false
	          })
	    },function(error){
	      $scope.loading=false;
	      $(".pwd-submit-btn").show();
		  $(".pwd-update").hide();
	      swal(
	      {   
	          title: "Forgot password Failed",
	          text: "Please try again",   
	          type: "error",   
	          confirmButtonColor: "#DD6B55",   
	          closeOnConfirm: false,   
	          closeOnCancel: false 
	      })
	    })    
	}
})