app.controller('SearchListingsCtrl',function($scope, City,Listing,$http,Amenity,Rules,$state,Area){
	//console.log('search-listings controller');

	//Fetching All Cities
	City.getCities().then(function(success){
		$scope.cities = success.results;			
		},function(error){
			
		})

	// Listing.getListings().then(function(success){
	// 	$scope.listings = success.results;
	// 	},function(error){
	// 		console.log("error",error)
	// 	})

	Amenity.getAmenities().then(function(success){
		$scope.amenities = success.results;
		},function(error){
			console.log("error",error)
		})

	// get area data 
	Area.getAreas().then(function(success){
		$scope.areas = success.results;
		},function(error){
			console.log("error",error)
		})

	//Fetching rules from backend
    Rules.getRules().then(function(success){
		$scope.rules = success.results;	
		// console.log($scope.rules,'success --getRules');		
		},function(error){
			
		})

    // serch by region 
    var searchTermObj = {}
    $scope.ListingSearchByRegion = function(){
    	searchTermObj['city_id'] = $scope.city_id
    	// console.log("region search",searchTermObj);
    	

    }

    $scope.ListingSearchByMoveDate = function(){
    	// console.log("movedate");
    	// console.log("region search",searchTermObj);
    }

    $scope.searchListingByDuration = function(){
    	searchTermObj['duration'] = $scope.duration
    	// console.log("duration search",searchTermObj);
    }

    $scope.searchListingByGender = function(){
    	searchTermObj['gender_choice'] = $scope.gender
    	// console.log("duration search",searchTermObj);

    }

    $scope.searchByPriceRange = function(){
    	// console.log("$scope.slider",$scope.slider)
    	searchTermObj['max_price'] = $scope.slider['maxValue']
    	searchTermObj['min_price'] = $scope.slider['minValue']
    	// console.log("duration search",searchTermObj);

    }
    $scope.data = {};
    $scope.getAmenityIds = function(amenityid){
    	// console.log("amenities",amenityid,$scope.data.amenitystatus)
    }

    $scope.searchListingByAge = function(){
    	// searchTermObj['gender'] = $scope.gender
    	// console.log("age search",searchTermObj);
    	// console.log("region search",searchTermObj);
    }


	$scope.SearchByAddress = function(){
		//console.log("SearchByAddress",$scope.addressSearchTerm)
		$http({
        url : '/api/v1/listings/?search=' + $scope.addressSearchTerm,
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results
        //console.log("length", $scope.listings.length);
        if($scope.listings.length > 0){
        	//console.log("iff@@@@@");
        	$scope.nolistings = false;
        }else{
        	//console.log("elseeeeee@@@@@");
        	$scope.nolistings = true;
        }
      },function(error){
          //console.log(error);
        }
      );
	}


	 $scope.individualpost = function(id){
      //console.log("id",id)
       $state.go('individualpost',{objectId :id});
    }

	$scope.AllListingSearch = function(){
		//console.log("all listing search");
   		var searhTerm = ''
   		angular.forEach(searchTermObj,function(key,value){
   			// console.log("key,value",key,value);
   			searhTerm = searhTerm+value+'='+key+'&'
   		})
   		// console.log("searhTerm",searhTerm)
   		$http({
        url : '/api/v1/listings/?' + searhTerm,
        method : 'GET'
	      }).then(function(success){
	        $scope.listings = success.data.results;
	        showMaplocations(success.data.results)
	      },function(error){
	          console.log(error);
	        }
	      );

	      $(".extra_search_filter, .extra_filter_bottom_btns").hide();
		$(".more_filters_btn, .list_view_image_grid").show();
	}

	$scope.ResetFilter = function(){
		$scope.city_id = ''
		$scope.area = ''
	}


	$scope.slider = {
	  minValue: 0,
	  maxValue: 5000,
	  options: {
	    floor: 0,
	    ceil: 5000,
	    translate: function(value, sliderId, label) {
	      switch (label) {
	        case 'model':
	          return 'AED' + value;
	        case 'high':
	          return 'AED' + value;
	        default:
	          return 'AED' + value
	      }
	    }
	  }
	};

	$(".extra_search_filter, .extra_filter_bottom_btns").hide();
	$scope.MoreFilterbtn = function(){
		$(".extra_search_filter, .extra_filter_bottom_btns").show();
		$(".more_filters_btn, .list_view_image_grid").hide();
	}
	$scope.CanceFilter = function(){
		$(".extra_search_filter, .extra_filter_bottom_btns").hide();
		$(".more_filters_btn, .list_view_image_grid").show();
	}


$scope.showMaplocations = function(location){
	    var map;
	    var bounds = new google.maps.LatLngBounds();
	    var mapOptions = {
	        mapTypeId: 'roadmap'
	    };
	                    
	    // Display a map on the web page
	    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
	    map.setTilt(50);
	        
	    // Multiple markers location, latitude, and longitude
	    /*var markers = [
	        ['Brooklyn Museum, NY', 40.671531, -73.963588],
	        ['Brooklyn Public Library, NY', 40.672587, -73.968146],
	        ['Prospect Park Zoo, NY', 40.665588, -73.965336]
	    ];*/
	    // 
	    // Multiple markers location, latitude, and longitude
        var markers = [];
        //console.log("locations", location);
        angular.forEach(location, function(obj){
          var listingmap = []
          // console.log("data for obj", obj)
          listingmap.push(obj.location_id.street_name);
          listingmap.push(parseInt(obj.latitude))
          listingmap.push(parseInt(obj.longitude))
          markers.push(listingmap)
        })
	                        
	    // Info window content
        var infoWindowContent = [];
        angular.forEach(location, function(obj){
          // console.log("location", obj);
          var listingdesc = []
          var listingMapDesc = '<div class="info_content">' +'<img src='+obj.photo_ids[0].photo+' class="image-map"/>' +'<h3>'+obj.location_id.street_name+'</h3>' +'<p>'+ obj.description +'</p>' + '</div>'
          listingdesc.push(listingMapDesc);
          infoWindowContent.push(listingdesc)
        })
	        
	    // Add multiple markers to map
	    var infoWindow = new google.maps.InfoWindow(), marker, i;
	    
	    // Place each marker on the map  
	    for( i = 0; i < markers.length; i++ ) {
	        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
	        bounds.extend(position);
	        marker = new google.maps.Marker({
                position: position,
                icon : "/static/images/map.png",
                map: map,
                title: markers[i][0]
            });
	        
	        // Add info window to marker    
	        google.maps.event.addListener(marker, 'click', (function(marker, i) {
	            return function() {
	                infoWindow.setContent(infoWindowContent[i][0]);
	                infoWindow.open(map, marker);
	            }
	        })(marker, i));

	        // Center the map to fit all markers on the screen
	        map.fitBounds(bounds);
	    }

	    // Set zoom level
	    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
	        this.setZoom(14);
	        google.maps.event.removeListener(boundsListener);
	    });
	    
	}
	// Load initialize function
})