app.controller('resetPasswordCtrl',function($scope,$stateParams,$state,Login){
	//console.log('resetPassword controller');

    //Post the reset password data to back end
  $scope.resetPassword = function(){
    var resetpassword = {
            email: $stateParams.emailId,
            token: $stateParams.token,
            password: $scope.password
        };
        // $scope.resetpwd = {"email":'',"token":''}
        // $scope.resetpwd['email'] = resetpassword.email;
        // $scope.resetpwd['token'] = resetpassword.token;
        // console.log($scope.resetpwd)
      Login.resetPassword(resetpassword).then(function(success){
       // console.log(success);
        swal(
            {   
              title: "success",
              text: "Your Password is successfully Reset",   
              type: "success",   
              confirmButtonColor: "#DD6B55",   
              closeOnConfirm: false,   
              closeOnCancel: false,
              timer : 1000,
              showConfirmButton : false
            })
          $state.go('home')
      },function(error){
        //console.log("reset password",error);
        swal(
            {   
              title: "Failed",
              text: error.error,   
              type: "error",   
              confirmButtonColor: "#DD6B55",   
              closeOnConfirm: false,   
              closeOnCancel: false 
            })
      })
    }

})