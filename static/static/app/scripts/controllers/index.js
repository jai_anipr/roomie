app.controller('IndexCtrl',function($scope, Login, User,$state,$auth, $window,$stateParams,$http,$rootScope,$timeout, $interval){
	//console.log('Index controller');

	$scope.data = {'token':$stateParams.objectId};
	if($auth.isAuthenticated()){
		$scope.LoggedIn = true
	}
	else{
		$scope.LoggedIn = false
	}

	var cover_image = JSON.parse(localStorage.getItem('cover_image'));
	var userdata = JSON.parse(localStorage.getItem('userdata'));
	var fbuserdata = JSON.parse(localStorage.getItem('fbuserdata'));

	if(userdata){
	setTimeout(function() {
		if(userdata.first_name){
	    	document.getElementById("name").innerHTML = userdata.first_name;
	    	document.getElementById("resname").innerHTML = userdata.first_name;
		}else{
			document.getElementById("name").innerHTML = userdata.email;
			document.getElementById("resname").innerHTML = userdata.email;
		}
	    document.getElementById("myImg").src = cover_image;
	    $scope.$apply(); //this triggers a $digest
  	});
	}
	if(fbuserdata){
  	setTimeout(function() {
		if(fbuserdata.first_name){
	    document.getElementById("name").innerHTML = fbuserdata.first_name +' '+ fbuserdata.last_name;
	    document.getElementById("resname").innerHTML = fbuserdata.first_name +' '+ fbuserdata.last_name;
		}
		if(fbuserdata.cover_image){
			document.getElementById("myImg").src = fbuserdata.cover_image;
		}else{
			document.getElementById("myImg").src = 'static/images/651fe3eb.guest-user.png';
		}
	    $scope.$apply(); //this triggers a $digest
  	});
  }
	var user_id = JSON.parse(localStorage.getItem('user_id'));
	if(user_id){
		User.getUser(user_id).then(function(success){
    	$scope.userInfo = success[0];
	    },function(error){
	       //console.log("user error data", error);
	    });
		// $scope.Msg = function(){
		// 	$http({
		// 	 	url : '/api/v1/messages/?userid='+user_id,
		// 	 	method : 'GET'
		//   	}).then(function(success){
		//   		setTimeout(function () {
		// 	        $scope.$apply(function () {
		// 	           $scope.messages = success.data.results
		// 	        });
		// 	    });
		// 	  	// console.log("data", $scope.messages)
		//   	},function(error){
		// 	  	console.log("messages error",error)
		//   	})
		// }
		// $scope.Msg();
		// $interval($scope.Msg, 30000);
	}


	// conversation redirect
	$scope.Converstation = function(id){
		$state.go("messages",{objectId : id})
	}
	// Login	
	$scope.login = function(){
		$scope.loading = true;
		$(".login-btn").hide();
		$(".add-disable").show();
		Login.Login($scope.user).then(function(success){
			// $("#LoginModal").modal('hide');
			$scope.loading = false;			
			$(".login-btn").show();
			$(".add-disable").hide();
			$scope.user_id = success.user[0].user_id;
			localStorage.setItem('user_id', JSON.stringify(success.user[0].user_id));
			$rootScope.$broadcast('fetchId', success.user[0].user_id);
			$scope.userInfo = {user:{"email":''}}
			localStorage.setItem('cover_image', JSON.stringify(success.user[0].cover_image));
			localStorage.setItem('userdata', JSON.stringify(success.user[0]));
			$("#PostLoginModal").modal('hide');
			$("#SearchLoginModal").modal('hide');
			$("#LoginModal").modal('hide');
			if(success.user[0].cover_image){
				$scope.userInfo.user.cover_image = success.user[0].cover_image;
			}
			if(success.user[0].first_name){
				$scope.userInfo.user.email = success.user[0].first_name;
			}
			else if(success.user[0].email){
				$scope.userInfo.user.email = success.user[0].email;
			}
			else{
				$scope.username = "User Name";
			}			
			$auth.setToken(success.token);
			if($auth.isAuthenticated()){				
				$scope.LoggedIn = true;
			}
			var cover_image = JSON.parse(localStorage.getItem('cover_image'));
			var userdata = JSON.parse(localStorage.getItem('userdata'));
			setTimeout(function() {
				if(userdata.first_name){
			    document.getElementById("name").innerHTML = userdata.first_name;
			    document.getElementById("resname").innerHTML = userdata.first_name;
				}else{
					document.getElementById("name").innerHTML = userdata.email;
					document.getElementById("resname").innerHTML = userdata.email;
				}
			    document.getElementById("myImg").src = cover_image;
			    $scope.$apply(); //this triggers a $digest
		  	});

		  	// chat plugin start
			(function(d, m){var s, h;       
				   s = document.createElement("script");
				   s.type = "text/javascript";
				   s.async=true;
				   s.src="https://apps.applozic.com/sidebox.app";
				   h=document.getElementsByTagName('head')[0];
				   h.appendChild(s);
				   window.applozic=m;
				   m.init=function(t){m._globals=t;}})(document, window.applozic || {});

			  window.applozic.init({
			    appId: "ehapi5193816c7f79c41127305ceb7d1368a0",      //Get your application key from https://www.applozic.com
			    userId: userdata.email,                     //Logged in user's id, a unique identifier for user
			    userName: userdata.first_name,                 //User's display name
			    imageLink : '',                     //User's profile picture url
			    email : '',                         //optional
			    contactNumber: '',                  //optional, pass with internationl code eg: +13109097458
			    desktopNotification: true,
			    source: '1',
			    topicBox: true,                          // optional, WEB(1),DESKTOP_BROWSER(5), MOBILE_BROWSER(6)

			    getTopicDetail: function(topicId) {
	            //Based on topicId, return the following details from your application
	            return {'title': 'topic-title',      // Product title
	                        'subtitle': 'sub-title',     // Product subTitle or Product Id
	                        'link' :'image-link',        // Product image link
	                        'key1':'key1' ,              // Small text anything like Qty (Optional)
	                        'value1':'value1',           // Value of key1 ex-10 (number of quantity) Optional
	                        'key2': 'key2',              // Small text anything like MRP (product price) (Optional)
	                        'value2':'value2'            // Value of key2 ex-$100  (Optional)
	                     };
	            },

			    notificationIconLink: 'https://www.applozic.com/favicon.ico',    //Icon to show in desktop notification, replace with your icon
			    authenticationTypeId: 1,          //1 for password verification from Applozic server and 0 for access Token verification from your server
			    accessToken: '',                    //optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
			    locShare: true,
			    // googleApiKey: "AIzaSyBwFpok8qPqfB3jbcnXH22YCxS-vLKnrfg",   // your project google api key 
			    // googleMapScriptLoaded : false,   // true if your app already loaded google maps script
			    // mapStaticAPIkey: "AIzaSyDQJiEDfujWcMt2pZcTgdoMiUH1maBdgSk",
			    autoTypeSearchEnabled : false,     // set to false if you don't want to allow sending message to user who is not in the contact list
			    loadOwnContacts : false, //set to true if you want to populate your own contact list (see Step 4 for reference)
			    olStatus: true,         //set to true for displaying a green dot in chat screen for users who are online
			    onInit : function(response) {
			       if (response === "success") {

			          console.log("on it response",response)
			          // login successful, perform your actions if any, for example: load contacts, getting unread message count, etc
			       } else {

			        console.log("on init response",response)

			          // error in user login/register (you can hide chat button or refresh page)
			       }
			   },
			   contactDisplayName: function(otherUserId) {
			         //return the display name of the user from your application code based on userId.
			         return "";
			   },
			   contactDisplayImage: function(otherUserId) {
			         //return the display image url of the user from your application code based on userId.
			         return "";
			   },
			   onTabClicked: function(response) {

			      console.log("response",response)

			         // write your logic to execute task on tab load
			         //   object response =  {
			         //    tabId : userId or groupId,
			         //    isGroup : 'tab is group or not'
			         //  }
			   }
			  });
   				// end chat code here 

		},function(error){
			$(".login-btn").show();
			$(".add-disable").hide();
			swal("error", "Invalid Credentials. Please try again", "error")
			
		})
	}	


// $scope.Messaging = function(email,displayname){
// console.log("email, displayname",email)
//   window.applozic.init({
//     appId: "ehapi5193816c7f79c41127305ceb7d1368a0",      //Get your application key from https://www.applozic.com
//     userId: email,                     //Logged in user's id, a unique identifier for user
//     userName: displayname,                 //User's display name
//     imageLink : '',                     //User's profile picture url
//     email : '',                         //optional
//     contactNumber: '',                  //optional, pass with internationl code eg: +13109097458
//     desktopNotification: true,
//     source: '1',                          // optional, WEB(1),DESKTOP_BROWSER(5), MOBILE_BROWSER(6)
//     notificationIconLink: 'https://www.applozic.com/favicon.ico',    //Icon to show in desktop notification, replace with your icon
//     authenticationTypeId: 1,          //1 for password verification from Applozic server and 0 for access Token verification from your server
//     accessToken: '',                    //optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
//     locShare: true,
//     // googleApiKey: "AIzaSyBwFpok8qPqfB3jbcnXH22YCxS-vLKnrfg",   // your project google api key 
//     // googleMapScriptLoaded : false,   // true if your app already loaded google maps script
//     // mapStaticAPIkey: "AIzaSyDQJiEDfujWcMt2pZcTgdoMiUH1maBdgSk",
//     autoTypeSearchEnabled : false,     // set to false if you don't want to allow sending message to user who is not in the contact list
//     loadOwnContacts : false, //set to true if you want to populate your own contact list (see Step 4 for reference)
//     olStatus: false,         //set to true for displaying a green dot in chat screen for users who are online
//     onInit : function(response) {
//        if (response === "success") {
//        	console.log("onInit",response)
//           // login successful, perform your actions if any, for example: load contacts, getting unread message count, etc
//        } else {
//           // error in user login/register (you can hide chat button or refresh page)
//        }
//    },
//    contactDisplayName: function(otherUserId) {
//          //return the display name of the user from your application code based on userId.
//          return "";
//    },
//    contactDisplayImage: function(otherUserId) {
//          //return the display image url of the user from your application code based on userId.
//          return "";
//    },
//    onTabClicked: function(response) {

//       console.log("response",response)

//          // write your logic to execute task on tab load
//          //   object response =  {
//          //    tabId : userId or groupId,
//          //    isGroup : 'tab is group or not'
//          //  }
//    }
//   });

// }

// messaging end

	//Signup 
	$scope.user = {"email":'',"password":''}
	$scope.signup = function(){
		$(".signup-btn").hide();
		$(".add-disabled").show();
		$scope.loading = true;
		User.createUser($scope.user).then(function(success){
		$scope.email = success.success.email
		$scope.username = $scope.email;
		$rootScope.$broadcast('fetchId', success.success.user_id);
		localStorage.setItem('user_id', JSON.stringify(success.success.user_id));
		localStorage.setItem('user', JSON.stringify(success.success));
		$("#SIgnupModal").modal('hide');
		// $auth.setToken(success.success.token);
		// if($auth.isAuthenticated()){				
		// 	$scope.LoggedIn = true;
		// }

		$("#show").modal('hide');
		$(".signup-btn").show();
		$(".add-disabled").hide();
		$scope.loading = false;
		// swal("Good job!", "We sent you a mail. please activate your account", "success")
		// $state.go('home');	
		$('#afterSIgnup').modal('show');
		},function(error){
			$(".signup-btn").show();
			$(".add-disabled").hide();
			swal("error", "Email already exist", "error")
		})
	}

	
	$scope.openSign = function(){
		$(".btn-sign-btn").hide();
		$scope.opensignDIv = true;
	}
	$scope.openSignup = function(){
		$(".btn-signgn-btn").hide();
		$scope.opensignupDIv = true;
	}

	$scope.AllMessages = function(){
		console.log("AllMessages")
	}

	$scope.logout = function(){
		$state.go('home');
    	$scope.LoggedIn = false;
    	$scope.SignIn = false;
    	$auth.removeToken()
    	$auth.logout()
    	$window.localStorage.clear();
    	$timeout(function () {
		 	$window.location.reload();
	    });
	}


    $scope.submitFacebookDeatils = function(data){
		Login.FacebookLogin(data).then(function(success){
			$("#LoginModal").modal('hide');
			$("#SIgnupModal").modal('hide');
			$("#SearchLoginModal").modal('hide');
			$("#PostLoginModal").modal('hide');
			$rootScope.$broadcast('fetchId', success.user.user_id);
			localStorage.setItem('fbuserdata', JSON.stringify(success.user));
			$scope.userInfo = {"user":{"email":''}}
			localStorage.setItem('user_id', JSON.stringify(success.user.user_id));
			if(success.user.first_name){
				$scope.userInfo.user.email = success.user.first_name;
			}
			else if(success.user.email){
				$scope.userInfo.user.email = success.user.email;
			}
			else{
				$scope.username = "User Name";
			}
			$auth.setToken(success.token);
			if($auth.isAuthenticated()){				
				$scope.LoggedIn = true;
			}
			var fbuserdata = JSON.parse(localStorage.getItem('fbuserdata'));
			setTimeout(function() {
				if(fbuserdata.first_name){
			    	document.getElementById("name").innerHTML = fbuserdata.first_name +' '+ fbuserdata.last_name;
			    	document.getElementById("resname").innerHTML = fbuserdata.first_name +' '+ fbuserdata.last_name;
				}
				if(fbuserdata.cover_image){
					document.getElementById("myImg").src = fbuserdata.cover_image;
				}else{
					document.getElementById("myImg").src = 'static/images/651fe3eb.guest-user.png';
				}
			    $scope.$apply(); //this triggers a $digest
		  	});
			// swal("Good job!", "You are Succefully LoggedIn", "success");
			// $state.go('home')
			// location.reload();	
			//swal("Good job!", "You are Succefully LoggedIn", "success")
		},function(error){
			//swal("Error", "It seems you already have an account !", "error")
			//console.log("error",error);
		})
	}

    // facebook login code
	$scope.checkLoginState = function(){
	    FB.login(function(response) {
		  if (response.status === 'connected') {
			FB.api('/me', { locale: 'en_US',fields: 'name, email, picture, first_name, last_name,gender'}, function(response) {
		      $scope.submitFacebookDeatils(response);
		    })
		  } else {
		    // The person is not logged into this app or we are unable to tell. 
		  }
		});
  	}
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '152149068701098',
			cookie     : true,  // enable cookies to allow the server to access 
			                    // the session
			xfbml      : true,  // parse social plugins on this page
			version    : 'v2.8' // use graph api version 2.8
		});
	};
	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
    // end
    // $(".drop_content_bot").hide();

    // profile image crop
    $scope.myImage='';
    $scope.profile_image = [];
    $scope.updateuser ={};
    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage = evt.target.result;
          $scope.profile_image.push(evt.target.result)
          // console.log($scope.myImage,"evt.target.result", evt.target.result);
          $("#profileImageId").attr("src", evt.target.result);
          if(evt.target.result){
          		$scope.hidedis = true;
          }
          else{
          	$scope.hidedis = false;
          }
        });
      };
      reader.readAsDataURL(file);
    };
    $scope.updateuser.cover_image = $scope.profile_image;
    angular.element(document.querySelector('#photo')).on('change',handleFileSelect);

      
	$scope.updateUserDetails = function(updateuser){
		$(".user-submit").hide();
		$(".user-disable").show();
		$scope.loading=true;
		$scope.user = JSON.parse(localStorage.getItem('user'));
		$scope.user_id = $scope.user.user_id;
		// if($scope.updateuser.dob){
	 //      var year=$scope.updateuser.dob.getFullYear();
		//   var date=$scope.updateuser.dob.getDate();
		//   var month=$scope.updateuser.dob.getMonth() + 1;
		//   var dateofbirth= year + '-' + month + '-' + date;
		//   // if($scope.updateuser.dob){
		//   	$scope.updateuser.dob = dateofbirth;
		//   	console.log(typeof($scope.updateuser.dob),'$scope.updateuser.dob')
		//  //  	console.log($scope.updateuser.dob,'$scope.updateuser.dob')
		// 	// }
		// }
		$scope.updateuser.dob = moment($scope.updateuser.dob).format('YYYY-MM-DD');

		User.updateUser($scope.user_id,$scope.updateuser).then(function(success){	
			$("#changePassword").modal('hide');
			$scope.loading=false;
			$scope.userInfo = success;
			$(".user-submit").show();
			$(".user-disable").hide();
			swal("Good job!", "You are Succefully Added Listings", "success");
			$('#afterSIgnup').modal('hide');	
			// $state.go('profile');
			swal("Good job!", "We sent you a mail. please activate your account", "success")

			},function(error){
				$scope.loading=false;
				$(".user-submit").show();
				$(".user-disable").hide();
				swal("Error", "Please Try Again", "error");
			})
		}
		$scope.myDate = new Date();
		$scope.maxDate = new Date(
	       $scope.myDate.getFullYear(),
	       $scope.myDate.getMonth(),
	       $scope.myDate.getDate()
       	);

		// chat code start
		if(userdata){
			(function(d, m){var s, h;       
			   s = document.createElement("script");
			   s.type = "text/javascript";
			   s.async=true;
			   s.src="https://apps.applozic.com/sidebox.app";
			   h=document.getElementsByTagName('head')[0];
			   h.appendChild(s);
			   window.applozic=m;
			   m.init=function(t){m._globals=t;}})

			(document, window.applozic || {});

		  window.applozic.init({
		    appId: "ehapi5193816c7f79c41127305ceb7d1368a0",      //Get your application key from https://www.applozic.com
		    userId: userdata.email,                     //Logged in user's id, a unique identifier for user
		    userName: userdata.first_name,                 //User's display name
		    imageLink : '',                     //User's profile picture url
		    email : '',                         //optional
		    contactNumber: '',                  //optional, pass with internationl code eg: +13109097458
		    desktopNotification: true,
		    source: '1',                          // optional, WEB(1),DESKTOP_BROWSER(5), MOBILE_BROWSER(6)
		    notificationIconLink: 'https://www.applozic.com/favicon.ico',    //Icon to show in desktop notification, replace with your icon
		    authenticationTypeId: 1,          //1 for password verification from Applozic server and 0 for access Token verification from your server
		    accessToken: '',                    //optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
		    locShare: true,

		    topicBox: true,                          // optional, WEB(1),DESKTOP_BROWSER(5), MOBILE_BROWSER(6)

			    getTopicDetail: function(topicId) {
	            //Based on topicId, return the following details from your application
	            return {'title': 'Demo Listing',      // Product title
	                        'subtitle': 'PR123',     // Product subTitle or Product Id
	                        'link' :'image-link',        // Product image link
	                        'key1':'qty' ,              // Small text anything like Qty (Optional)
	                        'value1':'100',           // Value of key1 ex-10 (number of quantity) Optional
	                        'key2': 'area',              // Small text anything like MRP (product price) (Optional)
	                        'value2':'200'            // Value of key2 ex-$100  (Optional)
	                     };
	            },
		    // googleApiKey: "AIzaSyBwFpok8qPqfB3jbcnXH22YCxS-vLKnrfg",   // your project google api key 
		    // googleMapScriptLoaded : false,   // true if your app already loaded google maps script
		    // mapStaticAPIkey: "AIzaSyDQJiEDfujWcMt2pZcTgdoMiUH1maBdgSk",
		    autoTypeSearchEnabled : false,     // set to false if you don't want to allow sending message to user who is not in the contact list
		    loadOwnContacts : false, //set to true if you want to populate your own contact list (see Step 4 for reference)
		    olStatus: true,         //set to true for displaying a green dot in chat screen for users who are online
		    onInit : function(response) {
		       if (response === "success") {

		          console.log("on it response",response)
		          // login successful, perform your actions if any, for example: load contacts, getting unread message count, etc
		       } else {

		        console.log("on init response",response)

		          // error in user login/register (you can hide chat button or refresh page)
		       }
		   },
		   contactDisplayName: function(otherUserId) {
		         //return the display name of the user from your application code based on userId.
		         return "";
		   },
		   contactDisplayImage: function(otherUserId) {
		         //return the display image url of the user from your application code based on userId.
		         return "";
		   },
		   onTabClicked: function(response) {

		      console.log("response",response)

		         // write your logic to execute task on tab load
		         //   object response =  {
		         //    tabId : userId or groupId,
		         //    isGroup : 'tab is group or not'
		         //  }
		   }
		  });
		}
		// chat code ends

})