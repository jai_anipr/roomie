app.controller('IndividualpostCtrl',function($scope, IntrestedListings,$auth,Listing,$stateParams, $http,$state,$mixpanel){
	//console.log('IndividualpostCtrl controller');
	$scope.user_id = JSON.parse(localStorage.getItem('user_id'));

	//====================================
    // Slick 2
    //====================================
    
    $scope.slickConfig2Loaded = true;
    $scope.slickConfig2 = {
      autoplay: true,
      infinite: true,
      autoplaySpeed: 3000,
      slidesToShow: 3,
      slidesToScroll: 3,
      method: {}
    };
    
    $scope.slickConfigmain2Loaded = true;
    $scope.slickConfigmain2 = {
      autoplay: true,
      infinite: true,
      autoplaySpeed: 3000,
      slidesToShow: 2,
      slidesToScroll: 2,
      method: {}
    };


	//Fetching my listings from backend
	$http({
        url : '/api/v1/listings/'+$stateParams.objectId+'/',
       method : 'GET'
     }).then(function(success){
     	$mixpanel.track('View Listing',
     			{"Listing Owner Name":success.data.user_id.user.firstname,"City Name":success.data.city_id.city_name,"Monthly Rent":success.data.monthly_rent}
     		)
     	console.log("success", success)
       	$scope.individualfavourite = success.data;
       	$scope.amenity_ids_length = false;
       	if($scope.individualfavourite.amenity_ids.length == 0){
       		$scope.amenity_ids_length = true;
       	}
       	$scope.rules_ids_length = false;
       	if($scope.individualfavourite.rules_ids.length == 0){
       		$scope.rules_ids_length = true;
       	}

       	$scope.showMaplocations(success.data);
		$scope.current_user = true;
		if($scope.user_id == $scope.individualfavourite.user_id.id ){
				//console.log('CURRENT USER');
				$scope.current_user = false;
		}
		var today = new Date();
       var birthDate = new Date($scope.individualfavourite.user_id.dob);
       var age = today.getFullYear() - birthDate.getFullYear();
       var m = today.getMonth() - birthDate.getMonth();
       if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
           age--;
       }
       	if(age){
       	 $scope.age = age;	
       	}
        
     },function(error){
         //console.log(error);
       }
     );

     $scope.showMaplocations = function(location){
	    var map;
	    var bounds = new google.maps.LatLngBounds();
	    var mapOptions = {
	        mapTypeId: 'roadmap'
	    };
	                    
	    // Display a map on the web page
	    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
	    map.setTilt(50);
	        
	    // Multiple markers location, latitude, and longitude
	    /*var markers = [
	        ['Brooklyn Museum, NY', 40.671531, -73.963588],
	        ['Brooklyn Public Library, NY', 40.672587, -73.968146],
	        ['Prospect Park Zoo, NY', 40.665588, -73.965336]
	    ];*/
	    // 
	    // Multiple markers location, latitude, and longitude
        var markers = [];
        //console.log("locations", location);
        //angular.forEach(location, function(obj){
          var listingmap = []
          // console.log("data for obj", obj)
          listingmap.push(location.location_id.street_name);
          listingmap.push(location.latitude)
          listingmap.push(location.longitude)
          markers.push(listingmap)
        // })
	                        
	    // Info window content
        var infoWindowContent = [];
        //angular.forEach(location, function(obj){
          // console.log("location", obj);
          var listingdesc = []
          var listingMapDesc = '<div class="info_content">' +'<img src='+location.photo_ids[0].photo+' class="image-map"/>' +'<h3>'+location.location_id.street_name+'</h3>' +'<p>'+ location.description +'</p>' + '</div>'
          listingdesc.push(listingMapDesc);
          infoWindowContent.push(listingdesc)
        // })
	        
	    // Add multiple markers to map
	    var infoWindow = new google.maps.InfoWindow(), marker, i;
	    
	    // Place each marker on the map  
	    for( i = 0; i < markers.length; i++ ) {
	        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
	        bounds.extend(position);
	        marker = new google.maps.Marker({
                position: position,
                icon : "/static/images/map.png",
                map: map,
                title: markers[i][0]
            });
	        
	        // Add info window to marker    
	        google.maps.event.addListener(marker, 'click', (function(marker, i) {
	            return function() {
	                infoWindow.setContent(infoWindowContent[i][0]);
	                infoWindow.open(map, marker);
	            }
	        })(marker, i));

	        // Center the map to fit all markers on the screen
	        map.fitBounds(bounds);
	    }

	    // Set zoom level
	    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
	        this.setZoom(14);
	        google.maps.event.removeListener(boundsListener);
	    });
	    
	}
	// Load initialize function


	// // Fetching interested listings from backend
	// IntrestedListings.getIntrestedListing($scope.user_id).then(function(success){
		
	// 	$scope.individualfavourite = success.results[0];
	// 	// console.log($scope.individualfavourite,'success')
	// 	var today = new Date();
 //       // var birthDate = new Date($scope.individualfavourite.user_id.dob);
 //       // var age = today.getFullYear() - birthDate.getFullYear();
 //       // var m = today.getMonth() - birthDate.getMonth();
 //       // if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
 //       //     age--;
 //       // }
 //       //  $scope.dob = age;

	// 	},function(error){
			
	// 	})
	$scope.SendMessage = function(id){
		if($auth.isAuthenticated()){
			//console.log("send messages",id);
			$state.go('messages', {objectId: id})
		}
		else{
			$("#LoginModal").modal('show');	
		}
	}
	

	 // Added listing as Favourite
	$scope.addFavourite = function(id){
		data ={'user_id':$scope.user_id,'listing_id':id};
	    IntrestedListings.createIntrestedListing(data).then(function(success){
	       swal(
	        "success", 
	        "Your Listing has been added as Favourites successfully!", 
	        "success"
	        )
	       $state.reload()
	      },function(error){
	        //console.log("Add Favourite Listings",error);
	        swal(
	        "Failed", 
	        "Listing already added in favourites!", 
	        "error"
	        )
	      })
	  }
    //Remove the listing from favourite
    $scope.unFavourite = function(id){
		//data ={'user_id':$scope.user_id,'listing_id':id}
	    IntrestedListings.deleteIntrestedListing(id).then(function(success){
	       swal(
	        "success", 
	        "Your Listing has been removed as Favourites successfully!", 
	        "success"
	        )
	       $state.reload()
	      },function(error){
	        //console.log("Delete Favourite Listings",error);
	        swal(
	        "Failed", 
	        "Listing already removed in favourites!", 
	        "error"
	        )
	      })
}
//Restricting the user profile, if user is_viewabale condition is false
    $scope.isViewable = function(id) {
      console.log($scope.user_id,id,$scope.user_id == id)
      if($scope.user_id == id){
      $state.go('profile')
     }else{
       swal("Profile Viewable Restricted")
     }
    };
})