app.controller('UserInfoCtrl',function($scope,$auth,$location, $stateParams, User, $state,IntrestedListings,$http){
	//console.log($stateParams.userId,'$stateParams.userId222',$stateParams.userId)
	//var user_id = $stateParams.userId;
	$scope.user_id = JSON.parse(localStorage.getItem('user_id'));

  if($auth.isAuthenticated()){
    //console.log("login")
    $scope.user_id = JSON.parse(localStorage.getItem('user_id'));
    if($scope.user_id == $stateParams.userId){
      $state.go('profile')
    }
  }
  $scope.slicklistingimagesLoaded = true;
  $scope.slicklistingimages = {
    autoplay: true,
    infinite: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    method: {}
  };
  $scope.IndPage = function(obj){
    window.open('#/individualpost/'+obj,'_blank');
  }

	if($scope.user_id){
		$http({
        url : '/api/v1/users/'+$stateParams.userId+'/?userids='+$scope.user_id,
        method : 'GET'
     }).then(function(success){
     	  $scope.userInfo = success.data;
        $(".loading-container").hide();
       	//For Finding Age
  	    var today = new Date();
        var birthDate = new Date($scope.userInfo.dob);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        if(age){
          $scope.userage = age;
        }

        //For finding latest listing or not
        angular.forEach($scope.userInfo.listings_data,function(listing){
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.move_in_date);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
          })
    	//console.log($scope.userInfo,'$scope.userInfo')
	    },function(error){
        $(".loading-container").hide();
	       //console.log("user error data", error);
     });

	}else
	{
	$http({
        url : '/api/v1/users/'+$stateParams.userId,
       method : 'GET'
     }).then(function(success){
      $(".loading-container").hide();
     	$scope.userInfo = success.data;
     	//For Finding Age
	    var today = new Date();
        var birthDate = new Date($scope.userInfo.dob);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        $scope.userage = age;

        //For finding latest listing or not
        angular.forEach($scope.userInfo.listings_data,function(listing){
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.move_in_date);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
          })
    	//console.log($scope.userInfo,'$scope.userInfo')
	    },function(error){
        $(".loading-container").hide();
	       //console.log("user error data", error);
     });
	
	}
	
  //Adding listing as Favourite
  $scope.addFavourite = function(id){

	data ={'user_id':$scope.user_id,'listing_id':id};
    IntrestedListings.createIntrestedListing(data).then(function(success){
       // console.log(success);
       swal(
        "success", 
        "Your Listing has been added as Favourites successfully!", 
        "success"
        )
       $state.reload();
       // $scope.apply();
      },function(error){
        //console.log("Add Favourite Listings",error);
        swal(
        "Failed", 
        "You already shown intrest in  this listing", 
        "error"
        )
      })
    }
  
    //Removing the listing as unfavourite
    $scope.unFavourite = function(id){   
      data ={'roomuserID':$scope.user_id,'listing_id':id};
      IntrestedListings.removeIntrestedListing(data).then(function(success){
         swal(
          "success", 
          "Your Listing has been removed as Favourites successfully!", 
          "success"
          )
         $state.reload();
         // $scope.apply();
        },function(error){
          //console.log("Delete Favourite Listings",error);
          swal(
          "Failed", 
          "Listing already removed in favourites!", 
          "error"
          )
        })
    }
	
})