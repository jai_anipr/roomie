app.controller('ProfileCtrl',function($scope,$auth,$state,User,$location){
	if (!$auth.isAuthenticated()) {
      $location.path("/home");
  }
	//console.log('About  controller');
	var user_id = JSON.parse(localStorage.getItem('user_id'));
	if(user_id){
		User.getUser(user_id).then(function(success){
    	$scope.user = success;
    	console.log("success", success)
    	$(".loading-container").hide();
    	$scope.background = $scope.user.work_exp.length;
    	if(success.dob){
		   var today = new Date();
	       var birthDate = new Date(success.dob);
	       var age = today.getFullYear() - birthDate.getFullYear();
	       var m = today.getMonth() - birthDate.getMonth();
	       if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
	           age--;
	       }
	        $scope.userage = age;
    	}else{
    		$scope.userage = "";
    	}
       
	    },function(error){
	    	$(".loading-container").hide();
	       //console.log("user error data", error);
	    });
	}
	else{
		// swal("Login Required", "Please Login")
		$state.go('home');
	}
	$scope.security = {'id_front_photo': '','id_back_photo': ''}
	$scope.UploadFront = function(){
		cloudinary.openUploadWidget({ 
	    	cloud_name: 'brunch-buddies', 
	    	upload_preset: 'u3erc5ru',
	    	cropping: 'server',
			folder: 'user_photos'
	    }, function(error, result) {
	      	//console.log(error, result);
	      	$scope.security.id_front_photo = result[0].secure_url;
	      	document.getElementById('frontimage').src = result[0].secure_url
	      	if(result[0].secure_url){
	      		$(".show-front-image").show();
	      	}
	    });
	}
	$scope.UploadBack = function(){
		cloudinary.openUploadWidget({ 
	    	cloud_name: 'brunch-buddies', 
	    	upload_preset: 'u3erc5ru',
	    	cropping: 'server', 
			folder: 'user_photos'
	    }, function(error, result) {
	      	//console.log(error, result);
	      	$scope.security.id_back_photo = result[0].secure_url;
	      	document.getElementById('backimage').src = result[0].secure_url
	      	if(result[0].secure_url){
	      		$(".show-back-image").show();
	      	}
	    });
	}
	$scope.PassportUploadFront = function(){
		cloudinary.openUploadWidget({ 
	    	cloud_name: 'brunch-buddies', 
	    	upload_preset: 'u3erc5ru',
	    	cropping: 'server',
			folder: 'user_photos'
	    }, function(error, result) {
	      	//console.log(error, result);
	      	$scope.passport.passport_front_photo = result[0].secure_url;
	      	document.getElementById('passportfrontimage').src = result[0].secure_url
	      	if(result[0].secure_url){
	      		$(".passport-show-front-image").show();
	      	}
	    });
	}
	$scope.PassportUploadBack = function(){
		cloudinary.openUploadWidget({ 
	    	cloud_name: 'brunch-buddies', 
	    	upload_preset: 'u3erc5ru',
	    	cropping: 'server', 
			folder: 'user_photos'
	    }, function(error, result) {
	      	//console.log(error, result);
	      	$scope.passport.passport_back_photo = result[0].secure_url;
	      	document.getElementById('passportbackimage').src = result[0].secure_url
	      	if(result[0].secure_url){
	      		$(".passport-show-back-image").show();
	      	}
	    });
	}

	

	$scope.Checkemirates = function(){
		var verify_statuss = {'verify_statuss': $scope.security}
		User.updateUser(user_id,verify_statuss).then(function(success){
			swal("Good job!", "Added emairates id successfully", "success")
			$("#Verifymodal").modal('hide');
			setTimeout(function() {
				$state.reload()
		  	},1000);
		}, function(error){
			//console.log("error data@@", error)
		})
	}
	$scope.Checkpassport = function(){
		var verify_statuss = {'verify_statuss': $scope.passport}
		console.log("verify_statuss", verify_statuss)
		User.updateUser(user_id,verify_statuss).then(function(success){
			swal("Good job!", "Added passport detailes successfully", "success")
			$("#Verifymodal").modal('hide');
			setTimeout(function() {
				$state.reload()
		  	},1000);
		}, function(error){
			//console.log("error data@@", error)
		})
	}
	$scope.emirates = true;
	$scope.checkemi = function(){
		if($scope.emirates == true){
			$scope.passportcheck = false;
		}else{
			$scope.passportcheck = true;
		}
	}
	$scope.checkpass = function(){
		if($scope.passportcheck == true){
			$scope.emirates = false;
		}else{
			$scope.emirates = true;
		}
	}



})