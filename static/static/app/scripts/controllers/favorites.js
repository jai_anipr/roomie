app.controller('FavoritesCtrl',function($scope, IntrestedListings, $state,$auth,$location,$http){
  if (!$auth.isAuthenticated()) {
      $location.path("/home");
  }
	//console.log('FavoritesCtrl  controller');

	$scope.user_id = JSON.parse(localStorage.getItem('user_id'));

	//Fetching individual favourites
  $scope.loading=true;
  $scope.nofav = true;
	IntrestedListings.getIntrestedListing($scope.user_id).then(function(success){	
    $scope.favouritecount = success.count;
    if(success.count===0){
      $scope.nofav = false
    }
		$scope.favourites = success.results;
   
    $(".loading-container").hide();
    if($scope.favourites.length > 0){
      $scope.loadmore = true;
    }else{
      
      $scope.loadmore = false;
    }
    if(success.count == $scope.favourites.length){
      $scope.loadmore = false;
    }else{
      $scope.loadmore = true;
    }
    $scope.loading=false;
    angular.forEach($scope.favourites, function(obj){
          var today = new Date();
           var birthDate = new Date(obj.listing_id.user_id.dob);
           var age = today.getFullYear() - birthDate.getFullYear();
           var m = today.getMonth() - birthDate.getMonth();
           if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
           if(age){
            obj['userdob'] = age;
          }
        })			
		},function(error){
			$(".loading-container").hide();
      $scope.nofav = false
      $scope.loading=false;
		})

	 // load more function
  $scope.page = 1;
    $scope.favourites = [];
    $scope.fetching = true;
    $scope.nofav = true;
    $scope.Loadmore = function() {
      $scope.fetching = true;
      $scope.page ++;
      $http({
        url : '/api/v1/intrested_listings/?page=' +  $scope.page+'&userid='+$scope.user_id,
        method : 'GET'
      }).then(function(success){
        if(success.count===0){
          $scope.nofav = false
        }
        $scope.fetching = false;
        $scope.favourites = $scope.favourites.concat(success.data.results);
        $(".loading-container").hide();
        if($scope.favourites.length > 0){
          $scope.loadmore = true;
        }else{
          
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.favourites.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
        $(".loading-container").hide();
        $scope.nofav = false
          //console.log(error);
        }
      );
    };
  // loadmore ends

    $scope.removeFavouriteListing = function(id){
	   data ={"roomuserID":$scope.user_id,"listing_id":id}
    IntrestedListings.removeIntrestedListing(data).then(function(success){
       swal(
        "success", 
        "Your Listing has been removed as Favourites successfully!", 
        "success"
        )
       $state.reload()
      },function(error){
        //console.log("Delete Favourite Listings",error);
        swal(
        "Failed", 
        "Listing already removed in favourites!", 
        "error"
        )
      })
    }

    $scope.individualpost = function(id){
       $state.go('individualpost',{objectId :id});
    }

    //Restricting the user profile, if user is_viewabale condition is false
    $scope.isViewable = function(id) {
      if($scope.user_id == id){
      $state.go('profile')
     }else{
       swal("Profile Viewable Restricted")
     }
    };
   
})