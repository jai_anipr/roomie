app.controller('MessagesCtrl',function($scope,$stateParams,User,Message,MessageConversation,$state,$auth,$location,$http){
	if (!$auth.isAuthenticated()) {
      $location.path("/home");
  }
	//console.log('MessagesCtrl  controller');

	var user_id = JSON.parse(localStorage.getItem('user_id'));

	var userdata = JSON.parse(localStorage.getItem('userdata'))
	console.log("userdata",userdata)

	if(user_id){
		$http({
        	url : '/api/v1/latest_conv/?userid='+user_id,
       		method : 'GET'
	     }).then(function(success){
	       //console.log("success",success)
	     },function(error){
	         //console.log(error);
	       }
	     );
	}

	var recipient = $stateParams.objectId

	$scope.getCov = {'sender_id':user_id,"recipient_id":recipient}
	MessageConversation.getMessageCov($scope.getCov).then(function(success){
		$scope.ConvDetails = success
		//console.log("cov success",success)
	},function(error){
		//console.log("error",error)
	})

	if(recipient){
		User.getUser(recipient).then(function(success){
    	$scope.senderInfo = success;
	    },function(error){
	       //console.log("sender error data", error);
	    });
	}

	$scope.message = {'text':'','sender':user_id,'recipient_id':recipient}
	$scope.submitMessage = function(){
		$scope.sender = user_id
		// console.log("$scope.message",$scope.message)
		Message.createMessage($scope.message).then(function(success){
    	   $scope.message.text = ''
    	   $state.reload()
	    },function(error){
	       //console.log("sender error data", error);
	    });

	}
})