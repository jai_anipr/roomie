app.controller('EditProfileCtrl',function($scope, User,$auth,$location,$state,City){
	if (!$auth.isAuthenticated()) {
      $location.path("/home");
   }
	$scope.user_id = JSON.parse(localStorage.getItem('user_id'));


	var resultimages = []
	var empdetails = [];

	//Send budget slider values
	$scope.sliderValue=function(slider){
		$scope.userInfo.budgetminvalue = slider.minvalue;
		$scope.userInfo.budgetmaxvalue = slider.maxvalue;
	}
	$scope.slider = {
	  minvalue: 0,
	  maxvalue: 50000,
	  options: {
	    floor: 0,
	    ceil: 50000,
	    translate: function(value, sliderId, label) {
	      switch (label) {
	        case 'model':
	          return 'AED' + value;
	        case 'high':
	          return 'AED' + value;
	        default:
	          return 'AED' + value
	      }
	    }
	  }
	};
	//Slider end

	//Fetching All Cities
	City.getCities().then(function(success){
	$scope.cities = success.results;			
	},function(error){
		
	})


	//Fetching individual favourites
	var user_id = JSON.parse(localStorage.getItem('user_id'));
	if(user_id){
		User.getUser(user_id).then(function(success){
    	$scope.userInfo = success;
    	$scope.images = success.photo_ids;
		if($scope.userInfo.budgetminvalue){
			$scope.slider.minvalue=$scope.userInfo.budgetminvalue;
		}
		if($scope.userInfo.budgetmaxvalue){
			$scope.slider.maxvalue=$scope.userInfo.budgetmaxvalue;
		}
    	if($scope.userInfo.dob){
    		$scope.userInfo.dob = moment($scope.userInfo.dob).format('MM/DD/YYYY');
    	}
    	if($scope.userInfo.move_in_date){
    		$scope.userInfo.move_in_date = moment($scope.userInfo.move_in_date).format('MM/DD/YYYY');
    	}
    	if($scope.userInfo.prefered_location){
    		$scope.userInfo.prefered_location = $scope.userInfo.prefered_location.id;
    	}
    	angular.forEach(success.photo_ids,function(obj){
    		resultimages.push(obj.photo)
    	})

    	angular.forEach(success.work_exp,function(obj){
    		empdetails.push(obj)
    	})

    	$scope.userInfo.photo_ids = resultimages
	    },function(error){
	       //console.log("user error data", error);
	    });
	}

	$scope.exp = {'first_name':'','last_name':''}
	$scope.exp = {'currently_employeed' : ''}
	$scope.exp.currently_employeed = true;
	$scope.userInfo = {'first_name':'','last_name':'','gender':''}
	$scope.save = function(){ 

		if($scope.exp.enddate){
			$scope.exp.currently_employeed = false;
		}
		$scope.loading=true;
		$(".edit-submit-btn").hide();
		$(".edit-update-btn").show();
		if($scope.userInfo.move_in_date){
			$scope.userInfo.move_in_date = moment($scope.userInfo.move_in_date).format('YYYY-MM-DD');
		}
		if($scope.userInfo.dob){
			$scope.userInfo.dob = moment($scope.userInfo.dob).format('YYYY-MM-DD');
		}


		// console.log($scope.updateuser.dob,'$scope.updateuser.dob')
		//console.log("request userInfo",$scope.userInfo)
		User.updateUser($scope.user_id,$scope.userInfo).then(function(success){
			//console.log(success,'success')
			$scope.loading=false;
			$(".edit-submit-btn").show();
			$(".edit-update-btn").hide();	
			swal("Good job!", "Profile updated Succefully", "success");
			$state.go('profile');
			},function(error){
				$scope.loading=false;
				$(".edit-submit-btn").show();
				$(".edit-update-btn").hide();			
				swal("Error", "Please Try Again", "error");			
			})
	}
	
	var listImages = []
	$scope.multipleimgdata = function(){
		cloudinary.openUploadWidget({ 
	    	cloud_name: 'brunch-buddies', 
	    	upload_preset: 'u3erc5ru'
	    }, function(error, result) {
	      	//console.log(error, result);
	      	angular.forEach(result, function(listImage) {
			  resultimages.push(listImage.secure_url)
			});
			$scope.profileimages = resultimages;
	      	// console.log("dataaaa", $scope.profileimages)
	      	$scope.uploadprofile = resultimages;
	      	$scope.userInfo.photo_ids = resultimages;
	      	$scope.$apply()
			//console.log("image", resultimages)
	    });
	}
	$scope.Deleteprofileimage = function(obj){
		$scope.uploadprofile.splice(obj, 1)
	}
	$scope.Deletedefaultimage = function(obj){
		$scope.userInfo.photo_ids.splice(obj, 1)
	}
	 


	 $scope.addJob = function(data){
   	$scope.exp ="";
   	//console.log('job details',data);
   	if(data.start_date){
   		data.start_date = moment(data.start_date).format('YYYY-MM-DD');
   	}
   	if(data.end_date){
   		data.end_date = moment(data.end_date).format('YYYY-MM-DD');
   	}
   	$scope.jobdetails = data;
   	empdetails.push($scope.jobdetails);
   	// $scope.empdetails = jobdetails;
   	//$scope.work_exp = $scope.empdetails;
   	// console.log("")	
    $scope.userInfo.work_exp = empdetails;
    //console.log("$scope.userInfo.work_exp",$scope.userInfo.work_exp)
   	$('.add-job-hide').hide();
   }
	$scope.addJobDetails=function(){
		$scope.exp ="";
		$('.add-job-hide').show();
	}   
	
})