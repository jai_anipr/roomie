app.controller('SearchListingsCtrl',function($scope,$stateParams, RoomType,PropertyType,TenantAge, City,Listing,$http,$state,Amenity,Rules,$state,Area, IntrestedListings, $auth,$window,$mixpanel, AgeChoices){
  
  //console.log('search-listings controller');
  $scope.$on('fetchId', function (event, data) {
    window.location.reload();
    $scope.user_id = data;
  });
  $scope.user_id = JSON.parse(localStorage.getItem('user_id'));

  var globalSearch = JSON.parse(localStorage.getItem('globalSearch'));

  // $mixpanel.track('Search Listing');

  $scope.IndPage = function(obj){
    window.open('#/individualpost/'+obj,'_blank');
  }

  $scope.slicklistingimagesLoaded = true;
  $scope.slicklistingimages = {
    autoplay: true,
    infinite: true,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    method: {}
  };

  //Fetching All Cities
  City.getCities().then(function(success){
  $scope.cities = success.results;      
  },function(error){
    
  })
  $scope.hello = function(){
      $state.reload();
  }

  RoomType.getRoomTypes().then(function(success){     
    $scope.roomtypes =success.results; 
  })
  PropertyType.getPropertyTypes().then(function(success){
    $scope.propertytypes =success.results; 
  })
  // get area data 
  Area.getAreas().then(function(success){
    $scope.areas = success.results;
    },function(error){
      //console.log("error",error)
    })

  Amenity.getAmenities().then(function(success){
    $scope.amenities = success;
    },function(error){
      //console.log("error",error)
    })

  $scope.selectedAmenities = [];
  $scope.searchAmenities = function(amenity, list){
    var idx = list.indexOf(amenity.id);
    if (idx > -1) {
      list.splice(idx, 1);
    }
    else {
      list.push(amenity.id);
    }
  }
  $scope.Amenitiesexists = function (amenity, list) {
    return list.indexOf(amenity.id) > -1;
  };

// fetching age chices
  AgeChoices.getAgeChoices().then(function(success){
    $scope.agechoices = success.results;
    },function(error){
      //console.log("error",error)
    })

  //Fetching rules from backend
    Rules.getRules().then(function(success){
    $scope.rules = success; 
    // console.log($scope.rules,'success --getRules');    
    },function(error){
      
    })
    $scope.selectedrules = [];
    $scope.searchRules = function(rule, list){
      var idx = list.indexOf(rule.id);
      if (idx > -1) {
        list.splice(idx, 1);
      }
      else {
        list.push(rule.id);
      }
    }
    $scope.Rulesexists = function (rule, list) {
      return list.indexOf(rule.id) > -1;
    };

   
    $scope.data = {};
    $scope.getAmenityIds = function(amenityid){
      // console.log("amenities",amenityid,$scope.data.amenitystatus)
    }




  $scope.SearchByAddress = function(){
    $scope.page = 1;
      $scope.listings = [];
      $scope.fetching = false;
    if($scope.user_id){
        $http({
        url : '/api/v1/listings/?search=' + $scope.addressSearchTerm+'&userid='+$scope.user_id+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results;
          angular.forEach($scope.listings,function(listing){
          //For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
        //End Badges
        //code for finding age
         $scope.dob = listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
          if(age){
            listing['userdob'] = age;
           }
        })
         //end code for age
        $scope.showMaplocations(success.data.results);
        if($scope.listings.length > 0){
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    }else{
    $http({
        url : '/api/v1/listings/?search=' + $scope.addressSearchTerm+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results;
        
          angular.forEach($scope.listings,function(listing){
          //For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
        //End Badges
        //code for finding age
         $scope.dob = listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
          if(age){
            listing['userdob'] = age;
           }
        })
         //end code for age
        $scope.showMaplocations(success.data.results);
        if($scope.listings.length > 0){
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
  }
}

if (globalSearch){
    $scope.addressSearchTerm = globalSearch['fulladdress']
    $scope.SearchByAddress()
    $window.localStorage.clear();
}else{
  // Listing.getListings().then(function(success){
  //   $scope.listings = success.results;
  //       angular.forEach($scope.listings,function(listing){
  //         //For badges
  //         var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
  //         var today = new Date();
  //         var secondDate = new Date(listing.move_in_date);
  //         var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
  //         listing['showbadge'] = diffDays;
  //       //End Badges
  //       //code for finding age
  //        $scope.dob = listing.user_id.dob;
  //        var today = new Date();
  //        var birthDate = new Date($scope.dob);
  //        var age = today.getFullYear() - birthDate.getFullYear();
  //        var m = today.getMonth() - birthDate.getMonth();
  //        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
  //              age--;
  //          }
  //        if(age){
           //  listing['userdob'] = age;
           // }
  //       })
  //        //end code for age
  //   $scope.showMaplocations(success.results);
  //   if($scope.listings.length > 0){
  //         //console.log("iff@@@@@");
  //         $scope.nolistings = false;
  //         $scope.loadmore = true;
  //       }else{
  //         //console.log("elseeeeee@@@@@");
  //         $scope.nolistings = true;
  //         $scope.loadmore = false;
  //       }
  //   },function(error){
  //     console.log("error",error)
  //   })
  if($scope.user_id){
      $http({
        url : '/api/v1/listings/?userid=' + $scope.user_id+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results
         angular.forEach($scope.listings,function(listing){
          //For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
        //End Badges
        //code for finding age
         $scope.dob = listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
          if(age){
                listing['userdob'] = age;
               }
        })
         //end code for age
        $scope.showMaplocations(success.data.results);
        if($scope.listings.length > 0){
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        $(".loading-container").hide();
        if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    }
    else{
    $http({
        url : '/api/v1/listings/?is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results
         angular.forEach($scope.listings,function(listing){
          //For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
        //End Badges
        //code for finding age
         $scope.dob = listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
          if(age){
            listing['userdob'] = age;
           }
        })
         //end code for age
        $scope.showMaplocations(success.data.results);
        $(".loading-container").hide();
        if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    }

}

  if($scope.user_id){
      $http({
        url : '/api/v1/listings/?userid=' + $scope.user_id+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results;
         angular.forEach($scope.listings,function(listing){
          //For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
        //End Badges
        //code for finding age
         $scope.dob = listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
           if(age){
            listing['userdob'] = age;
           }
        })
         //end code for age
        $scope.showMaplocations(success.data.results);
        if($scope.listings.length > 0){
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        $(".loading-container").hide();
        if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    }
    else{
    $http({
        url : '/api/v1/listings/?is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        $scope.listings = success.data.results
         angular.forEach($scope.listings,function(listing){
          //For badges
          var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
          var today = new Date();
          var secondDate = new Date(listing.created_at);
          var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
          listing['showbadge'] = diffDays;
        //End Badges
        //code for finding age
         $scope.dob = listing.user_id.dob;
         var today = new Date();
         var birthDate = new Date($scope.dob);
         var age = today.getFullYear() - birthDate.getFullYear();
         var m = today.getMonth() - birthDate.getMonth();
         if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
               age--;
           }
           if(age){
            listing['userdob'] = age;
           }
          
        })
         //end code for age
        $scope.showMaplocations(success.data.results);
        $(".loading-container").hide();
        if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    }

  // load more function
  $scope.page = 1;
    $scope.listings = [];
    $scope.fetching = false;
    if($scope.user_id){
    $scope.Loadmore = function() {
      $scope.fetching = true;
      $scope.page ++;
      $http({
        url : '/api/v1/listings/?page=' +  $scope.page+'&userid='+$scope.user_id+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        //console.log("get data load", success);
        $scope.fetching = false;
        $scope.listings = $scope.listings.concat(success.data.results);
         angular.forEach($scope.listings,function(listing){
          //For badges
            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var today = new Date();
            var secondDate = new Date(listing.created_at);
            var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
            listing['showbadge'] = diffDays;
          //End Badges
          //code for finding age
           $scope.dob = listing.user_id.dob;
           var today = new Date();
           var birthDate = new Date($scope.dob);
           var age = today.getFullYear() - birthDate.getFullYear();
           var m = today.getMonth() - birthDate.getMonth();
           if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                 age--;
             }
            if(age){
            listing['userdob'] = age;
           }
        })
         //end code for age
        if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        $scope.showMaplocations($scope.listings);
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    };
  }else{
       $scope.Loadmore = function() {
      $scope.fetching = true;
      $scope.page ++;
      $http({
        url : '/api/v1/listings/?page=' +  $scope.page+'&is_listable='+'True',
        method : 'GET'
      }).then(function(success){
        //console.log("get data load", success);
        $scope.fetching = false;
        $scope.listings = $scope.listings.concat(success.data.results);
         angular.forEach($scope.listings,function(listing){
          //For badges
            var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
            var today = new Date();
            var secondDate = new Date(listing.created_at);
            var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
            listing['showbadge'] = diffDays;
          //End Badges
          //code for finding age
           $scope.dob = listing.user_id.dob;
           var today = new Date();
           var birthDate = new Date($scope.dob);
           var age = today.getFullYear() - birthDate.getFullYear();
           var m = today.getMonth() - birthDate.getMonth();
           if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                 age--;
             }
            if(age){
            listing['userdob'] = age;
           }
        })
         //end code for age
        if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        $scope.showMaplocations($scope.listings);
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
      },function(error){
          //console.log(error);
        }
      );
    };
  }
  // loadmore ends

  /*$scope.individualpost = function(id){
      console.log("id",id)
       $state.go('individualpost',{objectId :id});
    }*/

  $scope.AllListingSearch = function(){

    // rule_ids filter start
    if($scope.selectedrules.length > 0){
      searchTermObj['rule_ids'] = $scope.selectedrules
      if(searchTermObj.rule_ids.length == 0){
        $scope.selectedRules = []
        delete searchTermObj.rule_ids
      }
    }
    // end

    // ame_ids filter start
    if($scope.selectedAmenities.length > 0){
      console.log("length select", $scope.selectedAmenities)
      searchTermObj['ame_ids'] = $scope.selectedAmenities
      console.log("serach teram object@@@@@@", searchTermObj)
      if(searchTermObj.ame_ids.length == 0){
        $scope.selectedAmenities = []
        delete searchTermObj.ame_ids
      }
    }
    // end
    console.log('searchTermObj', searchTermObj)


    

    $scope.ListingSearchByRegion()
      var searhTerm = ''
      angular.forEach(searchTermObj,function(key,value){
        
        if(value =='ame_ids'){
          var amenity_ids = []
          amenity_ids.push(searchTermObj['ame_ids'])
          searhTerm = searhTerm+'ame_ids'+'='+'['+amenity_ids+']'
          // if(value =='rule_ids'){
          // var rules_id = []
          // rules_id.push(searchTermObj['rule_ids'])
          //   searhTerm = searhTerm+'rule_ids'+'='+'['+rules_id+']'
          // }
        }else{
          searhTerm = searhTerm+value+'='+key+'&'
        }
        
      })
      // console.log("searhTerm",searhTerm)
      if($scope.user_id){
      $http({
        url : '/api/v1/listings/?' + searhTerm +'&userid='+$scope.user_id+'&is_listable='+'True',
        method : 'GET'
        }).then(function(success){
          $scope.listings = success.data.results
          $scope.showMaplocations($scope.listings);
            angular.forEach($scope.listings,function(listing){
              //For badges
              var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
              var today = new Date();
              var secondDate = new Date(listing.created_at);
              var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
              listing['showbadge'] = diffDays;
            //End Badges
            //code for finding age
             $scope.dob = listing.user_id.dob;
             var today = new Date();
             var birthDate = new Date($scope.dob);
             var age = today.getFullYear() - birthDate.getFullYear();
             var m = today.getMonth() - birthDate.getMonth();
             if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                   age--;
               }
              if(age){
                listing['userdob'] = age;
               }
          })
         //end code for age
         if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        $scope.showMaplocations($scope.listings);
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
        },function(error){
            //console.log(error);
          }
        );
      }else{
        $http({
        url : '/api/v1/listings/?' + searhTerm +'&is_listable='+'True',
        method : 'GET'
        }).then(function(success){
          $scope.listings = success.data.results;
          $scope.showMaplocations($scope.listings);
            angular.forEach($scope.listings,function(listing){
              //For badges
              var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
              var today = new Date();
              var secondDate = new Date(listing.created_at);
              var diffDays = Math.round(Math.abs((today.getTime() - secondDate.getTime())/(oneDay)));
              listing['showbadge'] = diffDays;
            //End Badges
            //code for finding age
             $scope.dob = listing.user_id.dob;
             var today = new Date();
             var birthDate = new Date($scope.dob);
             var age = today.getFullYear() - birthDate.getFullYear();
             var m = today.getMonth() - birthDate.getMonth();
             if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                   age--;
               }
              if(age){
                listing['userdob'] = age;
               }
            })
         //end code for age
         if($scope.listings.length > 0){
          //console.log("iff@@@@@");
          $scope.nolistings = false;
          $scope.loadmore = true;
        }else{
          //console.log("elseeeeee@@@@@");
          $scope.nolistings = true;
          $scope.loadmore = false;
        }
        $scope.showMaplocations($scope.listings);
        if(success.data.count == $scope.listings.length){
          $scope.loadmore = false;
        }else{
          $scope.loadmore = true;
        }
        },function(error){
            //console.log(error);
          }
        );
      }

    $(".extra_search_filter, .extra_filter_bottom_btns, .location_filter, .dates_filter").hide();
    $(".more_filters_btn, .list_view_image_grid").show();
  }

  //Adding listing as Favourite
  $scope.addFavourite = function(id){
  if($auth.isAuthenticated()){
    $scope.LoggedIn = true
  }
  else{
    $scope.LoggedIn = false
    $('#SearchLoginModal').modal('show');
    return false
  }
  data ={'user_id':$scope.user_id,'listing_id':id};
    IntrestedListings.createIntrestedListing(data).then(function(success){
       // console.log(success);
       swal(
        "success", 
        "Your Listing has been added as Favourites successfully!", 
        "success"
        )
       $state.reload();
       // $scope.apply();
      },function(error){
        //console.log("Add Favourite Listings",error);
        swal(
        "Failed", 
        "You already shown intrest in  this listing", 
        "error"
        )
      })
    }
  
    //Removing the listing as unfavourite
    $scope.unFavourite = function(id){
      if($auth.isAuthenticated()){
        $scope.LoggedIn = true
      }
     else{
        $scope.LoggedIn = false
        $('#SearchLoginModal').modal('show');
        return false
     }
    data ={'roomuserID':$scope.user_id,'listing_id':id};
      IntrestedListings.removeIntrestedListing(data).then(function(success){
         swal(
          "success", 
          "Your Listing has been removed as Favourites successfully!", 
          "success"
          )
         $state.reload();
         // $scope.apply();
        },function(error){
          //console.log("Delete Favourite Listings",error);
          swal(
          "Failed", 
          "Listing already removed in favourites!", 
          "error"
          )
        })
    }


  $scope.slider = {
    minValue: 0,
    maxValue: 5000,
    options: {
      floor: 0,
      ceil: 5000,
      translate: function(value, sliderId, label) {
        switch (label) {
          case 'model':
            return ' AED ' + value;
          case 'high':
            return ' AED ' + value;
          default:
            return ' AED ' + value
        }
      }
    }
  };

  $(".extra_search_filter, .extra_filter_bottom_btns, .location_filter, .dates_filter").hide();
  $scope.MoreFilterbtn = function(){
    $(".extra_search_filter, .extra_filter_bottom_btns").show();
    $(".more_filters_btn, .list_view_image_grid").hide();
  }
  $scope.CanceFilter = function(){
    $(".extra_search_filter, .extra_filter_bottom_btns, .location_filter, .dates_filter").hide();
    $(".more_filters_btn, .list_view_image_grid").show();
  }
  $scope.LocationBtn = function(){
    $('.location_filter, .extra_filter_bottom_btns').show()
    $(".more_filters_btn, .list_view_image_grid").hide();
  }
  $scope.DatesBtn = function(){
    $('.dates_filter, .extra_filter_bottom_btns').show()
    $(".more_filters_btn, .list_view_image_grid").hide();
  }

  $scope.showMaplocations = function(location){
      var map;
      var bounds = new google.maps.LatLngBounds();
      var mapOptions = {
          mapTypeId: 'roadmap'
      };
                      
      // Display a map on the web page
      map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
      map.setTilt(50);
      // Multiple markers location, latitude, and longitude
        var markers = [];
        //console.log("locations", location);
        angular.forEach(location, function(obj){
          if (obj.is_listable == true){
            var listingmap = []
            listingmap.push(obj.location_id.street_name);
            listingmap.push(parseFloat(obj.latitude))
            listingmap.push(parseFloat(obj.longitude))
            listingmap.push(parseFloat(obj.monthly_rent))
            markers.push(listingmap)
          }
          
        })               
      // Info window content
        var infoWindowContent = [];
        angular.forEach(location, function(obj){
          var listingdesc = []
          var listingMapDesc =  "<a class='targetblank' target='_blank' href='#/individualpost/"+obj.id+"'>" +'<div class="info_content">' +'<img src='+obj.photo_ids[0].photo+' class="image-map"/>' +'<h3 class="h3-class">'+obj.location_id.street_name+'</h3>' + '<h3 class="h3-class">' + ' AED ' +obj.monthly_rent+'</h3>' +'<p class="p-class">'+ obj.description +'</p>' + '</div>' + '</a>'
          listingdesc.push(listingMapDesc);
          infoWindowContent.push(listingdesc)
        })
          
      // Add multiple markers to map
      var infoWindow = new google.maps.InfoWindow(), marker, i;
      // Place each marker on the map  
      for( i = 0; i < markers.length; i++ ) {
          var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
          var markerLabel = 'AED '+markers[i][3];
          bounds.extend(position);
          
        marker = new MarkerWithLabel({
          map: map,
          animation: google.maps.Animation.DROP,
          position: position,
          icon: "/static/images/map1.png",
          labelContent: markerLabel,
          labelClass: "marker-labels"
        }); 
          
          // Add info window to marker    
          google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                  infoWindow.setContent(infoWindowContent[i][0]);
                  infoWindow.open(map, marker);
              }
          })(marker, i));

          // Center the map to fit all markers on the screen
          map.fitBounds(bounds);
      }

      // Set zoom level
      var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
          this.setZoom(9);
          google.maps.event.removeListener(boundsListener);
      });
      
  }
  // Load initialize function


   // serch by region 
    var searchTermObj = {}
    $scope.ListingSearchByRegion = function(){
      searchTermObj['city_id'] = $scope.city_id 

    }

    $scope.ListingSearchByMoveDate = function(){
      searchTermObj['move_date_gte'] = $scope.move_in_date
      $scope.AllListingSearch()
    }

    $scope.searchListingByDuration = function(){
      searchTermObj['validity_for_stay'] = $scope.duration
      $scope.AllListingSearch()

    }

    $scope.searchListingByGender = function(){
      searchTermObj['gender_choice'] = $scope.gender
      $scope.AllListingSearch()
    }
    $scope.searchListingByroomtype = function(){
      searchTermObj['room_type'] = $scope.roomtype;
      $scope.AllListingSearch()
    }
    $scope.searchListingBypropertytype = function(){
      searchTermObj['property_type_id'] = $scope.propertytype;
      $scope.AllListingSearch()
    }
    $scope.searchListingByAge = function(){
      searchTermObj['age_choices'] = $scope.selectedToppings
      // console.log("age search",searchTermObj.age_choices);
      if(searchTermObj.age_choices.length == 0){
        $scope.selectedToppings = ""
        delete searchTermObj.age_choices
      }
      $scope.AllListingSearch()
    }
    

    $scope.getAgeChoices = function(){
      //console.log("getAgeChoices",getAgeChoices)
    }

    $scope.getArea = function(){
      searchTermObj['area_id'] = $scope.area
      $scope.AllListingSearch()
    }

     $scope.searchByPriceRange = function(){
      searchTermObj['max_price'] = $scope.slider['maxValue']
      searchTermObj['min_price'] = $scope.slider['minValue']
      //console.log("duration search",searchTermObj);
      $scope.AllListingSearch()
      // console.log("duration search",searchTermObj);

    }

    
    

    $scope.handleDateChange = function(date){
      //console.log("handleDateChange",date)
    }

  // Reset Filter

  $scope.ResetFilter = function(){
    //console.log("reset filters");
    $scope.post = {"date":""}
    $scope.post.date = ""
    $scope.area = ""
    $scope.move_in_date = ""
    $scope.gender = ""
    $scope.roomtype = ""
    $scope.propertytype = ""
    $scope.selectedToppings = ""
    $scope.min_price = 0
    $scope.max_price = 0
    $scope.slider.maxValue = 5000
    $scope.slider.minValue = 0
    $scope.selectedAmenities = []
    $scope.selectedrules = []
    
    $scope.duration = ""
    delete searchTermObj.ame_ids
    delete searchTermObj.rule_ids
    delete searchTermObj.age_choices
    delete searchTermObj.room_type
    delete searchTermObj.property_type_id
    delete searchTermObj.gender_choice
    delete searchTermObj.duration
    delete searchTermObj.max_price
    delete searchTermObj.min_price
    delete searchTermObj.area_id
    delete searchTermObj.move_date_gte
    delete searchTermObj.validity_for_stay

    // searchTermObj['gender_choice'] = $scope.gender
    // searchTermObj['duration'] = $scope.duration
    // searchTermObj['max_price'] = 0
    // searchTermObj['min_price'] = 5000

    $scope.AllListingSearch()
    // $scope.apply()
  }

  //Restricting the user profile, if user is_viewabale condition is false
    $scope.isViewable = function(id) {
      if($scope.user_id == id){
      $state.go('profile')
     }else{
       swal("Profile Viewable Restricted")
     }
    };


  // map autocomplete code start 
  function addresscomplete() {
    //console.log("google")
      autocomplete = new google.maps.places.Autocomplete((document.getElementById('ListingSearch')), 
        {
            types: ['geocode']
        }
      );
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
          var result = autocomplete.getPlace();
          // console.log(result); // take a look at this result object
          // console.log(result.address_components); // a result has multiple address components
          for(var i = 0; i < result.address_components.length; i += 1) {
            var addressObj = result.address_components[i];
            for(var j = 0; j < addressObj.types.length; j += 1) {
              if (addressObj.types[j] === 'country') {
                // console.log(addressObj.types[j]); // confirm that this is 'country'
                //console.log(addressObj.long_name); // confirm that this is the country name
                if(addressObj.long_name == "United Arab Emirates"){
                  $scope.showinvalid = true;
                  $(".showinvalid").hide();
                  // alert("ok dubai")
                }else{
                  $scope.showinvalid = false;
                  $(".showinvalid").show();
                  // alert("ok not dubai")
                }
              }
            }
          }
      });      
  }
  addresscomplete();
  // map autocomplete code end 
  TenantAge.getTenantAges().then(function(success){
    // console.log("success", success)
    $scope.TenantAge = success
  }, function(error){
    console.log("error", error)
  })



})