
app.controller('HomeCtrl',function($scope,$state){
	//console.log('home controller');
	

	/*$scope.Cities = [
	    { name: 'New York' },
	    { name: 'Manhattan' },
	    { name: 'Brooklyn' },
	    { name: 'Dubai' },
	    { name: 'Abu Dhabi'}
	];
*/
    $scope.homepageslideLoaded = true;
    $scope.homepageslide = {
      autoplay: true,
      infinite: true,
      autoplaySpeed: 3000,
      slidesToShow: 1,
      slidesToScroll: 1,
      method: {}
    };

	$scope.city = {
	    "name": "Dubai",
	};

	$scope.Cities = [
	    { name: 'Dubai' },
	    { name: 'Abu Dhabi'}
	];

	$scope.SearchGlobal = function(){
		// console.log("search term",$scope.GlobalSearchTerm)
		var globalSearch = {"fulladdress":$scope.GlobalSearchTerm,"is_search":true}
		localStorage.setItem('globalSearch', JSON.stringify(globalSearch));
		$state.go('search-listings');
	}
	$scope.SearchBycity = function(area){
		$scope.GlobalSearchTerm = area;
		// console.log("search term",$scope.GlobalSearchTerm)
		var globalSearch = {"fulladdress":$scope.GlobalSearchTerm,"is_search":true}
		localStorage.setItem('globalSearch', JSON.stringify(globalSearch));
		$state.go('search-listings');
	}
	/*function initialize() {
	    autocomplete = new google.maps.places.Autocomplete((document.getElementById('GlobalSearch')), 
		    {
		        types: ['geocode']
		    }
	    );
	    google.maps.event.addListener(autocomplete, 'place_changed', function () {
	        place = autocomplete.getPlace();
	        console.log(place);
	        $scope.GlobalSearchTerm = place.formatted_address
	    });
	}
	initialize();*/

	function initialize() {
		var input = document.getElementById('GlobalSearch');
		var options = {
		    componentRestrictions: {
		        country: 'AE'
		    }
		};
		var autocomplete = new google.maps.places.Autocomplete(input, options);
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
		    var place = autocomplete.getPlace();
		    $scope.GlobalSearchTerm = place.formatted_address
		});
	}
	initialize();

})