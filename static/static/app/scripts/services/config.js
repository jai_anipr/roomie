
// Config module service for communicate with back end

// Amenity service for communicate with back end
app.factory('Amenity', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getAmenities = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createAmenity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getAmenity = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteAmenity = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateAmenity = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/amenities/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Amenity service here 

// City service for communicate with back end
app.factory('City', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getCities = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/cities/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createCity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getCity = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteCity = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateCity = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/cities/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end city service here 

// Country service for communicate with back end
app.factory('Country', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getCountries = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/countries/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createCountry = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getCountry = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteCountry = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateCountry = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/countries/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end country service here 

// States service for communicate with back end
app.factory('State', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getStates = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/states/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createState = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/states/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getState = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/states/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteState = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/states/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateState = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/states/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end State service here 

// Location service for communicate with back end
app.factory('Location', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getLocations = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/location/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createLocation = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/location/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getLocation = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/location/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteLocation = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/location/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateLocation = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/location/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Locations service here

// Notifications service for communicate with back end
app.factory('Notification', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getNotifications = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/notifications/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createNotification = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/notifications/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getNotification = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/notifications/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteNotification = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/notifications/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateNotification = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/notifications/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Notifications service here

// Area service for communicate with back end
app.factory('Area', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getAreas = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/area/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createArea = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/area/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getArea = function(id){
      console.log("id",id)
      var defered = $q.defer();
      $http({
        url : '/api/v1/area/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteArea = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/area/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateArea = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/area/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Messages service here

// Age Choices service for communicate with back end
app.factory('AgeChoices', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getAgeChoices = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/tenant_ages/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
// end Messages service here

// Bills service for communicate with back end
app.factory('Bills', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getBills = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/get_bill_details/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
// end Messages service here
// cleaning service for communicate with back end
app.factory('Cleaning', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getCleaning = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/get_all_cleaning_basis/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
// end Messages service here

