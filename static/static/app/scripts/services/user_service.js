// Config module service for communicate with back end

// listings service for communicate with back end
app.factory('User', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getUsers = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createUser = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getUser = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteUser = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateUser = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/users/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Users service here 

//Login service for communicate with back end
app.factory('Login', function($q, $http){
  var object = {};
  object.Login = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/login',
      method: 'POST',
      data: data
    }).then(function(success){
      defered.resolve(success.data);
    },
    function(error){
      defered.reject(error.data);
    });
    return defered.promise;
  }
  //Login Login service

  //Logout Service 
  object.Logout = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/logout',
      method : 'GET',
      data : data
    }).then(function(success){
      defered.resolve(success.data);
    },
      function(error){
        defered.reject(error.data);       
      }
    );
    return defered.promise;
  } 

  // facebook login
  object.FacebookLogin = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/facebook-login',
        method: 'POST',
        data: data
      }).then(function(success){
        defered.resolve(success.data);
      },
      function(error){
        defered.reject(error.data);
      });
      return defered.promise;
    }
   //End Facebook login

  //forgotPassword service
  object.forgotPassword = function(data){
    var defered = $q.defer();
    $http({
      url : '/api/v1/forgot-password',
      method : 'POST',
      data : data
    }).then(function(success){
      defered.resolve(success.data);
    },
      function(error){
        defered.reject(error.data);       
      }
    );
    return defered.promise;
  }
  // end Forget password service

  // forgot-password-process login
  object.resetPassword = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/forgot-password-process',
        method: 'POST',
        data: data
      }).then(function(success){
        defered.resolve(success.data);
      },
      function(error){
        defered.reject(error.data);
      });
      return defered.promise;
    };
  return object;
});