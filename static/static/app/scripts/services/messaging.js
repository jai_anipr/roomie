
// Messages service for communicate with back end
app.factory('Message', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getMessages = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/messages/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createMessage = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/messages/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getMessage = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/messages/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteMessage = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/messages/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateMessage = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/messages/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Messages service here

// Messages conversations service for communicate with back end
app.factory('MessageConversation', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getMessageCov = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/message_conv/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.deleteMessage = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/message_conv/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
// end Messages service here



