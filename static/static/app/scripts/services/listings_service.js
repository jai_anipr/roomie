// Config module service for communicate with back end

// listings service for communicate with back end
app.factory('Listing', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getListings = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createListing = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getListing = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/?userid='+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.getIndividualListing = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteListing = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/'+id,
        method : 'DELETE',
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateListing = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listings/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Listing service here 

//listing_photos service for communicate with back end
app.factory('ListingPhotos', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getListingPhotos = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/listing_photos/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createListingPhoto = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listing_photos/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getListingPhoto = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listing_photos/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteListingPhoto = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listing_photos/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateListingPhoto = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/listing_photos/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end listingPhotos service here 

// Interested list service for communicate with back end
app.factory('IntrestedListings', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getIntrestedListings = function(){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrested_listings/',
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.removeIntrestedListing = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/delete_intrest/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.createIntrestedListing = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrested_listings/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getIntrestedListing = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrested_listings/?userid='+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteIntrestedListing = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrested_listings/'+id+'/',
        method : 'DELETE',
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateIntrestedListing = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/intrested_listings/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end IntrestedListing service here 

//Rules service for communicate with back end
app.factory('Rules', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getRules = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/rules/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createRule = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/rules/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getRule = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/rules/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteRule = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/rules/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateRule = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/rules/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end Rules service here 


//Payment Plans service for communicate with back end
app.factory('PaymentPlan', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getPlans = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/purchase_type/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createPlan = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getPlan = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deletePlan = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updatePlan = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/purchase_type/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end PaymentPlan service here 

//Payment Plans service for communicate with back end
app.factory('TenantAge', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getTenantAges = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/tenant_ages/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createTenantAge = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tenant_ages/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getTenantAge = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tenant_ages/'+id+'/',
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.deleteTenantAge = function(id, data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tenant_ages/'+id+'/',
        method : 'DELETE',
        data : data,
        headers: {
            'Content-Type': 'application/json'
            }
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.updateTenantAge = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/tenant_ages/'+id+'/',
        method : 'PUT',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
          console.log(error);
        }
      )
      return defered.promise;
    };
  return object;
  
}]);
// end PaymentPlan service here 

//Property type service for communicate with back end
app.factory('PropertyType', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getPropertyTypes = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/property_type/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
  
}]);

//Furniture type service for communicate with back end
app.factory('Furniture', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getFurnitures = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/all_furnitures/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
//End Furniture type service for communicate with back end

//Profession photography service for communicate with back end
app.factory('Prophoto', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getProphoto = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/get_photo_schemes/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
//End Profession photography  service for communicate with back end

//Scan 360  service for communicate with back end
app.factory('Scan360', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getScan360 = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/get_scan360_schemes/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
}]);
//End Scan 360  service for communicate with back end

//Room type service for communicate with back end
app.factory('RoomType', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getRoomTypes = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/room_type/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
  
}]);
app.factory('Request', ['$http','$q', function ($http,$q) {
  var object = {};
    object.getRequests = function(){
      var defered = $q.defer();
      $http({
        url : "/api/v1/requests/",
        method : 'GET'
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
    object.createRequest = function(data){
      var defered = $q.defer();
      $http({
        url : '/api/v1/requests/',
        method : 'POST',
        data : data
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);       
        }
      );
      return defered.promise;
    };
    object.getIndividualRequests = function(id){
      var defered = $q.defer();
      $http({
        url : '/api/v1/requests/?userid='+id,
        method : 'GET',
      }).then(function(success){
        defered.resolve(success.data);
      },
        function(error){
          defered.reject(error.data);
        }
      );
      return defered.promise;
    };
  return object;
  
}]);