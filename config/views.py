# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

from rest_framework .viewsets import ModelViewSet
from .models import *
from .serializers import *
from rest_framework import status,response


class badge_viewset(ModelViewSet):
	queryset = badges.objects.all()
	serializer_class = badge_serializer

class notification_viewset(ModelViewSet):
	queryset = notification.objects.all()
	serializer_class = notification_serializer


class rules_viewset(ModelViewSet):
	queryset = rules.objects.all()
	serializer_class = rules_serializer

	def list(self, request, *args, **kwargs):
		queryset = rules.objects.all()
		serializer = rules_serializer(instance=queryset,many=True)


		def paginate_queryset(self, queryset, view=None):
			return None

		# for temp in serializer.data:
		#     brunch_obj = Brunch.objects.filter(id = temp['id'])
		#     brunch_data = brunch_serializer(instance=brunch_obj,many=True)
		#     temp['brunch_image'] = brunch_data.data



		return response.Response(serializer.data, status=status.HTTP_200_OK)



class amenity_viewset(ModelViewSet):
	queryset = amenity.objects.all()
	serializer_class = amenity_serializer

	def list(self, request, *args, **kwargs):
		queryset = amenity.objects.all()
		serializer = amenity_serializer(instance=queryset,many=True)


		def paginate_queryset(self, queryset, view=None):
			return None

		# for temp in serializer.data:
		#     brunch_obj = Brunch.objects.filter(id = temp['id'])
		#     brunch_data = brunch_serializer(instance=brunch_obj,many=True)
		#     temp['brunch_image'] = brunch_data.data



		return response.Response(serializer.data, status=status.HTTP_200_OK)
