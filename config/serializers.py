from .models import *
from rest_framework.serializers import ModelSerializer


class amenity_serializer(ModelSerializer):
    class Meta:
        model = amenity
        fields = '__all__'

class notification_serializer(ModelSerializer):
    class Meta:
        model = notification
        fields = '__all__'


class badge_serializer(ModelSerializer):
    class Meta:
        model = badges
        fields = '__all__'


class rules_serializer(ModelSerializer):
    class Meta:
        model = rules
        fields = '__all__'
