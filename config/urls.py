from django.conf.urls import include, url
from django.contrib import admin

from rest_framework.routers import DefaultRouter,SimpleRouter

from .views import *

router = SimpleRouter()


router.register(r'notifications',notification_viewset, 'notifications')
router.register(r'amenities',amenity_viewset, 'amenities')
router.register(r'badges',badge_viewset, 'badges')
router.register(r'rules',rules_viewset, 'rules')


urlpatterns = router.urls

