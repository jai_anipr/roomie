# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
"""
class country(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.name)

class state(models.Model):
    state_name = models.CharField(max_length=100)
    country_id = models.ForeignKey(country)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.state_name)


class city(models.Model):
    city_name = models.CharField(max_length=100)
    state_id = models.ForeignKey(country)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.city_name)

"""

class amenity(models.Model):
    amenity_name = models.CharField(max_length=100)
    icon_css_classname = models.CharField(max_length=100,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.amenity_name)

from users.models import findroomUser
#from listings.models import listings
class notification(models.Model):
    notification_text = models.TextField(max_length=100)
    user = models.ForeignKey(findroomUser)
    #favourited = models.ForeignKey('listings.listings',related_name='favourited',blank=True,null=True)
    listing = models.ForeignKey('listings.listings',blank=True,null=True)
    other_user = models.ForeignKey(findroomUser,related_name='other_user',blank=True,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.notification_text)

class badges(models.Model):
    badge_name = models.CharField(max_length=100,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.badge_name)

class rules(models.Model):
    rule_name = models.CharField(max_length=50)
    icon_css_classname = models.CharField(max_length=100,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.rule_name)

"""
class location(models.Model):
    street_name = models.CharField(max_length=100)
    latitude = models.FloatField()
    longitude = models.FloatField()
    apartment = models.CharField(max_length=100,blank=True,null=True)
    city = models.ForeignKey(city)
    state = models.ForeignKey(state)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s' % (self.street_name)


"""

