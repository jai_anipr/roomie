
# import required libraries/classes 
from rest_framework.routers import SimpleRouter
from rest_framework.routers import format_suffix_patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from django.conf.urls import url, include
from django.contrib import admin
from filters.views import *

from .views import *

router = SimpleRouter()

router.register(r'users-count',users_count , 'users-count')
router.register(r'listings-count',listings_count , 'listings-count')
router.register(r'free-listings-count',free_listings_count ,'free-listings-count')
router.register(r'paid-listings-count',paid_listings_count , 'paid-listings-count')
router.register(r'free-listings',free_listings , 'free-listings')
router.register(r'paid-listings',paid_listings , 'paid-listings')

urlpatterns = router.urls
