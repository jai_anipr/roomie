
# import required libraries here
from django.shortcuts import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import request, response, status
from listings.serializers import *
from listings.models import *
from rest_framework import status, viewsets
from users.models import *
from users.serializers import *

# import from settings.py
from roomi.settings import *

from datetime import datetime
import datetime

# import drf pagination library
from rest_framework.pagination import PageNumberPagination

from django.db.models import Q

# views start from here
class listings_count(viewsets.ViewSet):
	''' All the filters for dashboard '''
	# get all the listing count
	def list(self, request):
		listing_count = listings.objects.all().count()
		return Response({"listing_count":listing_count})
class free_listings_count(viewsets.ViewSet):
	# get all the user count
	def list(self, request):
		free_listings_count = listings.objects.filter(purchase_type__purchase_name='Basic').count()
		return Response({"free_listings_count":free_listings_count})

class free_listings(viewsets.ViewSet):
	# get all the user count
	def list(self, request):
		# free_listings_count = listings.objects.filter(purchase_type__purchase_name='Basic')
		# return Response({"free_listings_count":free_listings_count})

		try:
			queryset = listings.objects.filter(purchase_type__purchase_name='Basic').order_by('-created_at')
			serializer = listings_serializer(instance=queryset, many=True)
			qs = self.filter_queryset(queryset)
			page = self.paginate_queryset(qs)
			if page is not None:
				serializer = listings_serializer(page, many=True)

			# if (findroomuserid):

			#     for temp in serializer.data:

			#         my_list_object = listings.objects.filter(id=temp['id'], user_id=int(findroomuserid))

			#         if my_list_object:
			#             temp['I_created_this'] = True
			#         else:
			#             temp['I_created_this'] = False

			#         my_fav_object = intrested_listings.objects.filter(listing_id=temp['id'], user_id=int(findroomuserid))

			#         if my_fav_object:
			#             temp['I_favourited_this'] = True
			#         else:
			#             temp['I_favourited_this'] = False

			return (self.get_paginated_response(serializer.data))
		except Exception as e:
			return response.Response({'error': e}, status=status.HTTP_400_BAD_REQUEST)


class paid_listings_count(viewsets.ViewSet):
	# get all the user count
	def list(self, request):
		paid_listings_count = listings.objects.exclude(purchase_type__purchase_name='Basic').count()
		return Response({"paid_listings_count":paid_listings_count})

class paid_listings(viewsets.ViewSet):
	# get all the user count
	def list(self, request):
		paid_listings_count = listings.objects.exclude(purchase_type__purchase_name='Basic')
		return Response({"paid_listings_count":paid_listings_count})

class users_count(viewsets.ViewSet):
	
	# get all the user count
	def list(self, request):
		users_count = findroomUser.objects.all().count()
		return Response({"users_count":users_count})
